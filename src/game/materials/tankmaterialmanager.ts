import { AssetsManager, Color3, InputBlock, NodeMaterial, Scene, Texture, TextureBlock } from "babylonjs"
import { INamedColour } from "../interfaces"


export namespace TankMaterialManager{
  
  const textures:Array<Texture> = new Array<Texture>()
  const materials:Map<string,NodeMaterial> = new Map<string,NodeMaterial>()


  export function getTankMaterialNamed(scene:Scene, colourDesc:INamedColour){
    return getTankMaterial(scene, colourDesc.name, colourDesc.colour)
  }


  export function getTankMaterial(scene:Scene, name:string, colour:Color3):NodeMaterial{
    let tankMat = materials.get(name)
    if (!tankMat){
      tankMat = new NodeMaterial(`tankmaterial-${name}`,scene, { emitComments: true })
      materials.set(name, tankMat)
      tankMat.loadAsync("/assets/tankmat.json").then(() => {
        const normalBlock = tankMat.getBlockByPredicate((b)=>b.name==="NormalTexture") as TextureBlock
        normalBlock.texture = textures[1]
        const textureBlock = tankMat.getBlockByPredicate((b)=>b.name==="BaseTexture") as TextureBlock
        textureBlock.texture = textures[0]
        const markingColourBlock = tankMat.getBlockByPredicate((b)=>b.name==="MarkingColour") as InputBlock
        markingColourBlock.value = colour
        markingColourBlock.isConstant = true
        markingColourBlock.isUniform = true  
        tankMat.build()
      })
    }
    return tankMat
  }

  export function loadAssets(assetMan:AssetsManager):void {
    assetMan.addTextureTask("loadtanktext", "/assets/tanktexalpha.png", true,false).onSuccess = (t)=>{ textures[0] = t.texture }
    assetMan.addTextureTask("loadtanknormal", "/assets/tanknormalsmol.png", true,false).onSuccess = (t)=>{ textures[1] = t.texture }
  }
}