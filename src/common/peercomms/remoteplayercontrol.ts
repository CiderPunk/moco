import { ICommsEntity, ICommsBuffer, IPeerConnection } from "../../comms/client/clientinterfaces";
import { V2 } from "../math/v2";
import { MoCoPeerComms } from "./enums";
import { IController } from "../interfaces";

export class RemotePlayerControl implements ICommsEntity, IController{

  updated() {
    if (this.onChange) {
      this.onChange(this);
    }
  }

  get typeId():number { 
    return MoCoPeerComms.EntityType.Controller
  }

  public readonly joyLeft:V2
  public readonly joyRight:V2
  public fire1:boolean
  public fire2:boolean

  public onChange: (RemotePlayerControl) => void;

  public constructor(public readonly entId:number, public readonly pc?:IPeerConnection){
    this.joyLeft = new V2()
    this.joyRight = new V2()
    this.fire1 = false
    this.fire2 = false
    //client side we don't have a peer connection object so you cant do this...
    if (pc){
      pc.onPeerDisconnected = ()=>{  
        if (this.onDisconnected){ 
          this.onDisconnected()
        }
      }
    }
  }
  public onDisconnected: () => void;

  writeState(buffer: ICommsBuffer): void {
    buffer.WriteFloat(this.joyLeft.x)
    buffer.WriteFloat(this.joyLeft.y)
    buffer.WriteFloat(this.joyRight.x)
    buffer.WriteFloat(this.joyRight.y)
    buffer.WriteBool(this.fire1)
    buffer.WriteBool(this.fire2)
  }

  readState(buffer: ICommsBuffer): void {
    this.joyLeft.set( buffer.ReadFloat(), buffer.ReadFloat() )
    this.joyRight.set( buffer.ReadFloat(), buffer.ReadFloat() )
    this.fire1 = buffer.ReadBool()
    this.fire2 = buffer.ReadBool()
  }

  toString():string{
    return `left:${this.joyLeft.toString()}, right:${this.joyRight.toString()} fire1:${this.fire1}, fire2:${this.fire2}` 
  }
  
}