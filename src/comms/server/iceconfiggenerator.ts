import { IIceConfigGenerator, IClient } from "./interfaces"
import crypto = require('crypto')

export class IceConfigGenerator implements IIceConfigGenerator{

  constructor(protected stun:string, protected turn:string,  protected secret:string, protected validTime:number = 3600){}

  getIceConfig(user:IClient):object {
    const validTimeStamp = (Math.floor(Date.now() / 1000) + this.validTime)
    const uname = [validTimeStamp, user.id].join(':')
    const hmac = crypto.createHmac('sha1', this.secret)
    hmac.setEncoding('base64')
    hmac.write(uname)
    hmac.end()
    const passwd = hmac.read()
    return {
      iceServers:[
        { 
          urls:this.stun,
          credential:passwd,
          username:uname
        },
        {
          urls:this.turn,
          credential:passwd,
          username:uname
        } 
      ]}
  }

}