import { Color3 } from "babylonjs";
import { IV2 } from "../../common/interfaces";
import { Colours } from "../constants";
import { INamedColour } from "../interfaces";


export namespace TeamSetings{
  export const startPositions:Array<IV2> = [
    {x:10,y:0},
    {x:10,y:60},
    {x:10,y:-60},
    {x:40,y:20},
    {x:40,y:-20},
    {x:80,y:-40},
    {x:80,y:40},
    {x:90,y:0}
  ]




  export const colours = new Array<INamedColour>(
    { name:"red", colour: Colours.red },
    { name:"green", colour: Colours.green },
    { name:"blue", colour: Colours.blue },
    { name:"white", colour: Colours.white },
    { name:"black", colour: Colours.black },
    { name:"cyan", colour: Colours.cyan },
    { name:"cyan", colour: Colours.cyan },
    { name:"yellow", colour: Colours.yellow },
    { name:"orange", colour: Colours.orange },
    { name:"mauve", colour: Colours.mauve },
    { name:"teal", colour: Colours.teal },
    { name:"brown", colour: Colours.brown },
    { name:"gold", colour: Colours.gold },
    { name:"pink", colour: Colours.pink },
    { name:"fuschia", colour: Colours.fuschia },
    { name:"purple", colour: Colours.purple },
  )





 

}