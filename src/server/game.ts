import { IGame,   IMoCoClient, IClientManager } from "./interfaces";
import { MoCoComms } from "../common/mococomms";
import { BaseClient } from "../comms/server/baseclient";

export class Game implements IGame{

  readonly players:Map<string,IMoCoClient>
  playerId:number = 1
  addPlayer(player:IMoCoClient):void{
    player.setNextEntId((this.playerId++) * 32)
    const playerInfo = player.getPeerInfo() as MoCoComms.PlayerInfo
    console.log(`player ${playerInfo?.name ?? "Unnamed"} connecting to ${this.key}`)
    this.client.startPeerConnect(player , true)
  }

  dropPlayer(uuid:string) {
  //  this.players.delete(uuid)  
  //  this.socket.emit(EventNames.PlayerDisconnect, { uuid:uuid }  as Comms.PlayerDisconnecting )
  }
  
  constructor(protected client:IMoCoClient, protected owner:IClientManager, public key:string){
    this.players = new Map<string,IMoCoClient>()
    //tell the game that it's registered and can start....
    client.socket.emit(MoCoComms.Events.CreateGameResponse, { key: key } as MoCoComms.CreateGameResponse)
    console.log(`Game started ${this.key}`)
  }




}   