import { h, Component, ComponentChild } from "preact"
import { Joystick } from "./joystick"
import { ICanvasDrawControl } from "../interfaces"
import { IV2, IScreenSyncd } from "../../common/interfaces"
import { JoyButton } from "./joybutton"
import { RemotePlayerControl } from "../../common/peercomms/remoteplayercontrol"
import { RegisterList } from "../../common/utils/registerlist"

export interface ControlProps{
  control:RemotePlayerControl
  registerScreenSyncd:(IScreenSyncd, number?)=>void
  deRegisterScreenSyncd:(IScreenSyncd)=>void
}

interface ControlState{
  fullScreen:boolean
}

export class Control extends Component<ControlProps, ControlState> implements IScreenSyncd{

  ctx:CanvasRenderingContext2D
  canvas:HTMLCanvasElement

  private readonly canvasControls = new RegisterList<ICanvasDrawControl>()

  constructor(props:ControlProps){
    super(props)
    this.canvas = document.createElement('canvas')
    this.componentDidMount = ()=>{
      this.canvas.width = this.base.clientWidth
      this.canvas.height = this.base.clientHeight
      this.base.appendChild(this.canvas)
      this.ctx = this.canvas.getContext('2d')
      props.registerScreenSyncd(this, 10)
    }

    this.componentWillUnmount= ()=>{
      props.deRegisterScreenSyncd(this)
    }

    this.state = { fullScreen: false}

  }

  refresh(time: number, dt: number): void {
    this.ctx.save();
    // Use the identity matrix while clearing the canvas
    this.ctx.setTransform(1, 0, 0, 1, 0, 0);
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    // Restore the transform
    this.ctx.restore();
    //lets draw those components...
    this.canvasControls.items.forEach((cc:ICanvasDrawControl)=>{
      this.ctx.save()
      cc.drawCanvas(this.ctx)
      this.ctx.restore()
    })
  }



  updateControlState(){
    if (this.props.control){
      console.log(`Control state; ${this.props.control.toString()} `)
    }
  }

  render(props: Readonly<ControlProps>, state?: Readonly<ControlState>): ComponentChild { 
    return <div class="controler">
      {/* 
         {!state.fullScreen ? <button  class="button" onClick={()=>{ this.setState({ fullScreen:true})} }>Fullscreen</button> : null} 
      */}

      <Joystick cssClass="left" text="Steering"
        drawRegister={(con:ICanvasDrawControl)=>this.canvasControls.register(con)} 
        drawDeRegister={(con:ICanvasDrawControl)=>this.canvasControls.deregister(con)}
        updateValue={(state:IV2)=>{ 
          this.props.control.joyLeft.setV2(state)
          this.props.control.updated()
        }}
        deadZone={0.2}
      />

      <Joystick cssClass="right" text="Turret"
        drawRegister={(con:ICanvasDrawControl)=>this.canvasControls.register(con)} 
        drawDeRegister={(con:ICanvasDrawControl)=>this.canvasControls.deregister(con)} 
        updateValue={(state:IV2)=>{ 
          this.props.control.joyRight.setV2(state)
          this.props.control.updated()
        }}
        deadZone={0}
      />

      <JoyButton cssClass="top" updateValue={(state:boolean)=>{ this.props.control.fire1 = state
                this.props.control.updated() }} 
       />
      <JoyButton cssClass="btm" updateValue={(state:boolean)=>{ this.props.control.fire2 = state
                this.props.control.updated() }} 
       />

    </div>
  }

}


