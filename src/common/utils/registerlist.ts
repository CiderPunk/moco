export class RegisterList<T>{

  private readonly _items = new Array<[number, T]>()
  private _sorted:Array<T> = null
  
  get items():Array<T>{
    if (this._sorted == null){
      this._sorted = this._items
      .sort((a,b)=>a[0] - b[0])
      .map(it=>{ return it[1] as T})
    }
    return this._sorted 
  }

  public register(item:T, sort:number = 0):void{
    const index = this._items.findIndex(([,it])=>it==item)
    if (index < 0){
      this._items.push([sort,item])
      this._sorted = null
    }
  }
  public deregister(item:T):void{
    const index = this._items.findIndex(([,it])=>it==item)
    if (index > -1){
      this._items.splice(index,1)
      this._sorted = null
    }
  }

}