import { h, Component, ComponentChild } from "preact"
import { ICanvasDrawControl } from "../interfaces"
import { V2 } from "../../common/math/v2"

export interface JoystickProps{
  //canvasContainer:Component
  drawRegister: (con:ICanvasDrawControl)=>void
  drawDeRegister: (con:ICanvasDrawControl)=>void
  scaleFactor?:number
  cssClass?:string
  updateValue?:(IV2)=>void
  text?:string
  deadZone:number
}


export class Joystick extends Component<JoystickProps> implements ICanvasDrawControl {

  mouseDown:boolean
  readonly startPos:V2
  readonly movePos:V2
  readonly nubPos:V2
  readonly joyPos:V2
  boundsRadius:number
  nubRadius:number
  scaleFactor:number = 0.25



  public constructor(props:JoystickProps){
    super(props)
    this.startPos = new V2()
    this.movePos = new V2()
    this.nubPos = new V2()
    this.joyPos = new V2()

    this.scaleFactor = props.scaleFactor?? 0.35

    this.componentDidMount=()=>{
      this.props.drawRegister(this)
      const smallestAxis = this.base.clientWidth > this.base.clientHeight ? this.base.clientHeight: this.base.clientWidth
      this.boundsRadius = this.scaleFactor * smallestAxis
      this.nubRadius = this.boundsRadius * 0.5
    }
    this.componentWillUnmount=()=>{
      this.props.drawDeRegister(this)
    }
  }

  drawCanvas(ctx:CanvasRenderingContext2D){
    if (!this.startPos.isZero()){
      ctx.strokeStyle = 'white'
      ctx.beginPath()
      ctx.arc(this.startPos.x, this.startPos.y, this.boundsRadius, 0, 2 * Math.PI)
      ctx.stroke()


      //draw compas cardinals
      ctx.save()
      ctx.translate(this.startPos.x, this.startPos.y)
      for(let v= 0; v <4; v++){
        ctx.beginPath()
        ctx.moveTo(0,this.boundsRadius - 10)
        ctx.lineTo(0, this.boundsRadius + 10)
        ctx.stroke()
        ctx.rotate(Math.PI / 2)
      }
      ctx.restore()

      ctx.beginPath()
      ctx.lineWidth= 6
      ctx.arc(this.nubPos.x, this.nubPos.y, this.nubRadius, 0, 2 * Math.PI)
     
      ctx.stroke()

    }
  }

  render(props: Readonly<JoystickProps>): ComponentChild { 
    return <div class={"joystick " + props.cssClass} 
      onMouseDown={(ev:MouseEvent)=>this.handleMouseDown(ev)} 
      onMouseUp={()=>this.handleMouseStop()}     
      onMouseOut={()=>this.handleMouseStop()} 
      onMouseMove={(ev:MouseEvent)=>this.handleMouseMove(ev)} 
      onTouchStart={(ev:TouchEvent)=>this.handleTouchStart(ev)} 
      onTouchMove={(ev:TouchEvent)=>this.handleTouchMove(ev)}
      onTouchEnd={(ev:TouchEvent)=>this.handleTouchStop(ev)}
      >
  <p>{props.text}</p>
    </div>
  }

  protected handleMouseStop(): void {
    if (this.mouseDown){
      this.stopJoystick()
      this.mouseDown = false
    }
  }

  protected handleMouseMove(ev: MouseEvent): void {
    if (this.mouseDown){
      this.moveJoystick(ev.clientX, ev.clientY)
    }
  }

  protected handleMouseDown(ev: MouseEvent): void {
    this.mouseDown = true
    ev.preventDefault()
    this.startJoystick(ev.clientX, ev.clientY)
  }

  private firstTouchId:number

  protected handleTouchStart(ev: TouchEvent) {
    ev.preventDefault()
    if (ev.targetTouches.length < 2){
      this.firstTouchId = ev.targetTouches[0].identifier
      this.startJoystick(ev.targetTouches[0].clientX, ev.targetTouches[0].clientY)
    }
  }

  protected handleTouchMove(ev: TouchEvent) {
    ev.preventDefault()
    for(var i = 0; i < ev.targetTouches.length; i++){
      if (ev.targetTouches[i].identifier == this.firstTouchId){
        this.moveJoystick(ev.targetTouches[i].clientX, ev.targetTouches[i].clientY)
      }
    }

  }
  protected handleTouchStop(ev:TouchEvent): void {
    for(var i = 0; i < ev.changedTouches.length; i++){
      if (ev.changedTouches[i].identifier == this.firstTouchId){
        this.stopJoystick()
      }
    }


    if (ev.targetTouches.length == 0){
      this.stopJoystick()
    }
  }

  protected startJoystick(clientX: number, clientY: number) {
    //place a canvas at the center...draw a circle
    //console.log(`Start joystick ${clientX},${clientY}`)
    this.startPos.set(clientX,clientY)
    this.moveJoystick(clientX, clientY)
    
  }

  protected moveJoystick(clientX:number, clientY:number){
    this.movePos.set(clientX, clientY).sub(this.startPos)
    const angle = this.movePos.getAngle()
    let dist = this.movePos.len() / this.boundsRadius
    //limit to 1
    dist = dist > 1 ? 1 : dist 
    this.joyPos.setAngle(angle, dist)
    this.nubPos.setV2(this.startPos).addScale(this.joyPos, this.boundsRadius) 
    //quick switcheroo so up is positive
    this.joyPos.y = -this.joyPos.y

    if (Math.abs(this.joyPos.x) < this.props.deadZone){
      this.joyPos.x = 0
    }

    if (Math.abs(this.joyPos.y) < this.props.deadZone){
      this.joyPos.y = 0
    }

    this.props.updateValue(this.joyPos)

    //console.log(`Move joystick ${this.joyPos.x},${this.joyPos.y}`)
  }

  stopJoystick() {
    this.startPos.reset()
    this.props.updateValue(this.joyPos.reset())
    //console.log(`Stop joystick`)
  }
}

  
