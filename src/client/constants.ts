export enum Status {
  Register,
  Waiting,
  Playing,
  Prefs,
  Error
}

export namespace ClientConstants{
  export const PlayerNameKey = "PlayerName"


}