export namespace Config{
  export const iceConfig = { 
    iceServers:[{ 
      urls:"stun:tankclub.ciderpunk.net:3478"
    },
    {
      urls:"turn:tankclub.ciderpunk.net:3478?transport=tcp",
      credential: "test",
      username: "test"
    } 
  ]};
}