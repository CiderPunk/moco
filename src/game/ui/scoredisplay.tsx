import { h, Component, ComponentChild } from "preact"


export interface ScoreDisplayProps{
   teamAName:string,
   teamBName:string,
   teamAScore:number,
   teamBScore:number
}

interface ScoreDisplayState{
}

export class ScoreDisplay extends Component<ScoreDisplayProps,ScoreDisplayState>{
  
  render(props: Readonly<ScoreDisplayProps>, state?: Readonly<ScoreDisplayState>, context?: any): ComponentChild {
    return (  
    <div class="scoredisplay"> 
      <div class="team team-a">
        <h1>{props.teamAName}</h1>
        <h2>{props.teamAScore}</h2>
      </div>
      <div class="team team-b">
        <h1>{props.teamBName}</h1>
        <h2>{props.teamBScore}</h2>
      </div>
    </div>)
  }
  
}