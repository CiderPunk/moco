import { IV2 } from "../common/interfaces";

export interface ICanvasDrawControl{
  drawCanvas(ctx:CanvasRenderingContext2D):void
}
