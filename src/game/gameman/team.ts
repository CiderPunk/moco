import { IController, IV2 } from "../../common/interfaces";
import { MoCoComms } from "../../common/mococomms";
import { ICommsEntityManager } from "../../comms/client/clientinterfaces";
import { PlayerTank } from "../entity/playertank";
import { IBall, IEntity, IGameManager, INamedColour, IPlayer, IPositionOrientation, ITeam, IWorld } from "../interfaces";
import { TankMaterialManager } from "../materials/tankmaterialmanager";
import { TeamSetings } from "./teamsettings";

export class Team implements ITeam{



  public score:number = 0
  readonly players:Array<PlayerTank>
  private nextPos:number = 0;

  public constructor(public readonly owner:IGameManager, public name:string, public teamColour:INamedColour,  private orientation:number, protected colours:Array<INamedColour>, protected positions:Array<IV2>){
    this.players= new Array<PlayerTank>()
  }

  getNewSpawnPosition() : IPositionOrientation {
    return { position:this.getNextPosition(), orientation:this.orientation }
  }

  
  reset(hardReset:boolean = false) {
    this.players.forEach(p=>{ p.respawn()})
    if (hardReset){
      this.score = 0
      this.nextPos = 0
    }
  }


  addPlayer(world:IWorld, control: IController, playerInfo: MoCoComms.PlayerInfo): IPlayer {

    const scene = world.renderer.getScene()
    const playerColour = this.getNextColour()
    const player = new PlayerTank(world, control, this, playerColour,
      TankMaterialManager.getTankMaterialNamed(scene, this.teamColour),
      TankMaterialManager.getTankMaterialNamed(scene, playerColour),
      this.getNextPosition(), this.orientation )    
    this.players.push(player)
    console.log(`player ${playerInfo.name} joined team ${this.name}`)
    return player
  }

  removePlayer(tank:IPlayer):void{
    //find player
    const i = this.players.findIndex((t)=>(t.playerInfo.uuid === tank.playerInfo.uuid))
    //remove from our list
    if (i > -1){
      this.players.splice(i,1)
    }
    //return colour to rotation
    this.colours.push(tank.playerColour)
  }

  getNextColour():INamedColour{
    //pick a randomn colour!
    if (this.colours.length >0){
      const i = Math.floor(Math.random() * this.colours.length)
      const [col] = this.colours.splice(i,1)
      return col
    }
    else{ //ruh-roh hacky we ran out of colours!
      const i = Math.floor(Math.random() * TeamSetings.colours.length)
      return TeamSetings.colours[i]
    }
    
  }

  getNextPosition():IV2{
    return this.positions[this.nextPos++ % this.positions.length]
  }


  goalScore(ball:IBall):void{
    this.score++
    this.owner.goalScored(this, ball.lastTouch)
  }

  getScore(): number {
    return this.score
  }
  
  getPlayerCount():number { 
    return this.players.length 
  }

}