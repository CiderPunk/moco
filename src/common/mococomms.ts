
export namespace MoCoComms{

  export namespace Events{
    export const CreateGameRequest = "cgrq"
    export const CreateGameResponse = "cgrp"
    export const JoinGameRequest = "jgrq"
    export const ReadyToJoin = "rtj"
    export const PeerSignal = "sgnl"
    export const PlayerJoinRequest = "pjrq"
    export const PlayerDisconnect = "pdrq"
    export const Error = "Err"
    export const Connected = "connect"
    export const Disconnected = "disconnect"
  }

  export interface PlayerJoining{
    uuid:string
    name:string
  }

  export interface PlayerDisconnecting{
    uuid:string
  }

  export interface ErrorResponse{
    message:string
  }

  export interface JoinGameRequest{
    key:string
    name:string
  } 

  export interface CreateGameRequest{
    key:string
    hash:string
  }

  export interface CreateGameResponse{
    key:string
    hash:string
  }

  export interface PlayerInfo{
    name:string
    uuid:string
  }

}