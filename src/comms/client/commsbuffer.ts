import { ICommsBuffer, PeerMessageType, IPeerConnection } from "./clientinterfaces"


/**
 * provides read write facilites for array buffers used as comms between clients
 */
export class CommsBuffer implements ICommsBuffer {

  protected readonly dv:DataView

  protected index:number = 4
  
  public get Tick():number{ return this.dv.getInt32(0) }
  public set Tick(val:number){ this.dv.setUint32(0, val) }
  public get Length():number{ return this.index }

  constructor(protected readonly buffer: ArrayBuffer, public readonly owner:IPeerConnection = null) {
    this.dv = new DataView(buffer)
  }


  public Reset():void{
    this.index = 4
  }

  ReadFloat():number{
    const val = this.dv.getFloat32(this.index)
    this.index+=4 
    return val
  }

  ReadBool():boolean{
    return (this.dv.getInt8(this.index++) == 1)
  }

  ReadTinyInt():number{
    return this.dv.getInt8(this.index++)
  }

  ReadInt():number{
    const val = this.dv.getInt32(this.index)
    this.index+=4 
    return val
  }

  ReadUint():number{
    const val = this.dv.getUint32(this.index)
    this.index+=4 
    return val
  }
  
  ReadBigFloat():number{
    const val = this.dv.getFloat64(this.index)
    this.index+=8 
    return val
  }

  WriteBool(val:boolean):void{
    return (this.dv.setInt8(this.index++, +val))
  }  
  WriteTinyInt(val:number):void{
    return (this.dv.setInt8(this.index++, val))
  }

  WriteFloat(val:number):void{
    this.dv.setFloat32(this.index, val)
    this.index+=4 
  }

  WriteUint(val:number):void{
    this.dv.setUint32(this.index, val)
    this.index+=4 
  }  

  WriteInt(val:number):void{
    this.dv.setInt32(this.index, val)
    this.index+=4 
  }

  WriteBigFloat(val:number):void{
    this.dv.setFloat64(this.index, val)
    this.index+=8
  }

  GetSendView():Uint8Array{
    //write the end type...
    this.WriteUint(PeerMessageType.End)
    //get the message send buffer
    return new Uint8Array(this.buffer, 0, this.Length)
  }

}
