export enum EntityType {
  player,
  shell,
  ball,
  boundary,
  other,
  goal
}