import { World, Body, Vec2 } from "planck-js";
import planck = require("planck-js");
import { IV2 } from "../../common/interfaces";
import { CollisionGroup, Constants } from "../constants";
import { EntityType } from "../enum";
import { Team } from "../gameman/team";
import { IWorld, IEntity, ITeam, IBall } from "../interfaces";
import { BaseEntity } from "./baseentity";

export class Goal implements IEntity{

  get type(): EntityType { return EntityType.goal }
  
  protected readonly body: planck.Body;
  
  constructor(protected world:IWorld, protected team:ITeam, protected position:IV2){
    this.body = world.world.createBody({type: planck.Body.STATIC, position: new Vec2(this.position) })
    const shape = new planck.Box(Constants.GoalDepth / 2 ,Constants.GoalSize / 2)  
    const fix = this.body.createFixture({shape: shape, isSensor:true })
    fix.setFilterData({ groupIndex:0, categoryBits:CollisionGroup.ballBoundary, maskBits:CollisionGroup.ball})
    //this.body.setSleepingAllowed(false)
    this.body.setUserData(this);

  }
  setPosition(pos: IV2, orientation: number, resetVelocity: boolean) {
    this.body.setPosition(new Vec2(pos))
  }

  initDraw(world: IWorld): void {
  }

  prePhysics(dT: number): boolean {
    return true
  }
  preDraw(dt: number): void {
  }
  getPosition(): IV2 {
    return {x:0, y:0}
  }
  getVelocity(): IV2 {
    return {x:0, y:0}
  }


  collision(other: IEntity) {

    const ball = other as IBall
    //GOOAAAAALLLLL
    console.log(`GOOAAAAAAAALLLLLLL for team ${this.team.name} scored by ${ball.lastTouch.playerInfo.name}`)
    this.team.goalScore(other as IBall)
  }
}