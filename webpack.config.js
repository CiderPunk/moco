var path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
//const MiniCssExtractPlugin = require("mini-css-extract-plugin");
var fs = require('fs');
//from 
//https://github.com/wzr1337/node.express.webpack.starter/blob/master/webpack.config.js
var nodeModules = {};
fs.readdirSync('node_modules')
  .filter(function (x) {
    return ['.bin'].indexOf(x) === -1;
  })
  .forEach(function (mod) {
    nodeModules[mod] = 'commonjs ' + mod;
  });


module.exports = [{
  mode: "development",
  watchOptions: {
    poll: true,
    ignored: "/node_modules/"
  },
  devtool: "inline-source-map",
  entry: "./src/server/index.ts",
  target: "node",
  output: {
    filename: "server.js",
    path: path.resolve(__dirname, 'dist')
  },
  watch:true,
  resolve: {
    // Add `.ts` and `.tsx` as a resolvable extension.
    extensions: [".ts", ".js"]
  },
  module: {
    rules: [
      // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
      { test: /\.tsx?$/, loader: "ts-loader" }
    ]
  },
  externals: nodeModules
},
{
  mode: "development",
  watchOptions: {
    poll: true,
    ignored: /node_modules/
  },
  devtool: "inline-source-map",
  entry: "./src/game/index.tsx",
  target: "web",
  output: {
    filename: "game.js",
    path: path.resolve(__dirname, 'public/scripts')
  },
  watch:true,
  resolve: {
    // Add `.ts` and `.tsx` as a resolvable extension.
    extensions: [".ts", ".tsx", ".js"]
  },
  module: {
    rules: [
      //{test: /planck\-js\/lib\/index\.js/, use:"exports-loader?planck" },
     // {test: /planck/, use:"imports-loader?this=>window" },
     //{test: /planck/, use:["script-loader"] },
      // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
      { test: /\.tsx?$/, loader: "ts-loader" }
    ]
  },  
  plugins: [
    new webpack.ProvidePlugin({
          'process': 'process/browser',
          'planck': 'planck-js'
      })
  ],
  externals: {
    jquery: 'jQuery'
  }
},
{
  mode: "development",
  watchOptions: {
    poll: true,
    ignored: /node_modules/
  },
  devtool: "inline-source-map",
  entry: "./src/client/index.tsx",
  target: "web",
  output: {
    filename: "client.js",
    path: path.resolve(__dirname, 'public/scripts')
  },
  watch:true,
  resolve: {
    // Add `.ts` and `.tsx` as a resolvable extension.
    extensions: [".ts", ".tsx", ".js"]
  },
  module: {
    rules: [
      //{test: /planck\-js\/lib\/index\.js/, use:"exports-loader?planck" },
     // {test: /planck/, use:"imports-loader?this=>window" },
     //{test: /planck/, use:["script-loader"] },
      // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
      { test: /\.tsx?$/, loader: "ts-loader" }
    ]
  },
  plugins: [
    new webpack.ProvidePlugin({
          'process': 'process/browser'
      })
  ],
  externals: {
    jquery: 'jQuery'
  }
},
{
  mode: "development",
  watchOptions: {
    poll: true,
    ignored: /node_modules/
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'styles.css',
    }),
  ],
  entry: "./src/sass/index.js",
  target: "web",
  output: {
    filename: "style.js",
    path: path.resolve(__dirname, 'public/scripts')
  },
  watch:true,
  resolve: {
    extensions: [".js"]
  },
  module: {
    rules: [
      { 
        test: /\.s[a|c]ss$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader'
        ]
      }
     //
    ]
  }
}
];