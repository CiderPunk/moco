import { ICommsBuffer, ICommsEntity, IPeerConnection } from "../../comms/client/clientinterfaces";
import { ITankHealth, IV3 } from "../interfaces";
import { MoCoPeerComms } from "./enums";

export class TankHealth implements ICommsEntity, ITankHealth{

  public health:number
  public onUpdated:(health:number) => void

  public constructor(public readonly entId:number, public readonly pc?:IPeerConnection){
    this.health = 100
  }

  get typeId():number { 
    return MoCoPeerComms.EntityType.TankHealth
  }

  writeState(buffer: ICommsBuffer): void {
    buffer.WriteFloat(this.health)
  }

  readState(buffer: ICommsBuffer): void {
    this.health = buffer.ReadFloat()
    if (this.onUpdated){
      this.onUpdated(this.health)
    }
  }
  
}
