import { h, Component, ComponentChild } from "preact"
import { threadId } from "worker_threads"

export interface RegisterProps{
  gameKey:string
  playerName:string
  onRegister:(key:string, name:string)=>void
}


interface RegisterState{

  gameKey:string
  playerName:string
  playerNameValid:boolean
  gameKeyValid:boolean
}

export class Register  extends Component<RegisterProps, RegisterState> {

  public constructor(props:RegisterProps){
    super(props)

    this.state = { 
      gameKey: props.gameKey,
      playerName: props.playerName,
      playerNameValid: true,
      gameKeyValid: true 
    }
  }

  protected doRegister(e:Event){

   
    const formdata = new FormData(e.target as HTMLFormElement)
    const name = (formdata.get('playerName') as string).trim()
    const key = (formdata.get('gameKey') as string).trim()



    this.setState({ 
      playerName:name,
      gameKey:key,
      playerNameValid: name.length > 2,
      gameKeyValid: key.length == 5
    })

    if (this.state.playerNameValid && this.state.gameKeyValid){
    this.props.onRegister(this.state.gameKey, this.state.playerName )
    }
    e.preventDefault()
  }

  render(props: Readonly<RegisterProps>, state?: Readonly<RegisterState>): ComponentChild {
    return <section class="section">
      <h1 class="title">Join game</h1>
    <form onSubmit={(e:Event)=>{ this.doRegister(e) }}>

    <div class="field">
      <label class="label">Player Name</label>
      <div class="control">
        <input class="input" type="text" name="playerName" value={state.playerName} placeholder="Player name" maxLength={16}/>
        {
          !state.playerNameValid ? <p class="help is-danger">Please enter a valid name (more than 2 characters)</p> : null
        } 
      </div>
    </div>

    <div class="field">
      <label class="label">Game Key</label>
      <div class="control">
        <input class="input" type="text" name="gameKey" value={state.gameKey} placeholder="Game key" maxLength={6}/>
        {
          !state.gameKeyValid ? <p class="help is-danger">Please enter a valid game key</p> : null
        } 
      </div>
    </div>
    <div class="field is-grouped">
      <div class="control">
        <button class="button is-link" >Join</button>
      </div>
    </div>
  </form>
  </section>
  }
 

}