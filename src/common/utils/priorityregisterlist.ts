export interface IPriorityComponent{
  priorty:number
}

export class PriorityRegisterList<T extends IPriorityComponent>{

  public readonly items = new Array<T>()

  public register(item:T):void{
    if (this.items.indexOf(item) < 0){
      this.items.push(item)
      this.items.sort((a:T,b:T)=>a.priorty - b.priorty)
    }
  }
  public deregister(item:T):void{
    const i = this.items.indexOf(item)
    if (i > -1){
      this.items.splice(i,1)
    }
  }
}