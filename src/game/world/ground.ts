import { AbstractMesh, ArcRotateCamera,BlurPostProcess,  Material, MultiRenderTarget, Vector4, Camera, Color3, Color4, Engine, MeshBuilder, RenderTargetTexture, StandardMaterial, Texture, Vector2, Vector3, Effect, PostProcess, NodeMaterial, AssetsManager, TextureBlock, ShaderMaterial, Scene } from "babylonjs";
import { CustomMaterial } from "babylonjs-materials";


import { Tank } from "../entity/tank";
import { IGround, IWorld } from "../interfaces";

export class Ground implements IGround{
  
  static readonly trackLayerMask = 0x10000000
  
  static trackTexture: Texture;
  static macroTexture: Texture;

  protected groundTexRenderTarget: RenderTargetTexture
  protected resetGrass: boolean
  protected trackMat: Material



  public constructor(world:IWorld){
    const size = world.size
    const scene = world.renderer.getScene()
    const ground = MeshBuilder.CreateGround("ground", { width:size.x, height:size.y, subdivisions:2, updatable:false})

    //set up camera to track prints
    //const printCam = new ArcRotateCamera("groundcam", -Math.PI / 2, 0,0, new Vector3(0, size.y /2,0), scene, false)
    const printCam = new ArcRotateCamera("groundcam", -Math.PI / 2, Math.PI,1000, new Vector3(0, 0,0), scene, false)
    //ortho
    printCam.mode = Camera.ORTHOGRAPHIC_CAMERA
    //layer mask for tracks
    printCam.layerMask = Ground.trackLayerMask
    //cam view the same size as our ground
    
    printCam.orthoBottom = size.y/2 
    printCam.orthoTop= -size.y/2
    printCam.orthoLeft = -size.x/2
    printCam.orthoRight = size.x/2
    
    //add cam to scene
    scene.cameras.push(printCam)
    //set active cam for debugging
    //scene.activeCamera = printCam
    
    
    //setup rut render target
    this.groundTexRenderTarget = new RenderTargetTexture("ruts", 2048,scene, false,false, BABYLON.Constants.TEXTURETYPE_FLOAT, false, Texture.DEFAULT_ANISOTROPIC_FILTERING_LEVEL, false, false, false )

  

    const heightToNormal = this.initHeightToNormal(scene.getEngine())
    this.groundTexRenderTarget.addPostProcess(heightToNormal)
  
    this.groundTexRenderTarget.activeCamera = printCam
    
  

    //always do a first clear
    this.resetGrass = true
    this.groundTexRenderTarget.onClearObservable.add((engine:Engine) => {  
      if (this.resetGrass){
        engine.clear(new Color4(0,0,0,1),true,false,false)
        this.resetGrass = false
      }
    })
  
    //add rtt to scene   
    scene.customRenderTargets.push(this.groundTexRenderTarget)
  
    //setup material for visible ground, TODO: custom shader
    const groundMat2 = new StandardMaterial("simpleground", scene)
    groundMat2.disableLighting = false
    groundMat2.bumpTexture = this.groundTexRenderTarget
    //ground.material = groundMat2

    this.trackMat = this.buildTrackMat(scene)


    const groundMaterial = new NodeMaterial("ground material", scene, { emitComments: true })
    groundMaterial.loadAsync("/assets/pitchmat.json").then(() => {
      groundMaterial.build(false)
      const txBlock = groundMaterial.getBlockByPredicate((b)=>b.name==="Normal Map") as TextureBlock
      txBlock.texture = this.groundTexRenderTarget
      const txMacro = groundMaterial.getBlockByPredicate((b)=>b.name==="MacroTexture") as TextureBlock
      txMacro.texture = Ground.macroTexture
      ground.material = groundMaterial
    })

/*
 NodeMaterial.ParseFromSnippetAsync("2NGGDV#13", scene).then((nm)=>{
  const txNormal = nm.getBlockByPredicate((b)=>b.name==="Normal Map") as TextureBlock
  txNormal.texture = this.groundTexRenderTarget
  const txMacro = nm.getBlockByPredicate((b)=>b.name==="MacroTexture") as TextureBlock
  txMacro.texture = Ground.macroTexture
  ground.material = nm
})
*/

  }
  reset() {
    this.resetGrass = true
  }


  private initHeightToNormal(engine:Engine):PostProcess{

    //shader adpeted from https://stackoverflow.com/questions/5281261/generating-a-normal-map-from-a-height-map
    Effect.ShadersStore["heightToNormalFragmentShader"] = `
#ifdef GL_ES
    precision highp float;
#endif
varying vec2 vUV;
uniform sampler2D textureSampler;
const vec2 size = vec2(10.0,0.0);
const ivec3 off = ivec3(-2,0,2);

void main(void) 
{
  vec4 mid = texture(textureSampler, vUV);
  float s11 = mid.x;
  float s01 = textureOffset(textureSampler, vUV, off.xy).x;
  float s21 = textureOffset(textureSampler, vUV, off.zy).x;
  float s10 = textureOffset(textureSampler, vUV, off.yx).x;
  float s12 = textureOffset(textureSampler, vUV, off.yz).x;
  vec3 va = normalize(vec3(size.xy,s21-s01));
  vec3 vb = normalize(vec3(size.yx,s12-s10));
  gl_FragColor = vec4( cross(va,vb), s11 );
}`

const postProcess = new PostProcess("heightToNormal", "heightToNormal", [], null,1, null,1, engine);
postProcess.onApply = (effect)=> {
 // effect.setFloat("uFade", Math.random())
//  effect.setFloat("uTime", this.world.time)
}
return postProcess

  }



  /*
  private initFadeDownPostProcess(engine:Engine):PostProcess{
    Effect.ShadersStore["fadedownFragmentShader"] = `
    #ifdef GL_ES
        precision highp float;
    #endif

    // Samplers
    varying vec2 vUV;
    uniform sampler2D textureSampler;
    uniform float uFade;
    void main(void) 
    {
        vec4 baseColor = texture2D(textureSampler, vUV);
        gl_FragColor = baseColor.rgba * uFade;
    }
    `
    const postProcess = new PostProcess("Fadedown", "fadedown", ["uFade"], null,1, null,1, engine);
    postProcess.onApply = (effect)=> {
      effect.setFloat("uFade", Math.random()) 
    //  effect.setFloat("uTime", this.world.time)
    }
    return postProcess
  }
*/
  protected buildTrackMat(scene:Scene):ShaderMaterial{

    Effect.ShadersStore["trackPrintVertexShader"] = `
      #ifdef GL_ES
          precision highp float;
      #endif
      // Attributes
      attribute vec3 position;
      attribute vec2 uv;
      // Uniforms
      uniform mat4 worldViewProjection;
      // Varying
      varying vec2 vUV;
      void main(void) {
        gl_Position = worldViewProjection * vec4(position, 1.0);
        vUV = uv;
      }`
    
    Effect.ShadersStore["trackPrintFragmentShader"] = `
      #ifdef GL_ES
          precision highp float;
      #endif
      varying vec2 vUV;
      uniform sampler2D textureSampler;
      uniform float offset;
      void main(void) {
        gl_FragColor = texture2D(textureSampler, vec2(vUV.x, vUV.y+offset));
      }`
    const shaderMat =   new ShaderMaterial("track shader", scene,
    { vertex: "trackPrint", fragment: "trackPrint"},
    { 
      attributes:["position","uv"],
      uniforms:["world", "worldView", "worldViewProjection", "view", "projection", "offset"]
    })
    shaderMat.setTexture("textureSampler", Ground.trackTexture)
    shaderMat.backFaceCulling = false

    shaderMat.onBind = (mesh:AbstractMesh)=>{
      shaderMat.setFloat("offset", mesh.metadata.offset)
    }
    return shaderMat
  }


  public registerTrackMesh(mesh:AbstractMesh):void{

    //set the track mesh to the correct material, layer mask and add to render list
    mesh.layerMask = Ground.trackLayerMask
    //mesh.material = this.trackMat
    mesh.material = this.buildTrackMat(mesh.getScene())
    this.groundTexRenderTarget.renderList.push(mesh)
  }

  public unregisterTrackMesh(mesh:AbstractMesh):void{
    //not 100% sure on this one, maybe Map?
    const i = this.groundTexRenderTarget.renderList.lastIndexOf(mesh)
    if (i > -1){
      this.groundTexRenderTarget.renderList = this.groundTexRenderTarget.renderList.splice(i,1)
    }
  }

  public static loadAssets(assetMan:AssetsManager):void{
    assetMan.addTextureTask("tankprint", "/assets/trackprint2.png", true).onSuccess = (t)=>{
      Ground.trackTexture = t.texture
    }
    assetMan.addTextureTask("macroTex", "/assets/pitch.png", true).onSuccess = (t)=>{
      Ground.macroTexture = t.texture
    }

  }

}