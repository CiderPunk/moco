import { IV2 } from "../interfaces";

export class V2 implements IV2{

  public x:number = 0
  public y:number = 0

  public constructor(pos?:IV2){
    if (pos){
      this.x = pos.x
      this.y = pos.y
    }
  }

  public toString(): string {
    return `(X:${this.x},Y:${this.y})`
  }

  private static temp = new V2()
 
  public set(x:number, y:number):V2{
    this.x = x
    this.y = y
    return this
  }

  public setV2(vec:IV2):V2{
    this.x = vec.x
    this.y = vec.y
    return this
  }

  public isZero=():boolean => (this.x ==0 && this.y==0)

  public reset():V2{
    this.x = 0
    this.y = 0
    return this
  }

  
  public add(other:IV2):V2{
    this.x += other.x
    this.y += other.y
    return this
  }

  public addScale(other:IV2, scale:number):V2{
    this.x += other.x * scale
    this.y += other.y * scale
    return this
  }

  public sub(other:IV2):V2{
    this.x -= other.x
    this.y -= other.y
    return this
  }
  public scale(v:number):V2{
    this.x *= v
    this.y *= v
    return this
  }
  public len2():number{
    return (this.x * this.x) + (this.y * this.y)
  }  
  public len():number{
    return Math.sqrt(this.len2())
  }
  public dot(other:IV2):number{
    return this.x * other.x + this.y * other.y
  }
  public cross(other:IV2):number{
    return this.x * other.y - other.x * this.y
  }
  public norm():V2{
    let len = this.len()
    //no divide by zeros here!
    len= len < 0.00001 ? 0.00001 : len
    return this.scale(1/len)
  }
  public rotateCW():V2{
    let temp = this.x
    this.x = this.y
    this.y = -temp
    return this
  }
  public rotateCCW():V2{
    let temp = this.x
    this.x = -this.y
    this.y = temp
    return this
  }

  public rotate(angle:number):V2{
    let cos = Math.cos(angle)
    let sin = Math.sin(angle)
    let x= (this.x * cos) - (this.y * sin)
    let y= (this.x * sin) + (this.y * cos)
    this.x = x
    this.y = y
    return this
  }

  public fan(angle:number, dir1:V2, dir2:V2):V2{
    dir1.setV2(this)
    dir1.rotate(0.5 * angle)
    dir2.setV2(this)
    dir2.rotate(-0.5 * angle)
    return this
  }

  public fanArray(angle:number,result:Array<V2>):Array<V2>{
    var step = angle / (result.length -1)
    var start = -0.5 * angle
    result.forEach((val:V2, index:number)=>{  
      return val.setV2(this).rotate(start + (step * index))
    })
    return result
  }

  public invert(){
    
    this.x = -this.x
    this.y = -this.y
    return this
  }

  public mul(val:number):V2{
    this.x*= val
    this.y*=val
    return this
  }

  public setAngle(rads:number, mag:number){
    this.x = mag * Math.cos(rads) 
    this.y = mag * Math.sin(rads)
    return this
  }

  /**
   * returns angle from positive Y axis, always positive
   */
  public getAngleToVert():number{
    const val = (0.5 * Math.PI) - this.getAngle()
    return val < 0 ? val + (2*Math.PI) : val
  }

  public getAngle():number{
    return Math.atan2(this.y, this.x);
  }
/**
 * calculates reflection of vector relative to the normalized vector
 * @param norm normalized vector to reflect off
 */
  public reflect(norm:IV2){
    //http://math.stackexchange.com/questions/13261/how-to-get-a-reflection-vector
    V2.temp.set(norm.x, norm.y)
    let dp = this.dot(V2.temp)
    V2.temp.scale(2 * dp)
    this.sub(V2.temp)
    return this
  }
  public max(val:IV2){
    this.x = Math.max(this.x, val.x)
    this.y = Math.max(this.y, val.y)
  }
  public min(val:IV2){
    this.x = Math.min(this.x, val.x)
    this.y = Math.min(this.y, val.y)
  }
}