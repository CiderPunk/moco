import { IEntity, IRenderer, IShooter, IWorld } from "../interfaces"
import planck = require("planck-js")
import { CollisionGroup } from "../constants"
import { IV2 } from "../../common/interfaces"
import * as BABYLON from 'babylonjs'
import { AbstractMesh, AssetsManager, NodeMaterial, Texture, Vector3, Scene, TextureBlock, InputBlock, Color3, Material, MeshBuilder, StandardMaterial } from "babylonjs"
import { BaseEntity } from "./baseentity"




export namespace TankConstants{
  export const leftTrack = new planck.Vec2(-2.5,0)
  export const rightTrack = new planck.Vec2(2.5,0)
  export const leftDrive:Array<[number,number]> = [[-Math.PI,-0.5],[-0.75 * Math.PI, -0.6],[-0.45 * Math.PI,-0.6],[-0.55 * Math.PI,-0.6],[-0.25 * Math.PI,-0.2],[0, 0.65],[0.25* Math.PI, 0.8],[0.45* Math.PI, 1],[0.55* Math.PI, 1],[0.75* Math.PI, 0.3],[Math.PI, -0.5]]
  export const rightDrive:Array<[number,number]> = [[-Math.PI,0.65],[-0.75 * Math.PI, -0.2],[-0.45 * Math.PI,-0.68],[-0.55 * Math.PI,-0.68],[-0.25 * Math.PI,-0.6],[0, -0.5],[0.25* Math.PI, 0.3],[0.45* Math.PI, 1],[0.55* Math.PI, 1],[0.75* Math.PI, 0.8],[Math.PI, 0.65]]
}

export abstract class Tank extends BaseEntity implements IEntity{

  private _groupIndex: number
  private showNameTime: number = 0



  public get groupIndex():number{
    return this._groupIndex
  }

  private static groupIndexCount = -1
  static tankNo:number = 0

  //static loaded meshes
  static trackMesh:AbstractMesh  
  static turretMesh:AbstractMesh  
  static chassisMesh:AbstractMesh
  static fairingMesh:AbstractMesh
  static printMesh:AbstractMesh

  
  //display meshes
  protected trackLeft:AbstractMesh
  protected trackRight:AbstractMesh

  protected fairingLeft:AbstractMesh
  protected fairingRight:AbstractMesh

  protected chassis:AbstractMesh = null
  protected turret:AbstractMesh = null
  protected gun:AbstractMesh = null

  protected printLeft:AbstractMesh
  protected printRight:AbstractMesh

  protected namePlateMesh: AbstractMesh

  //turrent orientation
  protected turretOrient:number = 0
  protected targetOrientation:number = 0

  //track offset
  private leftOffset:number = 0
  private rightOffset:number = 0

  //forces app;lied to each track through control input
  protected leftDrive:number = 0
  protected rightDrive:number = 0

  abstract get name():string

  public createBody(world:planck.World, position:IV2,  orientation:number){
    this._groupIndex = Tank.groupIndexCount--
    const body = world.createBody({type: planck.Body.DYNAMIC,position: new planck.Vec2(position), angle: orientation, angularDamping:2})
    const shape = new planck.Box(2.65,3.375)
    const fix = body.createFixture({shape: shape, restitution:0.1, density:1})
    const turret = body.createFixture({shape: new planck.Circle(new planck.Vec2(0,1), 1.5), restitution:0.1, density:1.4})
    fix.setFilterData({groupIndex: this._groupIndex, categoryBits:CollisionGroup.all, maskBits:CollisionGroup.all })
    turret.setFilterData({groupIndex:0, categoryBits:CollisionGroup.none, maskBits:CollisionGroup.none })
    body.setSleepingAllowed(false)
    return body
  }

  public constructor(world:IWorld, position:IV2, orientation:number,  protected chassisMat:Material, protected turretMat:Material ){
    super(world, position, orientation)
    //init turret orientation
    this.turretOrient = this.targetOrientation = this.body.getAngle()
  }

  private lastHeading:number = 0

  prePhysics(dT: number): boolean {
    //get local velocity
    var localVelocity = this.body.getLocalVector(this.body.getLinearVelocity())
    //hard adjustments to lateral motions, more chilled reduction in forward/back motion
    var frictionAdjust = this.body.getWorldVector(planck.Vec2({x: localVelocity.x * -this.body.getMass() * 3, y: -50 * localVelocity.y}))
    this.body.applyForce(frictionAdjust, this.body.getWorldCenter())

    const newHeading = this.body.getAngle()
    //calculate turn since last refresh
    const turn = newHeading - this.lastHeading 
    //store new heading...
    this.lastHeading = newHeading

    //apply turn to current orientation
    this.turretOrient -= turn
    //normalize orientation
    if (this.turretOrient < -Math.PI) { 
      this.turretOrient += 2* Math.PI
    }
    if (this.turretOrient > Math.PI) { 
      this.turretOrient -= 2* Math.PI
    }

    //get difference from target orientation
    let diff = this.targetOrientation - this.turretOrient
    //if over 180 degrees switch direction
    if (diff > Math.PI || diff < -Math.PI){
      diff = -diff
    }
    //get smallest max turn or 
    const maxTurn = dT * 0.002
    diff = Math.min(maxTurn, diff)
    diff = Math.max(-maxTurn, diff)
    //apply new turn
    this.turretOrient += diff

    return true
  }


  collision(other: IEntity) { 
    //console.log("tank collision!")
  }
 
  initDraw(world:IWorld):void{

const scene = world.renderer.getScene()

    //get a new tank number
    const tankNumber = Tank.tankNo++
    this.chassis  = Tank.chassisMesh.clone(`chassis_${tankNumber}`, world.renderer.getRoot())
    this.chassis.rotationQuaternion = null

    this.turret =  Tank.turretMesh.clone(`turret_${tankNumber}`, this.chassis)
    this.turret.setPositionWithLocalVector(new Vector3(0.7,1.0,0))
    this.turret.rotationQuaternion = null

    this.fairingLeft = Tank.fairingMesh.clone(`fairing_l_${tankNumber}`, this.chassis)
    this.fairingRight = Tank.fairingMesh.clone(`fairing_r_${tankNumber}`, this.chassis)
    
    this.fairingLeft.setPositionWithLocalVector(new Vector3(0,0,0))
    this.fairingRight.setPositionWithLocalVector(new Vector3(0,0,-4)) 
    
    this.trackLeft = Tank.trackMesh.clone(`track_l_${tankNumber}`, this.fairingLeft)
    this.trackRight = Tank.trackMesh.clone(`track_r_${tankNumber}`, this.fairingRight)
    
    this.trackLeft.material = Tank.trackMesh.material.clone(`trackmat_l_${tankNumber}`)
    this.trackRight.material = Tank.trackMesh.material.clone(`trackmat_r_${tankNumber}`)

    this.printLeft = Tank.printMesh.clone(`print_l_${tankNumber}`, this.chassis)
    this.printRight = Tank.printMesh.clone(`print_r_${tankNumber}`, this.chassis)

    this.printLeft.setPositionWithLocalVector(new Vector3(0,0,0)) 
    this.printRight.setPositionWithLocalVector(new Vector3(0,0,-4)) 

    world.ground.registerTrackMesh(this.printLeft)
    world.ground.registerTrackMesh(this.printRight)

    this.gun = this.turret.getChildMeshes(true)[0]
    world.renderer.getScene().addMesh(this.chassis)


    this.gun.material = this.turretMat 
    this.turret.material = this.turretMat
    this.chassis.material = this.chassisMat 
    this.fairingLeft.material = this.chassisMat 
    this.fairingRight.material = this.chassisMat 



    //nameplate texture
    const outputplaneTexture = new BABYLON.DynamicTexture(`nameplate_tex_${tankNumber}`,{  width:256, height:32 }, scene, true);
    outputplaneTexture.hasAlpha = true
    outputplaneTexture.drawText(this.name, null, 28, "normal 28px verdana", "white", "transparent");

    //material
    const mat = new StandardMaterial("outputplane", scene)
    mat.alpha = 1
  //  mat.alphaMode = Material.MATERIAL_ALPHATEST
   // mat.alphaCutOff = 0.5
    mat.diffuseTexture = outputplaneTexture
    mat.specularColor = new BABYLON.Color3(0, 0, 0)
    mat.emissiveColor = new BABYLON.Color3(1, 1, 1)
    mat.backFaceCulling = false


    //create a mesh 
    this.namePlateMesh = MeshBuilder.CreatePlane(`nameplate_${tankNumber}`, {width:20, height:4, updatable:false}, scene)
    this.namePlateMesh.billboardMode = AbstractMesh.BILLBOARDMODE_ALL
    this.namePlateMesh.position = new BABYLON.Vector3(0, 8, 0)
    //rotate it cos otherwise the text it mirrored 
    this.namePlateMesh.rotate(Vector3.Up(), Math.PI)
    this.namePlateMesh.material = mat
    //attach to the chassis so it hangs above our tank
    this.chassis.addChild(this.namePlateMesh)

this.showNamePlate(5000)

  }

  static scratch:Vector3 = new Vector3()


  public showNamePlate(time:number):void{
    this.namePlateMesh.setEnabled(true)
    this.showNameTime = time
  }

  preDraw(dT:number): void {
    const pos = this.body.getPosition()
    this.chassis.setAbsolutePosition(Tank.scratch.set(pos.x, 0, pos.y))
    const ang = this.body.getAngle()
    this.chassis.rotation.set(0, (-0.5 * Math.PI) -  ang ,0)

    const angularVel = this.body.getAngularVelocity() / 1000

    const localVelocity = this.body.getLocalVector(this.body.getLinearVelocity())
    const angularComponent = Math.tan(angularVel) * 2650 


    this.leftDrive = localVelocity.y - angularComponent
    this.rightDrive = localVelocity.y + angularComponent
    //console.log( `av: ${angularVel} lv: ${localVelocity} angular component: ${angularComponent}`)
    

    this.leftOffset -= ((dT * this.leftDrive)   * (1/10000))
    this.rightOffset -= ((dT * this.rightDrive) * (1/10000))


    this.trackLeft.material.getActiveTextures().forEach((t:Texture)=>{ t.vOffset = this.leftOffset })
    this.trackRight.material.getActiveTextures().forEach((t:Texture)=>{ t.vOffset = this.rightOffset })

    //update print meta data
    this.printLeft.metadata.offset  = this.leftOffset
    this.printRight.metadata.offset  = this.rightOffset
    //finally orient the turret
    this.turret.rotation.set(0, this.turretOrient + ang, 0 )
    //console.log(`tank:${ang} target:${this.targetOrientation} turret:${this.turretOrient} `)

    if (this.showNameTime > 0){
      this.showNameTime -= dT
      if (this.showNameTime <= 0){
        this.namePlateMesh.setEnabled(false)
      }
    }


  }
  
  public static loadAssets(assetMan:AssetsManager):void {
    assetMan.addMeshTask("loadtank","","/assets/", "tankmk9.gltf").onSuccess = (task)=>{
        Tank.trackMesh = task.loadedMeshes.filter(v=>v.id === "track_primitive0")[0]
        Tank.fairingMesh = task.loadedMeshes.filter(v=>v.id === "track_primitive1")[0]
        Tank.chassisMesh = task.loadedMeshes.filter(v=>v.id === "chassis")[0]
        Tank.turretMesh = task.loadedMeshes.filter(v=>v.id === "turret")[0]
        Tank.printMesh = task.loadedMeshes.filter(v=>v.id==="trackprint")[0]
      }
  }
}