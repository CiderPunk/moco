import { SocketComms } from "../socketcomms"
import { PeerConnection } from "./peerconnection"
import { IPeerConnectionManager, IPeerConnection, ICommsEntityManager } from "./clientinterfaces"


export class PeerConnectionManager implements IPeerConnectionManager{


  public onConnecting: (peerConn:IPeerConnection)=> void
  public onConnected: (peerConn:IPeerConnection) => void
  public onDisconnected: (peerConn:IPeerConnection)=> void

  protected readonly peerConns = new Map<string, PeerConnection>()

  public constructor(protected readonly socket:SocketIOClient.Socket, public readonly entMan:ICommsEntityManager, config:any, ){
    //signal - route to correct peer connection
    socket.on(SocketComms.MessageType.InitConnection, (req:SocketComms.Payload.InitConnection)=>{ 
      //set next entity id in ent manager
      this.entMan.setNextEntId(req.entId)
      const peerConn =  new PeerConnection(this, req.primary, req.key, this.socket, req.peerInfo, req.iceConfig)
      this.peerConns.set(req.key, peerConn)
      //trigger on connecting
      if (this.onConnecting){
        this.onConnecting(peerConn)  
      }
    })  

    socket.on(SocketComms.MessageType.PeerSignal, (req:SocketComms.Payload.PeerSignal)=>{
      //try get the connection
      var peerConn = this.peerConns.get(req.key)
      if (!peerConn) {
        //only allow connections already created
        throw new Error("Unknown peer connection key")
      }     
      peerConn.signal(req.data)
    })
  }

  connected(peerConn:IPeerConnection):void {
    if (this.onConnected){
      this.onConnected(peerConn)
    }
  }
  
  dropConnection(key: string) :void{
    const peerConn = this.peerConns.get(key)
    if (peerConn!= null && this.onDisconnected){
      this.onDisconnected(peerConn)
    }
    this.peerConns.delete(key)
  }

  public getConnections():Array<IPeerConnection>{ 
    return Array.from(this.peerConns.values())
  }

  update(t: number, dt: number) {
    this.peerConns.forEach((pc:PeerConnection)=>{ pc.update(t,dt)})
  }


}