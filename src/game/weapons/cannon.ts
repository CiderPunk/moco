import { IWeapon, IWorld, IShooter, IPhysicsObject, IEntity, IsPhysicsObject } from "../interfaces";
import { V2 } from "../../common/math/v2";

/**
 * simple cannon
 */
export class Cannon implements IWeapon{
  
  timeToShoot:number

  constructor(protected readonly world:IWorld, protected readonly fireDelay:number = 500, protected readonly muzzleVelocity:number = 60){
    this.timeToShoot = 0
   }

   private static tempVel:V2 = new V2()
   private static tempPos:V2 = new V2()


  update(dT:number, trigger:boolean, shooter:IShooter){
    this.timeToShoot-=dT
    if (this.timeToShoot <= 0 && trigger){
      this.fire(shooter)
      console.log("fire!")
      this.timeToShoot = this.fireDelay
    }
  }

  fire(shooter:IShooter):void {
    //do shoot
    const shell = this.world.spawnShell(shooter)
    Cannon.tempVel.setAngle((0.5* Math.PI) - shooter.getWeaponOrientation(), this.muzzleVelocity ).add(shooter.getVelocity())
    //add a bit of veloocity to keep the shell from appearing inside the gun
    Cannon.tempPos.setV2(shooter.getWeaponPosition()).addScale(Cannon.tempVel, 0.03)
    shell.init(  Cannon.tempPos,Cannon.tempVel, shooter, 2)

    if (IsPhysicsObject(shooter)){
      shooter.applyImpulse(Cannon.tempVel.scale(-shell.getMass()), Cannon.tempPos)
    }
  }

}