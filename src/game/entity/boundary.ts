import { IEntity, IRenderer, IWorld } from "../interfaces";
import planck = require("planck-js");
import { CollisionGroup, Constants } from "../constants";
import { IV2 } from "../../common/interfaces";
import { AbstractMesh, AssetsManager,  Texture } from "babylonjs";
import { EntityType } from "../enum";
import { Vec2 } from "planck-js";

export class Boundary implements IEntity{
  static meshes: AbstractMesh[];


  get type(): EntityType { return EntityType.boundary }
  
  getPosition(): IV2 {
    return {x:0, y:0}
  }
  getVelocity(): IV2 {
    return {x:0, y:0}
  }

  protected readonly body: planck.Body;

  prePhysics(dT: number):boolean { return true }

  constructor(world:planck.World, protected size:IV2){
    this.body = world.createBody({type: planck.Body.STATIC, position: planck.Vec2(0,0) })
    //create walls around world
    this.body.setUserData(this)
    const playerBoundsShape = new planck.Chain([ new planck.Vec2(0,0), new planck.Vec2(size.x, 0),new planck.Vec2(size), new planck.Vec2(0, size.y) ], true)
    const playerFix = this.body.createFixture({shape: playerBoundsShape, restitution: 1})
    playerFix.setFilterData({ 
      groupIndex: 0, 
      categoryBits:CollisionGroup.boundary,
      maskBits: CollisionGroup.all 
    })


    const halfGoalSize = Constants.GoalSize / 2
    const goalDepth = Constants.GoalDepth


    const ballBoundsShape = new planck.Chain([ 
      new planck.Vec2(0,0), new planck.Vec2(size.x, 0), 
      new planck.Vec2(size.x, (size.y/2)-halfGoalSize), new planck.Vec2(size.x + goalDepth, (size.y/2)-halfGoalSize),new planck.Vec2(size.x + goalDepth, (size.y/2)+halfGoalSize),new planck.Vec2(size.x, (size.y/2)+halfGoalSize),
      new planck.Vec2(size),new planck.Vec2(0, size.y),
      new planck.Vec2(0, (size.y/2)+halfGoalSize), new planck.Vec2(-goalDepth, (size.y/2)+halfGoalSize),new planck.Vec2(-goalDepth, (size.y/2)-halfGoalSize),new planck.Vec2(0, (size.y/2)-halfGoalSize) ], true)
    const ballFix = this.body.createFixture({shape: ballBoundsShape, restitution: 1})
    ballFix.setFilterData({ 
      groupIndex: 0, 
      categoryBits:CollisionGroup.ballBoundary,
      maskBits: CollisionGroup.ball 
    })

    this.body.setPosition(new planck.Vec2(-size.x/2, -size.y / 2))
  }
  setPosition(pos: IV2, orientation: number, resetVelocity: boolean) {
    this.body.setPosition(new Vec2(pos))
  }

  collision(other: IEntity) {}

  initDraw(world: IWorld): void { 
    const root = world.renderer.getRoot()
    const scene = world.renderer.getScene()
    Boundary.meshes.forEach((m)=>{ scene.addMesh(m)})  
  }

  preDraw(dT:number): void {  }


  static textures = new Array<Texture>()

  public static loadAssets(assetMan:AssetsManager):void {

    const task = assetMan.addMeshTask("loadstadia","","/assets/", "stadia2.gltf")
    task.onSuccess = (task)=>{
      Boundary.meshes =task.loadedMeshes.filter(V=>V.id.startsWith('goal') || V.id.startsWith('perim') || V.id === 'outer' )
    }

  }

}