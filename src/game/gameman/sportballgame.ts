
import { IController } from "../../common/interfaces"
import { MoCoComms } from "../../common/mococomms"
import {  IGameManager,  IPlayer, ITeam, IWorld } from "../interfaces"
import { TeamSetings } from "./teamsettings"
import { Team } from "./team"
import { Colours, Constants } from "../constants"
import { Boundary } from "../entity/boundary"
import { Ball } from "../entity/ball"
import { Goal } from "../entity/goal"

export class SportBallGame implements IGameManager{


  readonly teams:Array<ITeam>
  lastAdded: any;
  ball: Ball;
  
  resetTime: number = 0
  
  
  public constructor(readonly owner:IWorld){
    this.teams = [
      new Team(this, "Digital", {name: "red", colour: Colours.red}, Math.PI/2, TeamSetings.colours.map(c=>{return c}), TeamSetings.startPositions.map((p)=>{ return { x:p.x, y:p.y }})),
      new Team(this, "Print", {name: "blue", colour: Colours.blue}, -Math.PI/2, TeamSetings.colours.map(c=>{return c}), TeamSetings.startPositions.map((p)=>{ return { x:-p.x, y:-p.y }}))
    ]
    this.updateScoreUI()
  }

  

  updateScoreUI(  ){
    this.owner.game.setScore({ 
      teamA: this.teams[0].getScore(), 
      teamB: this.teams[1].getScore(), 
      teamAName:this.teams[0].name, 
      teamBName:this.teams[1].name
    })
  }

  goalScored(team:ITeam, player:IPlayer ): void {
    this.ball.setActive(false)
    this.resetTime = Constants.GoalResetTime
    this.updateScoreUI()
    this.owner.game.setGoalState({ score:true, teamName:team.name, scorerName:player?.playerInfo.name, wasOwnGoal: (player.team !== team) })
    this.owner.startOrbitCam(player, 5000)
    player.showNamePlate(5000)
  }

  public reset(hardReset:boolean = false){
    this.teams.forEach(t=>{t.reset(hardReset)})
    this.ball.setPosition({x:0, y:0}, 0, true)
    this.ball.setActive(true)
    this.owner.game.setGoalState({ score:false, teamName:'', scorerName:'', wasOwnGoal:false })
    this.updateScoreUI()
  }

  public update (dT:number){
    if (this.resetTime > 0){
      this.resetTime -= dT
      if (this.resetTime < 0){
        this.reset()
      }
    }
  }


  public initWorld(world:IWorld){
    world.addEnt(new Boundary(world.world, world.size))
    this.ball = new Ball(world, this, {x:0,y:0})
    world.addEnt(this.ball)
    world.addEnt(new Goal(world, this.teams[0], {x:(-world.size.x/2)-Constants.BallRadius - (Constants.GoalDepth / 2), y:0} ))
    world.addEnt(new Goal(world, this.teams[1], {x:(world.size.x/2)+Constants.BallRadius + (Constants.GoalDepth / 2), y:0} ))
  }

  addPlayer(control: IController, playerInfo:MoCoComms.PlayerInfo):IPlayer {
    return this.teams[this.getNextTeam()].addPlayer(this.owner, control, playerInfo)
  }


  // returns team to add next player to index
  private getNextTeam():number{
    //get player difference
    const playerDiff = this.teams[0].getPlayerCount() - this.teams[1].getPlayerCount()
    //if playerdifference is zero get goal difference
    const order = playerDiff == 0 ? this.teams[0].getScore() - this.teams[1].getScore() : playerDiff
    //finally if still 0 add player to the not-last-added team
    this.lastAdded = (order < 0 ? 0 : (order > 0 ? 1 : (this.lastAdded == 0 ? 1 : 0)))
    //return...
    return this.lastAdded
  }



}