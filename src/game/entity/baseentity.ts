import { IEntity, IRenderer, IWorld } from "../interfaces";
import { IV2 } from "../../common/interfaces";
import { create } from "qrcode";
import { EntityType } from "../enum";
import { Vec2 } from "planck-js";

export abstract class BaseEntity implements IEntity{

  //public readonly position:V2
  protected readonly body: planck.Body
  public constructor(world:IWorld, position:IV2, orientation:number = 0){
    this.body = this.createBody(world.world, position, orientation)
    this.body.setUserData(this);

  }


  setPosition(pos: IV2, orientation: number, resetVelocity: boolean) {
    this.body.setPosition(new Vec2(pos))
    this.body.setAngle(orientation)
    this.body.setAngularVelocity(0)
    if (resetVelocity){
      this.body.setLinearVelocity(Vec2.zero())
    }
  }

  abstract createBody(world:planck.World, position:IV2, orientation:number):planck.Body

  abstract initDraw(world: IWorld): void 
  abstract collision(other: IEntity) 
  abstract prePhysics(dT: number): boolean
  abstract preDraw(dt: number): void

  getPosition(): IV2 { return this.body.getPosition() }
  getVelocity(): IV2 { return this.body.getLinearVelocity() }
  
  
  abstract get type():EntityType

}