import { AbstractMesh, AssetsManager, Vector3, MeshBuilder, LinesMesh, Space } from "babylonjs"
import planck = require("planck-js")
import { IV2 } from "../../common/interfaces"
import { CollisionGroup, Constants } from "../constants"
import { EntityType } from "../enum"
import { SportBallGame } from "../gameman/sportballgame"
import { IBall, IEntity, IPlayer, IShell, IWorld } from "../interfaces"
import { BaseEntity } from "./baseentity"

export class Ball extends BaseEntity implements IEntity, IBall{
  fixture: planck.Fixture
  get type(): EntityType { return EntityType.ball }
  

  //static loaded meshes
  private static _ballMesh:AbstractMesh  

  private mesh:AbstractMesh
  points: Vector3[]
  lines: LinesMesh
  vectorOpts: { points: Vector3[]; updatable: boolean, instance:LinesMesh }

  public constructor(world:IWorld, manager:SportBallGame, position:IV2){
    super(world, position)
  }

  lastTouch: IPlayer = null


  public createBody(world:planck.World, position:IV2, orientation:number){
    const body = world.createBody({type: planck.Body.DYNAMIC, position: new planck.Vec2(position)})
    const shape = new planck.Circle(new planck.Vec2(0,0), Constants.BallRadius)
    this.fixture = body.createFixture({shape: shape, restitution:0.8, density:0.5, friction:1})
    this.setActive(true)
    //this.fixture.setFilterData({groupIndex: 1, categoryBits:CollisionGroup.ball, maskBits:CollisionGroup.tank | CollisionGroup.projectile | CollisionGroup.ballBoundary})
    body.setSleepingAllowed(false)
    return body
  }


  prePhysics(dT: number): boolean {
    //get local velocity
    var localVelocity = this.body.getLocalVector(this.body.getLinearVelocity())
    //hard adjustments to lateral motions, more chilled reduction in forward/back motion
    var frictionAdjust = this.body.getWorldVector(planck.Vec2({x:-2 * localVelocity.x, y: -2 * localVelocity.y}))
    this.body.applyForce(frictionAdjust, this.body.getWorldCenter())
 
    return true
  }

  collision(other: IEntity) { 
    switch (other.type){
      case EntityType.player:
        this.lastTouch = other as IPlayer
        console.log(`ball touch ${this.lastTouch.playerInfo.name}`)
        break

      case EntityType.shell:
        const ent = (other as IShell).shooter
        if (ent.type === EntityType.player){
          this.lastTouch = ent as IPlayer
          console.log(`ball shot ${this.lastTouch.playerInfo.name}`)
        }
        break
    }
  }
 

  setActive(state:boolean){
    //enables or disables collisions
    this.fixture.setFilterData({groupIndex: 1, categoryBits:CollisionGroup.ball,  maskBits: (state ? CollisionGroup.tank | CollisionGroup.projectile | CollisionGroup.ballBoundary : CollisionGroup.none) })

  }

  initDraw(world:IWorld):void{
    this.mesh = Ball._ballMesh.clone("ball", world.renderer.getRoot())
    world.renderer.getScene().addMesh(this.mesh)

    /*
    this.points = [
      new Vector3(0,0,0),
      new Vector3(0,0,0),
      new Vector3(0,0,0)
    ]
  
    this.vectorOpts = {points:this.points, updatable:true, instance:null}
    const lineMesh = MeshBuilder.CreateLines("ballvector", this.vectorOpts)
    this.vectorOpts.instance = lineMesh
*/
  }

  static scratch:Vector3 = new Vector3()

  preDraw(dT:number): void {
    const pos = this.body.getPosition()
    this.mesh.setAbsolutePosition(Ball.scratch.set(pos.x, 3, pos.y))

    const vect = this.body.getLinearVelocity()
    const speed = vect.length()
    
    Ball.scratch.set(-vect.y,0,vect.x)
    
    this.mesh.rotate(Ball.scratch, speed * 0.003, Space.WORLD)
  
    /*
    this.points[0].set(pos.x + vect.x,2,pos.y+vect.y)
    this.points[1].set(pos.x,2,pos.y)
    this.points[2].set(pos.x -vect.y,2,pos.y+vect.x)
    MeshBuilder.CreateLines("ballvector", this.vectorOpts)
    */
  }
  
  public static loadAssets(assetMan:AssetsManager):void {
    const task = assetMan.addMeshTask("loadball","","/assets/", "ball.gltf")
    task.onSuccess = (task)=>{
        this._ballMesh = task.loadedMeshes.filter(v=>v.id === "ball")[0]
      }
  }
}