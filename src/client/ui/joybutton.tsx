import { h, Component, ComponentChild } from "preact"
import { ICanvasDrawControl } from "../interfaces"
import { V2 } from "../../common/math/v2"

export interface JoyButtonProps{
  cssClass?:string
  updateValue?:(boolean)=>void
}


export class JoyButton extends Component<JoyButtonProps> {

  private firstTouchId:number
  private defClass:string

  public constructor(props:JoyButtonProps){
    super(props)
  }

  render(props: Readonly<JoyButtonProps>): ComponentChild { 
    this.defClass = "joybutton " + props.cssClass
    return <a class={this.defClass} 
      onMouseDown={(ev:MouseEvent)=>this.handleMouseDown(ev)} 
      onMouseUp={()=>this.handleMouseStop()}     
      onTouchStart={(ev:TouchEvent)=>this.handleTouchStart(ev)} 
      onTouchEnd={(ev:TouchEvent)=>this.handleTouchStop(ev)}
      >
    </a>
  }


  private activate(state:boolean){
    this.props.updateValue(state)
    this.base.className = this.defClass + (state ? " active" : "")
  }

  protected handleMouseStop(): void {
    this.activate(false)
  }

  protected handleMouseDown(ev: MouseEvent): void {
    this.activate(true)
  }

  protected handleTouchStart(ev: TouchEvent) {
    ev.preventDefault()
    if (ev.targetTouches.length < 2){
      this.firstTouchId = ev.targetTouches[0].identifier
      this.activate(true)
    }
  }

  protected handleTouchStop(ev:TouchEvent): void {
    for(var i = 0; i < ev.changedTouches.length; i++){
      if (ev.changedTouches[i].identifier == this.firstTouchId){
        this.activate(false)
      }
    }
  }

}

  
