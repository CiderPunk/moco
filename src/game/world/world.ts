import * as planck from "planck-js"
import { IScreenSyncd, IV2, IController } from "../../common/interfaces"
import { Constants } from "../constants"
import { IEntity, IWorld, IRenderer, IGround, IGame, IScore, IGameManager, IPlayer } from "../interfaces"
import { FastArray } from "../../common/utils/fastarray"
import { Boundary } from "../entity/boundary"
import { Tank } from "../entity/tank"
import { ShellPool, Shell } from "../entity/shell"
import { LocalControl } from "../localcontrol"
import {  AssetsManager, Camera,  DirectionalLight, FreeCamera,  RenderTargetTexture,  Vector3 } from "babylonjs"
import { Ground } from "./ground"
import { _ThinInstanceDataStorage } from "babylonjs/Meshes/mesh"
import { Ball } from "../entity/ball"
import { ICommsEntityManager, IPeerConnection } from "../../comms/client/clientinterfaces"
import { MoCoComms } from "../../common/mococomms"
import { SportBallGame } from "../gameman/sportballgame"
import { TankMaterialManager } from "../materials/tankmaterialmanager"
import { V2 } from "../../common/math/v2"
import { DepthOfFieldBlurPostProcess } from "babylonjs/PostProcesses/depthOfFieldBlurPostProcess"
import { CommsEntityManager } from "../../comms/client/commsentitymanager"


export class World implements IScreenSyncd, IWorld{

  //hard rest the game
  resetGame() {
    this.gameManager.reset(true)
    this.ground.reset()
  }

  public readonly world:planck.World 
  tickTime:number
  scratch:FastArray<IEntity>
  ents:FastArray<IEntity>
  private _renderer:IRenderer
  shellPool:ShellPool

  groundTexRenderTarget: RenderTargetTexture
  resetGrass: boolean
  ground: IGround
  camera: FreeCamera
  worldTime: number = 0
  readonly target= new Vector3()
  readonly perturbedTarget= new Vector3()
  score:IScore
  gameManager:IGameManager
  orbitCamera: FreeCamera
  orbitTime: number
  orbitTarget: IEntity

  
  get renderer():IRenderer{
    return this._renderer
  }

  public constructor(public readonly game:IGame, public readonly size:IV2 = {x:200, y:150}){
    this.world = new planck.World()
    this.world.on("begin-contact",this.Contact)
    this.tickTime = 0
    this.ents = new FastArray<IEntity>()
    this.scratch = new FastArray<IEntity>()
  
    this.score = { teamA:0, teamB:0, teamAName:"", teamBName:"" }
    this.gameManager = new SportBallGame(this)
    this.gameManager.initWorld(this)
  }

  public addEnt(ent:IEntity){
    this.ents.push(ent)
  }

  
  public initDraw(renderer:IRenderer):void{
    this._renderer = renderer
    //init ground
    this.ground = new Ground(this)
    const scene = this.renderer.getScene()
    this.ents.forEach((ent:IEntity)=>ent.initDraw(this))
    //set up lights
    const sun = new DirectionalLight("DirectionalLight", new Vector3(1, -1, -1), renderer.getScene());
    sun.intensity = 2


    //set up lights
    const sun2 = new DirectionalLight("DirectionalLight", new Vector3(1, 1, -1), renderer.getScene());
    sun2.intensity = 1

    this.camera = new FreeCamera("Camera1", new Vector3(0,0,100), scene,true)
    this.camera.position.set(0,800,800 )
    this.camera.fov = 0.11
   //this.camera.fov = 0.03
    this.target.set(0,0,0)
    this.camera.setTarget(this.target)


    this.orbitCamera = new FreeCamera("orbitCam", new Vector3(0,0,0), scene, false)
    this.orbitCamera.position.set(0,20,50 )
    this.orbitCamera.setTarget(new Vector3(0,0,0))
    this.orbitCamera.fov = 0.6
    //scene.activeCamera = this.orbitCamera
    //shell pool
    this.shellPool = new ShellPool(this)
  }

  spawnLocalTank() { 

    const localCon = new LocalControl()
    //local TEST player
  
    localCon.setPlayer(this.addPlayer(localCon))

  }

  public spawnShell():Shell {
    const shell = this.shellPool.GetNew()
    shell.initDraw(this)
    this.ents.push(shell)
    return shell
  }

  public addPlayer(control:IController, pc?:IPeerConnection):IPlayer{
    //read player info from peerconn....
    const playerInfo = pc?.peerInfo as MoCoComms.PlayerInfo ?? { name:'Local Player', uuid:''}
    //add player to game manager
    const tankEnt = this.gameManager.addPlayer(control, playerInfo)

    this.ents.push(tankEnt)
    tankEnt.initDraw(this)
    return tankEnt
  }

  refresh(time: number, dt: number): void {
    const delta = time - this.tickTime
    //skip if we're really late..
    if (delta > 200){
      this.tickTime = time
    }

    while(this.tickTime < time){
      let ent:IEntity  

      //update game manager
      this.gameManager.update(Constants.TickLength * 1000)
      //pre-phys all our ents
      while(ent = this.ents.pop()){
        if (ent.prePhysics(Constants.TickLength * 1000)){
          //moce ents to scratch if they persist
          this.scratch.push(ent)
        }
      }
      //swap scratch for main ent list...
      this.ents.swap(this.scratch)

      //physics step
      this.world.step(Constants.TickLength )
      //console.log(`tick ${this.tickTime}`)
      //increase tick time..
      this.tickTime+= (Constants.TickLength * 1000)

    }
  }


  startOrbitCam(target:IEntity, time:number){

    this.orbitTime = time
    this.orbitTarget = target

  }

  preDraw(dT:number) {
    this.worldTime += dT

    if (this.orbitTime > 0){
      this.orbitTime -= dT
      
      const target = new V2(this.orbitTarget.getPosition())
      const offset = new V2({x:0, y:40})



      offset.rotate(this.orbitTime /1000)
      
      
      this.orbitCamera.position.set(target.x+offset.x, 30, target.y + offset.y,)
      this.orbitCamera.setTarget(new Vector3(target.x,5,  target.y))
      this.renderer.getScene().activeCamera = this.orbitCamera
    }
    else{
      this.renderer.getScene().activeCamera = this.camera
      this.camera.position.set(0 + (Math.sin(this.worldTime / 17700.0) * 2.0) ,800 ,800 + Math.cos(this.worldTime / 23770.0) * 2.0)
      this.perturbedTarget.set(this.target.x + (Math.sin(this.worldTime / 12000.0) * 4.0),this.target.y, this.target.z + Math.cos(this.worldTime / 7700.0) * 3.0)
      this.camera.setTarget(this.perturbedTarget)
    }
    this.ents.forEach((ent:IEntity)=>ent.preDraw(dT)) 
    //this.renderer.getCamera().
  }

  protected Contact(contact: planck.Contact): void {
    var ent1 = contact.getFixtureA().getBody().getUserData() as IEntity;
    var ent2 = contact.getFixtureB().getBody().getUserData() as IEntity;

//contact.getTangentSpeed()

    if (ent1 && ent2){
      //TODO: check for bullets....
      ent1.collision(ent2)
      ent2.collision(ent1)
    }    
  }


  loadAssets(assetMan:AssetsManager):void{
    Tank.loadAssets(assetMan)  
    Boundary.loadAssets(assetMan)
    Ground.loadAssets(assetMan)
    Ball.loadAssets(assetMan)
    TankMaterialManager.loadAssets(assetMan)
  }

}