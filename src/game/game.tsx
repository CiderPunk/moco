import { h, Component, ComponentChild } from "preact"
import * as io from "socket.io-client"
import { Join} from "./ui/join"
import { PlayerList } from "./ui/playerlist";
import { PeerConnectionManager } from "../comms/client/peerconnectionmanager";
import { Config } from "../common/config";
import { MoCoComms } from "../common/mococomms";
import { IPeerPlayer, IGame, IScore, IWorld, IGoalState } from "./interfaces";
import { PeerPlayer } from "./PeerPlayer";
import { IPeerConnection, ICommsEntity } from "../comms/client/clientinterfaces";
import { CommsEntityManager } from "../comms/client/commsentitymanager";
import {  MoCoEntityFactory } from "../common/mocogameentityfactory";
import { RegisterList } from "../common/utils/registerlist";
import { IScreenSyncd,  IRemoteController } from "../common/interfaces";

import { PcmProxy } from "../common/pcmproxy";
import { RenderView } from "./ui/renderview";
import { DebugView } from "./ui/debugview";
import { ScoreDisplay } from "./ui/scoredisplay";
import { GoalStatus } from "./ui/goalstatus";
import { World } from "./world/world";
import { Stage } from "babylonjs/sceneComponent";

export interface GameProps{

}

interface GameState{
  message?:string
  gameKey:string
  players:Map<string,IPeerPlayer>
  showDebug:boolean
  score:IScore
  goalState:IGoalState
}

export class Game extends Component<GameProps, GameState> implements IGame{

  protected readonly players:Map<string,IPeerPlayer> 
  public readonly pcm:PeerConnectionManager
  public readonly socket:SocketIOClient.Socket
  public readonly entman:CommsEntityManager
  private readonly timedComponents:RegisterList<IScreenSyncd>
  protected reqAnimFrame:number
  protected world:World
  pcmProxy:PcmProxy
  showDebug: boolean;

  public constructor(props:GameProps){
    super(props)

    this.timedComponents = new RegisterList<IScreenSyncd>()
    this.players = new Map<string,IPeerPlayer>()
    this.state = {  players: this.players, gameKey:"", showDebug:false, score:{ teamAName:'', teamBName:'', teamA:0, teamB:0}, goalState:{ score:false, teamName:null, scorerName:null, wasOwnGoal:false}} 
    this.socket = io()    

    this.entman = new CommsEntityManager(new MoCoEntityFactory())
    this.pcm = new PeerConnectionManager(this.socket, this.entman, Config.iceConfig)
    this.pcm.onConnected = (pc:IPeerConnection)=>{ this.playerConnected(pc) }
    this.pcm.onDisconnected = (pc:IPeerConnection)=>{ this.playerDisconnected(pc) }

    this.socket.on(MoCoComms.Events.CreateGameResponse, (resp:MoCoComms.CreateGameResponse)=>{ this.startGame(resp)})
    this.socket.emit(MoCoComms.Events.CreateGameRequest, {} as MoCoComms.CreateGameRequest)
    this.world = new World(this)

    this.entman.onNetCreate=(peer:IPeerConnection, ent:ICommsEntity )=>{
      const controller = ent as IRemoteController
      if (controller){
        this.world.addPlayer(controller, peer)
      }
    }

    this.componentDidMount = ()=>{
      this.reqAnimFrame = window.requestAnimationFrame(()=>{this.doFrame()})
      this.timedComponents.register(this.world, 0)
      this.timedComponents.register(this.pcmProxy = new PcmProxy(this.pcm),10)
    }

    this.componentWillUnmount= ()=>{
      //this.timedComponents.deregister(this.world)
      window.cancelAnimationFrame(this.reqAnimFrame)
      this.timedComponents.deregister(this.world)
      this.timedComponents.deregister(this.pcmProxy)
    }
  }

  setGoalState(goalState: IGoalState) {
    this.setState({ goalState:goalState})
  }

  setScore(score: IScore) {
    this.setState({ score:score})
  }

  //create a new player
  playerConnected(pc: IPeerConnection) {
    const player = new PeerPlayer(this, pc)
    this.players.set(player.uuid, player)
    this.setState({ players: this.players } )
    console.log(`Player ${player.name} ${player.uuid} connected`)
  }

  playerDisconnected(pc: IPeerConnection) {
    const playerInfo = pc.peerInfo as MoCoComms.PlayerInfo
    if (playerInfo){
      //remove player and redraw player list
      this.players.delete(playerInfo.uuid)
      this.setState({ players: this.players } )
      console.log(`Player ${playerInfo.name} ${playerInfo.uuid} disconnected`)
    }
  }

  startGame(resp: MoCoComms.CreateGameResponse) {
    this.setState({ gameKey: resp.key})
  }

  render(props: Readonly<GameProps>, state?: Readonly<GameState>, context?: any): ComponentChild {
    const debugView = state.showDebug ? <DebugView 
    register={(com:IScreenSyncd)=>this.timedComponents.register(com, 0)}
    deregister={(com:IScreenSyncd)=>this.timedComponents.deregister(com)} world={this.world}/> : ""

    return (
    <div class="gamecontainer">
    <RenderView 
      register={(com:IScreenSyncd)=>this.timedComponents.register(com, 2)}
      deregister={(com:IScreenSyncd)=>this.timedComponents.deregister(com)} world={this.world}/>

      {debugView}
      
 
      <GoalStatus goalState={state.goalState} />
      <PlayerList players={state.players}/>
      <Join gameKey={state.gameKey}/>
      <ScoreDisplay  teamAName={state.score.teamAName} teamBName={state.score.teamBName} teamAScore={state.score.teamA} teamBScore={state.score.teamB}  />
      <div class="helpers">
        <p>
          <a href="#" onClick={(e)=>{ e.preventDefault(); this.setState({ showDebug: !state.showDebug})}}>Debug</a> |
          <a href="#" onClick={(e)=>{ e.preventDefault(); this.world.spawnLocalTank() }}>Local test</a> | 
          <a href="#" onClick={(e)=>{ e.preventDefault(); this.world.resetGame() }}>Reset</a>
        </p>
      </div>
    </div>)
  }


  private lastFrame:number


  protected doFrame():void{
    //queue up next frame
    this.reqAnimFrame = window.requestAnimationFrame(()=>{this.doFrame()})
      
    //get time and last time..
    const  t:number = performance.now()
    const dt = t- this.lastFrame
    this.lastFrame = t

    //lets trigger those timed components
    this.timedComponents.items.forEach((com:IScreenSyncd)=>{
      com.refresh(t, dt)
    })
  }

}
