import { ICommsBuffer, ICommsEntity, IPeerConnection } from "../../comms/client/clientinterfaces";
import { ITankDescription, IV3 } from "../interfaces";
import { MoCoPeerComms } from "./enums";

export class TankDescription implements ICommsEntity, ITankDescription{

  public turretColour:IV3
  public chassisColour:IV3
  public onDisconnected:() => void

  public constructor(public readonly entId:number, public readonly pc?:IPeerConnection){
    this.turretColour = {x:0,y:0,z:0}
    this.chassisColour = {x:0,y:0,z:0}
  }

  get typeId():number { 
    return MoCoPeerComms.EntityType.TankDescrition
  }


  writeState(buffer: ICommsBuffer): void {
    buffer.WriteFloat(this.turretColour.x)
    buffer.WriteFloat(this.turretColour.y)
    buffer.WriteFloat(this.turretColour.z)
    buffer.WriteFloat(this.chassisColour.x)
    buffer.WriteFloat(this.chassisColour.y)
    buffer.WriteFloat(this.chassisColour.z)
  }

  readState(buffer: ICommsBuffer): void {
    this.turretColour.x = buffer.ReadFloat()
    this.turretColour.y = buffer.ReadFloat()
    this.turretColour.z = buffer.ReadFloat()
    this.chassisColour.x = buffer.ReadFloat()
    this.chassisColour.y = buffer.ReadFloat()
    this.chassisColour.z = buffer.ReadFloat()
  }

}
