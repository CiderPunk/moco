export enum PeerMessageType {
  End = 0,
  Start,
  EntityUpdate,
  EntityCreate,
  EntityDestroy
}

export interface IPeerConnectionManager{
  connected(peerConn: IPeerConnection)
  dropConnection(key:string)
  getConnections():Array<IPeerConnection>
  onConnecting: (peerConn:IPeerConnection) => void
  onConnected: (peerConn:IPeerConnection) => void
  onDisconnected: (peerConn:IPeerConnection) => void
  entMan:ICommsEntityManager
}

export interface IPeerConnection{
  peerInfo:any
  heartBeatTime:number
  readonly state:PeerConnectionState   
  onPeerDisconnected: (peerConn:IPeerConnection)=> void
  sendEntity(ent: ICommsEntity, forceFlush?:boolean):void
  flush():void
}



export interface ICommsBuffer{
  owner:IPeerConnection
  ReadFloat():number
  ReadBool():boolean
  ReadTinyInt():number
  ReadInt():number
  ReadUint():number
  ReadBigFloat():number
  WriteBool(val:boolean):void
  WriteTinyInt(val:number):void
  WriteFloat(val:number):void
  WriteUint(val:number):void
  WriteInt(val:number):void
  WriteBigFloat(val:number):void
}

export interface ICommsEntity{
  entId:number
  typeId:number
  writeState(buffer:ICommsBuffer):void
  readState(buffer:ICommsBuffer):void
}

export interface ICommsEntityManager{
  updateEntity(rcvBuffer: ICommsBuffer, pc:IPeerConnection);
  createEntity(typeId:number);
  getNextEntID():number
  setNextEntId(nextId:number):void
  onNetCreate:(IPeerConnection, ICommsEntity)=>void
}

export interface ICommsEntityFactory{
  createEntity(entId:number,typeId:number, pc?:IPeerConnection):ICommsEntity
}

export enum PeerConnectionState{
  Disconnected,
  Connecting,
  Connected,
  Unknown,
}

