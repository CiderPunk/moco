import { h, Component, ComponentChild } from "preact"
import { IScreenSyncd } from "../../common/interfaces"
import { IWorld, IRenderer } from "../interfaces"
import 'babylonjs-loaders'
import { AssetContainer, AssetsManager, Engine, FreeCamera, Node, Scene, TransformNode, Vector3 } from "babylonjs"

export interface RenderViewProps{
  register:(IScreenSyncd)=>void
  deregister:(IScreenSyncd)=>void
  world:IWorld
}

interface RenderViewState{
  
}


export class RenderView  extends Component<RenderViewProps, RenderViewState> implements IScreenSyncd, IRenderer{

  ctx:CanvasRenderingContext2D
  canvas:HTMLCanvasElement

  protected engine:Engine
  protected scene:Scene = null
  protected assetMan:AssetsManager
  protected container:AssetContainer
  protected camera:FreeCamera
  protected rootNode:TransformNode


  public getCamera():FreeCamera{
    return this.camera
  }

  public getScene():Scene{
    return this.scene
  }


  constructor(props:RenderViewProps){
    super(props)
    this.canvas = document.createElement('canvas')

    this.componentDidMount = ()=>{ 
      this.initBabylon()
    }
    this.componentWillUnmount= ()=>{
      //deregister
      this.props.deregister(this)
    }
  }
  getRoot(): Node {
    return this.rootNode
  }

  initBabylon():void {
    //create canvas
    this.canvas.width = this.base.clientWidth
    this.canvas.height = this.base.clientHeight
    this.base.appendChild(this.canvas)
    //init engine
    this.engine = new Engine(this.canvas, false, {  preserveDrawingBuffer : true})
    this.engine.enableOfflineSupport = false

    //scene
    this.scene = new Scene(this.engine) 
    //this.scene.autoClear =false
    //this.scene.autoClearDepthAndStencil = false
    this.rootNode = new TransformNode("root", this.scene)
 
    //test sphere
    //const sphere = BABYLON.MeshBuilder.CreateSphere("testsphere", { diameter:2}, this.scene)
    //sphere.setAbsolutePosition(new BABYLON.Vector3(124,120,0))


    //test box
    //const cube = BABYLON.MeshBuilder.CreateBox("testcube", {  size: 2}, this.scene)
    //cube.setAbsolutePosition(new BABYLON.Vector3(120,120,0))

    //asset container
    this.container = new AssetContainer(this.scene)

    //init asset manager
    this.assetMan = new AssetsManager(this.scene)
    
    //on finish we can start!
    this.assetMan.onFinish = ()=>{
      this.container.moveAllFromScene()
      //camera
      this.props.world.initDraw(this)
      //start recieving refresh calls...
      this.props.register(this)
    } 
    this.assetMan.onTaskError = (task)=>{ "Error: " + console.log(task.name)}

    //load assets
    this.props.world.loadAssets(this.assetMan)
    this.assetMan.load()
  }
  
  refresh(time: number, dt: number): void {

    this.engine.beginFrame()
    this.scene.render()
    this.engine.endFrame()
    this.props.world.preDraw(dt)
  }

  render(props?:Readonly<RenderViewProps>, state?: Readonly<RenderViewState>, context?: any): ComponentChild {
    return <div class="gamecontainer"></div>
  }
}