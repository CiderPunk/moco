import { ICommsEntity, ICommsEntityFactory, ICommsBuffer, IPeerConnection } from "../comms/client/clientinterfaces";
import { MoCoPeerComms } from "./peercomms/enums";
import { RemotePlayerControl } from "./peercomms/remoteplayercontrol";
import { TankDescription } from "./peercomms/tankdescription";
import { TankHealth } from "./peercomms/tankHealth";

export class MoCoEntityFactory implements ICommsEntityFactory{

  createEntity(entId: number, typeId: number, pc?:IPeerConnection) : ICommsEntity {
    switch (typeId as MoCoPeerComms.EntityType){
      case MoCoPeerComms.EntityType.Controller:
        const con = new RemotePlayerControl(entId, pc)
        return con 
      case MoCoPeerComms.EntityType.TankDescrition:
        return new TankDescription(entId, pc)
      case MoCoPeerComms.EntityType.TankHealth:
        return new TankHealth(entId, pc)     
    }
  }


}