import { h, Component, ComponentChild } from "preact"
import { IScreenSyncd } from "../../common/interfaces"
import { IWorld } from "../interfaces"

export interface DebugViewProps{
  register:(IScreenSyncd)=>void
  deregister:(IScreenSyncd)=>void
  world:IWorld
}

interface DebugViewState{
  
}


export class DebugView  extends Component<DebugViewProps, DebugViewState> implements IScreenSyncd{

  ctx:CanvasRenderingContext2D
  canvas:HTMLCanvasElement

  constructor(props:DebugViewProps){
    super(props)
    this.canvas = document.createElement('canvas')
    this.componentDidMount = ()=>{
      this.canvas.width = this.base.clientWidth
      this.canvas.height = this.base.clientHeight
      this.base.appendChild(this.canvas)
      this.ctx = this.canvas.getContext('2d')
      this.props.register(this)
    }
    this.componentWillUnmount= ()=>{
      this.props.deregister(this)
    }
  }


  refresh(time: number, dt: number): void {
    const ctx = this.ctx
    const world = this.props.world.world

    //ctx.resetTransform();
    ctx.setTransform(1,0,0,1,0, 0);
    this.ctx.clearRect(0,0,this.ctx.canvas.width, this.ctx.canvas.height);
    ctx.setTransform(-4,0,0,4,this.canvas.width / 2, this.canvas.height / 2);
    ctx.strokeStyle = "pink"
    ctx.lineWidth = 1

    for(var body = world.getBodyList(); body; body = body.getNext() ){
      if (body.isActive()){
        ctx.save()
        ctx.translate(body.getPosition().x, body.getPosition().y)
        ctx.rotate(body.getAngle())
        for(var fix = body.getFixtureList(); fix; fix = fix.getNext()){
          var shape = fix.getShape();
          switch(shape.m_type){
            case  "circle":
              var circle = shape as planck.Circle

              const center = circle.getCenter()
              //ctx.moveTo(center.x, center.y)
              ctx.beginPath()
              ctx.strokeStyle = "red"
              ctx.arc(center.x, center.y, circle.m_radius , 0,2*Math.PI)
              ctx.lineTo(center.x,center.y)
              ctx.stroke()
              break;

            case "polygon":
              var poly = shape as planck.Polygon
              var verts = poly.m_vertices
              var last = verts[verts.length - 1]
              ctx.beginPath()
              ctx.strokeStyle = "orange"
              ctx.moveTo(last.x, last.y);
              for(var point of poly.m_vertices){
                ctx.lineTo(point.x,point.y)
              }
              ctx.stroke()
              break
            case "edge":
              //var edge = shape as planck.EdgeShape
              break

            case "chain":
              var chain = shape as planck.Chain
              var verts = chain.m_vertices
              ctx.moveTo(verts[0].x, verts[0].y)
              ctx.beginPath()
              ctx.strokeStyle = "blue"      
              for(var point of verts)  {
                ctx.lineTo(point.x,point.y)
              }
              ctx.stroke()
              break
          }
        }
        ctx.restore()
      }
    }
  }

  render(props?:Readonly<DebugViewProps>, state?: Readonly<DebugViewState>, context?: any): ComponentChild {
    return <div class="debugcontainer"></div>
  }





}