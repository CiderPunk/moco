
import { AssetsManager, FreeCamera, Scene , Node, AbstractMesh, Vector2, Color3} from 'babylonjs';
import { ImageBasedSlider } from 'babylonjs-gui/2D/controls/sliders/imageBasedSlider';
import { IBehaviorAware } from 'babylonjs/Behaviors/behavior';
import { IController, IV2 } from '../common/interfaces';
import { MoCoComms } from '../common/mococomms';
import { ICommsEntityManager, IPeerConnection } from '../comms/client/clientinterfaces';
import { CommsBuffer } from '../comms/client/commsbuffer';
import { EntityType } from './enum';


export interface IGame{
  setScore(score:IScore)
  setGoalState(goalState: IGoalState) 
  entman:ICommsEntityManager
}

export interface IWorld{
  game:IGame
  addEnt(ent:IEntity)
  addPlayer(control:IController, pc:IPeerConnection)
  spawnShell(shooter: IShooter):IShell
  world:planck.World
  renderer:IRenderer
  initDraw(renderer:IRenderer):void
  loadAssets(assetMan:AssetsManager):void
  preDraw(dt:number):void
  size:IV2
  ground:IGround

  startOrbitCam(target:IEntity, time:number)
}

export interface IScore{ 
  teamAName:string,
  teamBName:string,
  teamA:number, 
  teamB:number
}

export interface IPeerPlayer{
  name:string
  uuid:string
}

export interface IGround{
  reset()
  registerTrackMesh(mesh:AbstractMesh):void
}


export interface INamedColour{
  name:string
  colour:Color3
}


export interface IEntity{
  //getPosition:planck.Vec2
  initDraw(world: IWorld):void
  collision(other:IEntity)
  prePhysics(dT:number):boolean
  preDraw(dt:number):void
  

  setPosition(pos:IV2, orientation:number, resetVelocity:boolean)


  getPosition(): IV2 
  getVelocity(): IV2 
  get type():EntityType
}


export interface IPhysicsObject extends IEntity{
  applyImpulse(impulse:IV2, position:IV2)
}


export function IsPhysicsObject(ent:IEntity):ent is IPhysicsObject{
  return (ent as IPhysicsObject).applyImpulse !== undefined
}

 
export interface IDamageable extends IEntity{
  hurt(energy:number, other:IEntity, type:string)
}

export function IsDamageable(ent:IEntity):ent is IDamageable{
  return (ent as IDamageable).hurt !== undefined
}

export interface IPlayer extends IEntity{
  getScreenPosition():IV2
  playerInfo:MoCoComms.PlayerInfo
  playerColour:INamedColour
  team:ITeam
  showNamePlate(time:number):void


}

export interface IShell extends IEntity{
  shooter:IEntity
  getMass():number
  init(position:IV2, velocity:IV2, shooter:IShooter, density:number):void
}




export interface IRenderer{
  getScene():Scene
  getCamera():FreeCamera
  getRoot():Node
}

export interface IPool{
  Release(item:any):void
}

export interface IPooledItem<T>{
  Free:()=>void
  CleanUp:()=>void
}

export interface IShooter extends IEntity{
  getVelocity(): IV2
  getWeaponPosition():IV2
  getWeaponOrientation():number
  groupIndex:number
}

export interface IWeapon{
  update(dT:number, trigger:boolean, shooter:IShooter):void
}

//sportball game interfaces
export interface IGameManager{
  reset(hardReset?:boolean):void
  update(arg0: number);
  goalScored(team:ITeam, player:IPlayer);
  initWorld(world:IWorld)
  addPlayer(control: IController, playerInfo:MoCoComms.PlayerInfo):IPlayer 
}

export interface ITeam{
  getNewSpawnPosition():IPositionOrientation
  reset(hardReset?:boolean):void
  goalScore(ball:IBall)
  addPlayer(world:IWorld, control: IController, playerInfo: MoCoComms.PlayerInfo): IPlayer
  name:string
  teamColour:INamedColour
  getPlayerCount():number 
  getScore():number
  removePlayer(tank:IPlayer)
}

export interface IBall extends IEntity{
  lastTouch:IPlayer
}

export interface IPositionOrientation{
  position:IV2
  orientation:number
}

export interface IGoalState{
  score:boolean
  teamName:string
  scorerName:string
  wasOwnGoal:boolean
}