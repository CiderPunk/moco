import { IController, IScreenSyncd, IV2 } from "../common/interfaces";
import { V2 } from "../common/math/v2";
import { MoCoComms } from "../common/mococomms";
import { ICommsEntity, IPeerConnection, PeerConnectionState } from "../comms/client/clientinterfaces";
import { IPlayer } from "./interfaces";

export class LocalControl implements IController{
  setPlayer(player: IPlayer) {
    this.player = player
  }

  player:IPlayer

  joyLeft: V2 
  joyRight: V2
  fire1: boolean 
  fire2: boolean 

  pc:IPeerConnection = {
    peerInfo: { name: 'local Player', uuid: '123456789' } as MoCoComms.PlayerInfo,
    heartBeatTime: 0,
    state: PeerConnectionState.Connected,
    onPeerDisconnected: () => { },
    sendEntity: function (ent: ICommsEntity, forceFlush: boolean): void { },
    flush: function (): void {}
  }

  keystate:Map<number, boolean>
  private keydir:V2
  speedMod:boolean = false

  constructor(){
    document.addEventListener("keydown", (e:KeyboardEvent) =>{ this.keyDownHandler(e) }, false);
    document.addEventListener("keyup", (e:KeyboardEvent) =>{ this.keyUpHandler(e) }, false);
    this.joyLeft = new V2()
    this.joyRight = new V2()

    this.fire1 = false
    this.fire2 = false
    this.keystate = new Map<number, boolean>()
    this.keydir = new V2()
    document.addEventListener("mousemove", (e:MouseEvent)=>{ this.mouseHandler(e) })

    document.addEventListener("mousedown", (e:MouseEvent)=>{ 
      this.fire1 = true
      e.stopPropagation()
     })

     document.addEventListener("mouseup", (e:MouseEvent)=>{ 
      this.fire1 = false
      e.stopPropagation()
     })

  }
  onDisconnected: () => void;
  

  mouseHandler(e: MouseEvent) {
    const playerPos = this.player.getScreenPosition()
    this.joyRight.set( playerPos.x- e.clientX, playerPos.y - e.clientY).norm()
    this.joyRight.x = -this.joyRight.x
  }


  keyDownHandler(e: KeyboardEvent) {
    //console.log(e.keyCode)
    if (this.keystate[e.keyCode]){
      return
    }
    
    this.keystate[e.keyCode] = true
    switch (e.keyCode){
      case 16:
        this.speedMod = true
        break;
      case 37:
        this.keydir.x-=1
        break;
      case 39:
        this.keydir.x+=1
        break;
      case 38:
        this.keydir.y+=1
        break;
      case 40:
        this.keydir.y-=1
        break;
    }
    this.joyLeft.setV2(this.keydir).norm()
  }
  keyUpHandler(e: KeyboardEvent) {
    this.keystate[e.keyCode] = false
    switch (e.keyCode){
      case 16:
        this.speedMod = false
        break;
      case 37:
        this.keydir.x+=1
        break;
      case 39:
        this.keydir.x-=1
        break;
      case 38:
        this.keydir.y-=1
        break;
      case 40:
        this.keydir.y+=1
        break;
    }
    this.joyLeft.setV2(this.keydir).norm().scale(this.speedMod ? 1: 0.5)
  }

  
}