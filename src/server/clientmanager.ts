import * as Io from "socket.io"
import { IClientManager, IGame, IMoCoClient, IceConfogJson, SimpleIceConfogJson } from "./interfaces"
import { Constants } from "./constants"
import { Game } from "./game"
import { MoCoClient} from "./mococlient"
import { IceConfigGenerator } from "../comms/server/iceconfiggenerator"
import fs = require('fs')
import { SimpleIceConfigGen } from "../comms/server/simpleiceconfiggen"
import { IIceConfigGenerator } from "../comms/server/interfaces"

export class ClientManager implements IClientManager{


  readonly iceConfigGen:IIceConfigGenerator

  readonly clients:Map<string,IMoCoClient>
  readonly games:Map<string,IGame>

  createGame(client:IMoCoClient, key: string, hash: string) {
    //generate random keys until we find one not currently in use.. unlikely but possible i guess.
    do{
      var key = this.createGameKey()
    }while(this.games.has(key))
    //store the new game
    this.games.set(key, new Game(client, this, key))
  }

  getGame(key:string): IGame{
    return this.games.get(key)
  }

  dropGame(key: string): void {
    this.games.delete(key)
  }

  dropClient(id:string):void{
    this.clients.delete(id)
  }

  createGameKey(){
    var key = ""
    for (var  i = 0;i < Constants.KeyLength; i++){
       key  += Constants.KeyChars[Math.floor(Math.random() * Constants.KeyChars.length)]
    }
    return key;
  }


/*
  joinGame(req: Comms.JoinGameRequest, socket: Io.Socket) {
    //validate requested key... or dont bother
    let game = this.games.get(req.key)
    if (game){
      game.addPlayer(new Player(req.name, socket, game))
    }
    else{
      socket.emit(EventNames.Error, {message: "Game not found" } as Comms.ErrorResponse)
    }
  }
  
  dropGame(key:string):void{
    this.games.delete(key)
  }

  createGame(req: Comms.CreateGameRequest, socket: Io.Socket):void {
    //TODO: validate requested existing key
    //get a new unused key
 
  }

  */


  constructor(private io:SocketIO.Server){
    this.clients = new Map<string, IMoCoClient>()
    this.games = new Map<string, IGame>()


    const conf = JSON.parse(fs.readFileSync('./iceconfig.json', 'utf8')) as IceConfogJson
    this.iceConfigGen = new IceConfigGenerator(conf.stun, conf.turn, conf.secret, conf.ttl)

    //const conf = JSON.parse(fs.readFileSync('./simpleconfig.json', 'utf8')) as SimpleIceConfogJson
    //this.iceConfigGen = new SimpleIceConfigGen(conf.stun, conf.turn, conf.username, conf.password)
      

    this.io.on('connection', (socket:Io.Socket)=>{
      console.log(`user connected: ${socket.id}`)
      const client = new  MoCoClient(this, socket, this.iceConfigGen)
      this.clients.set(client.id, client)
    })
  }
}
