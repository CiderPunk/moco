import * as Http from "http"
import * as Https from "https"
import * as fs from "fs"
import * as Express  from "express"
import * as Io from "socket.io"
import * as path from "path"
import { ClientManager } from "./clientmanager";

export class Server{

  private readonly router:Express.Router
  private readonly app:Express.Express
  private readonly io:SocketIO.Server
  private readonly clientMan:ClientManager

  protected CreateHttpsServer(app:Express.Express){
    const privateKey = fs.readFileSync('/etc/letsencrypt/live/starduel.ciderpunk.net/privkey.pem', 'utf8')
    const certificate = fs.readFileSync('/etc/letsencrypt/live/starduel.ciderpunk.net/cert.pem', 'utf8')
    const ca = fs.readFileSync('/etc/letsencrypt/live/starduel.ciderpunk.net/chain.pem', 'utf8')
    const credentials = {
      key: privateKey,
      cert: certificate,
      ca: ca
    }

    const httpsServer = Https.createServer(credentials, app)
    httpsServer.listen(443,()=>{
      console.log('listening on *:443')
    })
    return httpsServer
  } 

  protected CreateHttpServer(app:Express.Express, port:number = 80){
    const httpServer = Http.createServer(app)
    httpServer.listen(port,()=>{
      console.log('listening on *:' + port)
    })
    return httpServer
  } 

  constructor(enableHttps:boolean = false, port:string = process.env.PORT || "80" ){
    this.app  = Express();
    //this.app.use('/src', Express.static('src'));
    this.app.use('/node_modules', Express.static('node_modules'));
    this.app.use(Express.static('public'));
    this.app.use('/play/:key', (req,res)=>{ 
      res.sendFile("public/play/index.html", { root:process.cwd()})   
    })


    let server:any = null
    //https
    if (enableHttps){
      server = this.CreateHttpsServer(this.app)
      //create a simple redirect service
      const redirectApp = Express()
      redirectApp.get('*', (req,res)=>{
        res.redirect('https://' + req.headers.host + req.url);
      })
      this.CreateHttpServer(redirectApp,parseInt(port))
    }
    else{
      server = this.CreateHttpServer(this.app, parseInt(port))
    }

    //create socketio
    this.io = Io(server)
    this.clientMan = new ClientManager(this.io)
  }

}