import { IIceConfigGenerator, IClient } from "./interfaces"

export class SimpleIceConfigGen implements IIceConfigGenerator{

  constructor(protected stun:string, protected turn:string, protected user:string, protected password:string){}

  getIceConfig(user:IClient):object {

    return {
      iceServers:[
        { 
          urls:this.stun,
          credential:this.user,
          username:this.password
        },
        {
          urls:this.turn,
          credential:this.user,
          username:this.password
        } 
      ]}
  }

}