import { Component, ComponentChild, h } from "preact"

export interface HealthBarProps{
  health:number
}

export class HealthBar  extends Component<HealthBarProps>{
  
  constructor(props:HealthBarProps){
    super(props)
  }
  
  render(props: Readonly<HealthBarProps>): ComponentChild { 
    return <div className="healthbar"><progress value={props.health} max="100">{props.health}%</progress></div>
  }  
}