import { PeerConnectionManager } from "../comms/client/peerconnectionmanager";
import { IScreenSyncd } from "./interfaces";

export class PcmProxy implements IScreenSyncd{
  constructor(protected readonly pcm:PeerConnectionManager){}

  refresh(time: number, dt: number): void {
    this.pcm.update(time, dt)
  }


}