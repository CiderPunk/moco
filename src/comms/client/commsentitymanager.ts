import { ICommsEntityManager, ICommsEntity, ICommsEntityFactory, IPeerConnection } from "./clientinterfaces"
import { CommsBuffer } from "./commsbuffer"
import { PeerConnection } from "./peerconnection"

export class CommsEntityManager implements ICommsEntityManager{

  protected nextEntityId:number = -1

  public onNetCreate:(IPeerConnection, ICommsEntity)=>void


  protected entMap:Map<number,ICommsEntity>

  public constructor(protected readonly factory:ICommsEntityFactory){
    this.entMap = new Map<number, ICommsEntity>()
  }

  public getNextEntID():number{
    return this.nextEntityId++
  }

  public setNextEntId(nextId:number):void{
    if (this.nextEntityId < 0){
      this.nextEntityId = nextId
    }
  }
  
  //locally create a new entity
  createEntity(typeId:number):ICommsEntity {
    const entId = this.getNextEntID()
    const ent = this.factory.createEntity(entId, typeId)
    this.entMap.set(entId, ent)
    return ent
  }

  //update create an entity from a remote source
  updateEntity(rcvBuffer:CommsBuffer, pc:IPeerConnection) {
    //read entity id
    const entId:number = rcvBuffer.ReadUint()
    //read entity type
    const typeId:number = rcvBuffer.ReadUint()
    //try to get our entity
    let ent = this.entMap.get(entId)
    if (!ent){
      //ent doesnt exist, lets make a new one!
      ent = this.factory.createEntity(entId, typeId, pc)
      this.entMap.set(entId, ent)
      //call net create event
      if (this.onNetCreate){ 
        this.onNetCreate(rcvBuffer.owner, ent)
      }
    }
    ent.readState(rcvBuffer)
  }

}