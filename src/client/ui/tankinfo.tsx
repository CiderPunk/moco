import { h, Component, ComponentChild } from "preact"
import {  ITankDescription, IV3 } from "../../common/interfaces"

export interface TankInfoProps{
  tankDescription?:ITankDescription
}



export class TankInfo extends Component<TankInfoProps>{

  constructor(props:TankInfoProps){
    super(props)
  }
  
  render(props: Readonly<TankInfoProps>): ComponentChild { 
    
    return props.tankDescription ? 
    <div class="tankdesc">
      your tank:
      <strong style={"color:" + this.renderColor(props.tankDescription.turretColour)}>Turret</strong>
      <strong style={"color:" + this.renderColor(props.tankDescription.chassisColour)}>Chassis</strong>  
    </div>
    : null
    
  }

  renderColor(col:IV3):string{
    return `rgb(${Math.round(col.x * 255)},${Math.round(col.y * 255)},${Math.round(col.z * 255)})`
  }

}



