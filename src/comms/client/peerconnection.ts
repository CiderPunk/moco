
import * as io from "socket.io-client"
import Peer = require("simple-peer")
import { SocketComms } from "../socketcomms"
import { IPeerConnectionManager, IPeerConnection, PeerConnectionState, PeerMessageType, ICommsEntity } from "./clientinterfaces"
import { CommsBuffer } from "./commsbuffer"


export class PeerConnection implements IPeerConnection{
  get state():PeerConnectionState { return this.connectionState }

  public onPeerDisconnected: (peerConn:IPeerConnection)=> void

  protected connectionState:PeerConnectionState
  protected readonly peer:Peer.Instance
  protected readonly sendBuffer:CommsBuffer

  protected nextBeat:number = 0
  public heartBeatTime:number = 1000

  constructor(protected readonly owner: IPeerConnectionManager,
      primary:boolean, 
      public readonly key:string,
      protected readonly socket:SocketIOClient.Socket, 
      public readonly peerInfo:any = null, 
      config:any){

    this.connectionState = PeerConnectionState.Connecting 
    this.peer = new Peer({
      initiator:primary,
      trickle: false,
      config: config,
    })
    this.sendBuffer = new CommsBuffer(new ArrayBuffer(8192))
    
    //simple peer wants to send a signal, we forward it to the server via socket.io
    this.peer.on("signal", data=>{ 
      console.log("Sending signal data " + this.key)
      this.socket.emit(SocketComms.MessageType.PeerSignal, { key: this.key, data: data } as SocketComms.Payload.PeerSignal)
    })

    this.peer.on("connect", ()=>{
      console.log("Connected " + this.key)

      this.connectionState = PeerConnectionState.Connected
      //signal manager
      this.owner.connected(this)
    })
    this.peer.on("error",(err:any)=>{ 
      console.log("Error " + err.code + this.key)
      //this.connectionState = PeerConnectionState.Disconnected
      //this.owner.dropConnection(this.key)
    })
    this.peer.on("close",()=>{ 
      console.log("Disconnected " + this.key)
      //call our disconnect handler
      this.connectionState = PeerConnectionState.Disconnected
      this.owner.dropConnection(this.key)
      if (this.onPeerDisconnected){
        this.onPeerDisconnected(this)
      }
    })
    this.peer.on("data",(data:Uint8Array)=>{ 
      this.handleData(data)
    })
  }
  public signal(data:any) : void{
    console.log("Recieved signal data "+ this.key)
    this.peer.signal(data)
  }

  protected handleData(data: Uint8Array) {
    const rcvBuffer = new CommsBuffer(data.buffer, this)
    let msgType:PeerMessageType = PeerMessageType.Start
    while(msgType != PeerMessageType.End){
      switch (msgType){
        case PeerMessageType.EntityUpdate:
          this.owner.entMan.updateEntity(rcvBuffer, this)
          break;
      }
      msgType = rcvBuffer.ReadUint() as PeerMessageType
    }
  }

  sendEntity(ent: ICommsEntity, forceFlush:boolean = false) {
    this.sendBuffer.WriteUint(PeerMessageType.EntityUpdate)
    this.sendBuffer.WriteUint(ent.entId)
    this.sendBuffer.WriteUint(ent.typeId)
    ent.writeState(this.sendBuffer)
    if (forceFlush){
      this.flush()
    }
  }


  public flush(){
    this.nextBeat = this.heartBeatTime
    this.peer.send(this.sendBuffer.GetSendView())
    this.sendBuffer.Reset()

  }
  
  update(t: number, dt: number) {
    if (this.state != PeerConnectionState.Connected){ return }
    if (this.nextBeat > 0){ 
      this.nextBeat -= dt
      return
    }
    this.flush()
  }




}