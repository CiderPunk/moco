import { ICommsEntity, ICommsEntityManager, IPeerConnection } from "../comms/client/clientinterfaces";

export interface IV2{
  x:number
  y:number
}
export interface IV3{
  x:number
  y:number
  z:number
}

export interface IScreenSyncd{
  refresh(time:number, dt:number):void
}

export interface IController{
  pc?:IPeerConnection
  readonly joyLeft:IV2
  readonly joyRight:IV2
  fire1:boolean
  fire2:boolean
  onDisconnected:()=> void
}

export interface ITankDescription extends ICommsEntity{
  turretColour:IV3
  chassisColour:IV3
}
export interface ITankHealth extends ICommsEntity{
  onUpdated?:(health:number)=>void
  health:number
}
export interface IRemoteController extends ICommsEntity, IController{}


