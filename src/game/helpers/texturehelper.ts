
import { AssetsManager, Texture } from 'babylonjs';

export namespace TextureHelpers{

  export function loadTextures(assMan:AssetsManager, urls:Array<string>, dest:Array<Texture>, namebase:string, offset:number = 0 ):void{
    for (let i= 0; i < urls.length; i++){
      const task = assMan.addTextureTask(`${namebase}_${i}`, urls[i])
      task.onSuccess = (task)=>{ 
        dest[i + offset] = task.texture  
      }
    }
  }



}