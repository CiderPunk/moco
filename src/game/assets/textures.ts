export namespace Textures{
  export const Concrete = [
    "/assets/concrete/Concrete1_albedo.png",
    "/assets/concrete/Concrete1_normal.png"
  ]

  export const Metal = [
    "/assets/metal/Metal_albedo.png",
    "/assets/metal/Metal_normal.png",
    "/assets/metal/Metal_mr.png",
  ]
  export const Asphalt = [
    "/assets/asphalt/asphalt_albedo.png",
    "/assets/asphalt/asphalt_norm.png",
    "/assets/asphalt/asphalt_r_ao.png",
  ]



}