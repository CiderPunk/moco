import { Pool } from "../helpers/pool";
import { IPooledItem, IWorld, IPool, IEntity, IRenderer, IShooter, IShell, IsDamageable, IPlayer } from "../interfaces";
import planck = require("planck-js");
import { IV2 } from "../../common/interfaces";
import { CollisionGroup } from "../constants";
import { V2 } from "../../common/math/v2";
import { BaseEntity } from "./baseentity";
import { Color3, InstancedMesh, Mesh, MeshBuilder, StandardMaterial } from "babylonjs";
import { EntityType } from "../enum";

export class ShellPool extends Pool<Shell>{

  constructor(world:IWorld){
    super(world, "shell_", Shell.GetNew)
  }
  
}

export class Shell extends BaseEntity implements IPooledItem<Shell>, IEntity, IShell{

  get type(): EntityType { return EntityType.shell }
  
  createBody(world: planck.World, position: IV2, orientation:number): planck.Body {
    const body = world.createBody({  type: planck.Body.DYNAMIC })
    body.setBullet(true)
    body.setUserData(this)
    return body
  }

  static _shellMesh:Mesh  
  protected mesh:InstancedMesh
  protected readonly body: planck.Body
  protected readonly fixture: planck.Fixture
  private _shooter:IEntity
  protected alive:boolean

  static getShellMesh(renderer:IRenderer):Mesh{
    if (!Shell._shellMesh){
      
      const shell = Shell._shellMesh = MeshBuilder.CreateSphere("shelltemplate", {segments:5, slice:1, diameter:0.8}, renderer.getScene())
      const mat = new StandardMaterial("shell", renderer.getScene())
      mat.diffuseColor = new Color3(0.1,0.1,0.1)
      shell.material = mat
      shell.setEnabled(false)
    }
    return Shell._shellMesh
  }

  public get shooter():IEntity {
    return this._shooter
  }


  static readonly pos = new planck.Vec2
  static readonly vel = new planck.Vec2

  public init(position:IV2, velocity:IV2, shooter:IShooter){
    this._shooter = shooter
    this.body.setPosition(Shell.pos.set(position.x, position.y))
    this.body.setLinearVelocity(Shell.vel.set(velocity.x, velocity.y))
    this.body.setActive(true)
    this.fixture.setFilterData({ groupIndex:shooter.groupIndex, categoryBits:CollisionGroup.projectile , maskBits: CollisionGroup.all})
    this.mesh.setEnabled(true)
    this.alive = true
  }

  getMass():number{
    return this.body.getMass()
  }

  initDraw(world: IWorld): void {
    if (!this.mesh){
      this.mesh = Shell.getShellMesh(world.renderer).createInstance(name+ "_mesh")
      world.renderer.getScene().addMesh(this.mesh)
    }
  }


  static realtiveSpeed:V2 = new V2()

  collision(other: IEntity) {
    if (IsDamageable(other)){
      Shell.realtiveSpeed.setV2(this.getVelocity()).sub(other.getVelocity())
      var ke= 0.5 * this.getMass() * Shell.realtiveSpeed.len2()
      other.hurt(ke, this.shooter, "armour piercing sabot")
    }
    //damage other
    this.alive = false
  }

  prePhysics(dT: number): boolean {
    if (!this.alive){
      this.Free()
    }
    return this.alive
  }

  preDraw(dt: number): void {
    const pos = this.body.getPosition()
    this.mesh.position.set(pos.x, 2.5, pos.y)
  }
  
  public static GetNew(name:string, world:IWorld, pool:ShellPool ):Shell{
    return new Shell(name, world, pool)
  }

  protected constructor(protected name:string, protected world:IWorld, private pool:ShellPool){
    super(world, {x:0, y:0})
    //store first fixture
    this.fixture = this.body.createFixture({shape: new planck.Circle(new planck.Vec2(0,0),0.3), density:5, restitution:0.1})
    //pooled entities gotta start innactive
    this.body.setActive(false)
    this.body.setBullet(true)
  }

  Free():void{
    this.CleanUp()
    this.pool.Release(this)
  }

  CleanUp(){
    this.mesh.setEnabled(false)
    this.body.setActive(false)
  }
}