import * as Io from "socket.io"
import uuid = require("uuid");
import { BaseClient } from "../comms/server/baseclient";
import { IMoCoClient, IClientManager } from "./interfaces";
import { MoCoComms } from "../common/mococomms";
import { IIceConfigGenerator } from "../comms/server/interfaces";


export class MoCoClient extends BaseClient implements IMoCoClient{

  protected name:string

  public constructor(protected owner:IClientManager, socket:Io.Socket, iceGen:IIceConfigGenerator){
    super(socket, null, iceGen)
    this.name = ""

    socket.on(MoCoComms.Events.CreateGameRequest, (req:MoCoComms.CreateGameRequest)=>{  
      owner.createGame(this, req.key, req.hash)
    })

    socket.on(MoCoComms.Events.JoinGameRequest, (req:MoCoComms.JoinGameRequest)=>{ 
      this.name = req.name
      const game =owner.getGame(req.key)
      game.addPlayer(this);

    })
    socket.on(MoCoComms.Events.Disconnected, ()=>{ 
      this.owner.dropClient(this.id)
    })
  }

  public getPeerInfo():object{
    return { name: this.name, uuid:this.id } as MoCoComms.PlayerInfo
  }
  
  setNextEntId(num: number): void {
    this.entId = num
  }

  
}
