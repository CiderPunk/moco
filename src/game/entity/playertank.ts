import { IEntity, IShooter, IWorld, IWeapon, IPhysicsObject, IDamageable, ITeam, INamedColour, IPlayer } from "../interfaces";
import { IController,  ITankDescription,  ITankHealth,  IV2, IV3 } from "../../common/interfaces";
import { V2 } from "../../common/math/v2";
import planck = require("planck-js");
import { Tank, TankConstants } from "./tank";
import * as BABYLON from 'babylonjs';
import { Cannon } from "../weapons/cannon";
import { Material } from "babylonjs/Materials/material";
import { MoCoComms } from "../../common/mococomms";
import { EntityType } from "../enum";
import { Constants } from "../constants";
import { MoCoPeerComms } from "../../common/peercomms/enums";

export class PlayerTank extends Tank implements IPlayer, IShooter, IPhysicsObject, IDamageable{
  get name(): string {
    return this.control.pc?.peerInfo.name
  }

  protected deathTimer: number = 0
  //public readonly position:V2
  protected readonly body: planck.Body
  protected readonly leftForce:planck.Vec2 
  protected readonly rightForce:planck.Vec2 

  protected health:number = Constants.TankHealth

  protected steervec:V2 = new V2()
  protected aimvec:V2 = new V2()

  protected active = true  

  public weapon:IWeapon
  private tankHealthEnt: ITankHealth;


  public constructor(protected world:IWorld, protected readonly control:IController, public team:ITeam, public playerColour:INamedColour,
    chassisMat:Material, turretMat:Material, startPos:IV2,  orientation:number  ){
    super(world, startPos, orientation,  chassisMat, turretMat)
    this.leftForce = new planck.Vec2()
    this.rightForce = new planck.Vec2()
    this.weapon = new Cannon(this.world)
    this.playerInfo = control.pc.peerInfo as MoCoComms.PlayerInfo
    control.onDisconnected = ()=>{this.disconnected() }

    //send remote player thier tank description      
    if (control.pc){
      const tankDesc = this.world.game.entman.createEntity(MoCoPeerComms.EntityType.TankDescrition) as ITankDescription
      tankDesc.turretColour.x= playerColour.colour.r 
      tankDesc.turretColour.y= playerColour.colour.g
      tankDesc.turretColour.z= playerColour.colour.b

      tankDesc.chassisColour.x= team.teamColour.colour.r
      tankDesc.chassisColour.y= team.teamColour.colour.g
      tankDesc.chassisColour.z= team.teamColour.colour.b
      this.control.pc.sendEntity(tankDesc, true)

      this.tankHealthEnt =  this.world.game.entman.createEntity(MoCoPeerComms.EntityType.TankHealth) as ITankHealth
      this.tankHealthEnt.health = 100
      this.control.pc.sendEntity(this.tankHealthEnt, true)
    }
  }


  sendHealthUpdate(){
    if (this.control.pc){
      this.tankHealthEnt.health = (this.health / Constants.TankHealth) * 100
      this.control.pc.sendEntity(this.tankHealthEnt, true)
    }
  }

  respawn() {
    //get new psotion
    const spawnLoc = this.team.getNewSpawnPosition()
    this.setPosition(spawnLoc.position, spawnLoc.orientation, true)
    this.body.setActive(true)
    this.chassis.setEnabled(true)
    this.health = Constants.TankHealth
    this.sendHealthUpdate()

    this.showNamePlate(3000)
  }


  get type(): EntityType { return EntityType.player }
  

  playerInfo: MoCoComms.PlayerInfo;
  hurt(energy: number, other: IEntity, type: string) {
    this.health -= energy
    console.log(`tank hit for ${energy} health remaining: ${this.health}`)
    this.sendHealthUpdate()
  }

  static imp = new planck.Vec2()
  static pos = new planck.Vec2()
  applyImpulse(impulse: IV2, position: IV2) {
    this.body.applyLinearImpulse(PlayerTank.imp.set(impulse.x, impulse.y), PlayerTank.pos.set(position.x, position.y))
  }

  getVelocity(): IV2 {
    return this.body.getLinearVelocity()
  }

  /**
   * gets x,y coordinates of player turret position on screen
   */
  getScreenPosition():IV2 {  
    //no mesh assigned yet
    if (!this.turret){
      return {x:0, y:0}
    }
    const scene = this.turret.getScene()
    const engine = scene.getEngine()

    const pos = BABYLON.Vector3.Project(
      this.turret.getAbsolutePosition(),
      BABYLON.Matrix.IdentityReadOnly,
      scene.getTransformMatrix(),
      scene.activeCamera.viewport.toGlobal(engine.getRenderWidth(), engine.getRenderHeight()))
    return { x:pos.x, y:pos.y}
  }
  
  public getHealth():number{
      return this.health
  }
  

  disconnected():void {
    this.team.removePlayer(this)
    this.active = false
  }


  collision(other: IEntity) {
   // console.log("tank collision")
  }


  prePhysics(dT: number): boolean {
    super.prePhysics(dT)

    if (this.deathTimer <= 0){
      //cancel lateral motion
      this.steervec.setV2(this.control.joyLeft)
      const magnitude = this.steervec.len() * 800
      
      this.leftDrive = 0
      this.rightDrive = 0
      if (magnitude > 0){
        const angle = this.steervec.getAngle()

        //calculate input to each drive
        this.leftDrive = this.calcDrive(angle, TankConstants.leftDrive) * magnitude
        this.rightDrive = this.calcDrive(angle, TankConstants.rightDrive) * magnitude
        //create left/right forces
        this.leftForce.set(0,this.leftDrive)
        this.rightForce.set(0,this.rightDrive)
        //apply to each track
        this.body.applyForce(this.body.getWorldVector(this.leftForce), this.body.getWorldPoint(TankConstants.leftTrack))
        this.body.applyForce(this.body.getWorldVector(this.rightForce), this.body.getWorldPoint(TankConstants.rightTrack))
      }

      //get new aim direction
      this.aimvec.setV2(this.control.joyRight).rotateCW()
      //invert ...cos reasons
      this.aimvec.x = - this.aimvec.x
      //if outside deadzone
      if (this.aimvec.len2() > 0.15){
        //set new targte direction
        this.targetOrientation = this.aimvec.getAngle()
      }
      //update our current weapon
      this.weapon.update(dT, this.control.fire1, this)
      

if (this.control.fire2){
  this.showNamePlate(200)
}

      //check if we're dead
      if (this.health <= 0){
        //console.log(this.control.)
        //hide model
        this.body.setActive(false)
        this.chassis.setEnabled(false)
        //spawn a splosion??



        BABYLON.ParticleHelper.CreateAsync("explosion", this.world.renderer.getScene()).then((set) => {
          set.systems.forEach(s => {
              s.emitter = this.chassis.position
              s.disposeOnStop = true
          })
          set.start();
      });


        //set deatntimer
        this.deathTimer = Constants.DeathTimer
        this.health = Constants.TankHealth
      }
    }
    else{
      this.deathTimer -= dT
      if (this.deathTimer < 0){
        this.respawn()
      }
    }
    if (!this.active){ this.cleanup()}
    return this.active
  }


  cleanup() {
    this.body.setActive(false)
    this.world.world.destroyBody(this.body)
    this.chassis.dispose(false, false)
  }

  weaponPosition:V2 = new V2()

  getWeaponPosition(): IV2{
    //turret is 1.2m forwartds of chassis centre for some reason
    return this.weaponPosition.setAngle(this.body.getAngle() +( 0.5 * Math.PI),0.85).add(this.body.getPosition())
  }

  getWeaponOrientation() :number{
    return this.turretOrient
  }

  /**
   * generates drive value for selected steering angel
   * @param angle selected angle
   * @param drive array defining transmission curve radians to thrust
   */
  calcDrive(angle: number, drive: Array<[number, number]>):number {
    for(var i = 1; i< drive.length; i++){
      if (angle <= drive[i][0]){
        const start = drive[i-1]
        const end = drive[i]
        const ratio = (angle - start[0]) / (end[0] - start[0])
        return start[1] + ratio * (end[1] - start[1])
      }
    }
    return 0
  }
}

