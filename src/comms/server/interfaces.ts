

export interface IClient{
  id:string
  socket:SocketIO.Socket
  getPeerInfo():object
  closePeer(key: string) : void
  startPeerConnect(target:IClient, primary?:boolean, key?:string ):void
  sendPeerSignal(sender:IClient, key:string, data:object) :void
}


export interface IIceConfigGenerator{  
  getIceConfig(user:IClient)
}
