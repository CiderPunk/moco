import { h, Component, ComponentChild } from "preact"
import { IGoalState } from "../interfaces"


export interface GoalStatusProps{
   goalState:IGoalState
}



export class GoalStatus extends Component<GoalStatusProps>{
  
  

  render(props: Readonly<GoalStatusProps>): ComponentChild {

    return props.goalState.score ? (
      <div> 
        <div className="goal">GOOOAAAAAL!!!!1!1!</div>
        <div className="goal-details">
          <strong>{props.goalState.scorerName}</strong> scored {props.goalState.wasOwnGoal ? "an own " :" a "} goal for <strong>{props.goalState.teamName}</strong>
        </div>
      </div>
    ) : null
    
  }
  
}