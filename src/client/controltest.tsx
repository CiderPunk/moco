import { h, Component, ComponentChild } from "preact"
import { Control } from "./ui/control"
import { RemotePlayerControl } from "../common/peercomms/remoteplayercontrol"
import { RegisterList } from "../common/utils/registerlist"
import { IScreenSyncd } from "../common/interfaces"

export class ControlTest extends Component{
  //todo: add debug output from controls
  protected reqAnimFrame:number
  private readonly screenSynced = new RegisterList<IScreenSyncd>()

  private lastTime:number
  protected doFrame():void{
    //get time and last time..
    const  t:number = performance.now()
    const dt = t- this.lastTime
    this.lastTime = t
    //queue up next frame
    this.reqAnimFrame = window.requestAnimationFrame(()=>{this.doFrame()})
    //lets trigger those timed components
    this.screenSynced.items.forEach((com:IScreenSyncd)=>{
      com.refresh(t, dt)
    })
  }

  public constructor(){
    super()
    this.componentDidMount = ()=>{
      this.reqAnimFrame = window.requestAnimationFrame(()=>{this.doFrame()})
    }
    this.componentWillUnmount= ()=>{
      window.cancelAnimationFrame(this.reqAnimFrame)
    }
  }

  render(): ComponentChild { 
    return <Control control={new RemotePlayerControl(0)}
    
    registerScreenSyncd={(com:IScreenSyncd)=>{ this.screenSynced.register(com)}} 
    deRegisterScreenSyncd={(com:IScreenSyncd)=>{ this.screenSynced.deregister(com)}}
    />
  }


}
