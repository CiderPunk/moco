export namespace SocketComms{

  export namespace MessageType{
    export const PeerSignal = "comms.peersignal"
    export const InitConnection = "comms.initconn"
  }

  export namespace Payload{

    export interface PeerSignal{
      key:string
      data:object
    }
    export interface InitConnection{
      key:string
      primary:boolean
      entId:number
      peerInfo:object
      iceConfig:object
    }

  }
}

