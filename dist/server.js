/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/common/mococomms.ts":
/*!*********************************!*\
  !*** ./src/common/mococomms.ts ***!
  \*********************************/
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.MoCoComms = void 0;
var MoCoComms;
(function (MoCoComms) {
    let Events;
    (function (Events) {
        Events.CreateGameRequest = "cgrq";
        Events.CreateGameResponse = "cgrp";
        Events.JoinGameRequest = "jgrq";
        Events.ReadyToJoin = "rtj";
        Events.PeerSignal = "sgnl";
        Events.PlayerJoinRequest = "pjrq";
        Events.PlayerDisconnect = "pdrq";
        Events.Error = "Err";
        Events.Connected = "connect";
        Events.Disconnected = "disconnect";
    })(Events = MoCoComms.Events || (MoCoComms.Events = {}));
})(MoCoComms = exports.MoCoComms || (exports.MoCoComms = {}));


/***/ }),

/***/ "./src/comms/server/baseclient.ts":
/*!****************************************!*\
  !*** ./src/comms/server/baseclient.ts ***!
  \****************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.BaseClient = void 0;
const uuid = __webpack_require__(/*! uuid */ "uuid");
const socketcomms_1 = __webpack_require__(/*! ../socketcomms */ "./src/comms/socketcomms.ts");
class BaseClient {
    constructor(socket, clientId = null, iceConfigGen = null) {
        this.socket = socket;
        this.iceConfigGen = iceConfigGen;
        this.entId = 0;
        this.peerConnections = new Map();
        socket.on(socketcomms_1.SocketComms.MessageType.PeerSignal, (req) => {
            const target = this.peerConnections.get(req.key);
            if (target !== null) {
                target.sendPeerSignal(this, req.key, req.data);
            }
        });
        socket.on("disconnect", () => {
            this.peerConnections.forEach((client, key, map) => { client.closePeer(key); });
        });
        this.id = clientId !== null && clientId !== void 0 ? clientId : uuid();
    }
    getPeerInfo() {
        return null;
    }
    sendPeerSignal(other, key, data) {
        if (!this.peerConnections.has(key)) {
            this.peerConnections.set(key, other);
        }
        this.socket.emit(socketcomms_1.SocketComms.MessageType.PeerSignal, { key: key, data: data });
    }
    startPeerConnect(target, isPrimary = true, key = null) {
        var _a;
        if (isPrimary) {
            key = uuid();
            target.startPeerConnect(this, false, key);
        }
        this.peerConnections.set(key, target);
        this.socket.emit(socketcomms_1.SocketComms.MessageType.InitConnection, {
            key: key,
            primary: isPrimary,
            entId: this.entId,
            peerInfo: target.getPeerInfo(),
            iceConfig: (_a = this.iceConfigGen) === null || _a === void 0 ? void 0 : _a.getIceConfig(this)
        });
    }
    closePeer(key) {
        this.peerConnections.delete(key);
    }
}
exports.BaseClient = BaseClient;


/***/ }),

/***/ "./src/comms/server/iceconfiggenerator.ts":
/*!************************************************!*\
  !*** ./src/comms/server/iceconfiggenerator.ts ***!
  \************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.IceConfigGenerator = void 0;
const crypto = __webpack_require__(/*! crypto */ "crypto");
class IceConfigGenerator {
    constructor(stun, turn, secret, validTime = 3600) {
        this.stun = stun;
        this.turn = turn;
        this.secret = secret;
        this.validTime = validTime;
    }
    getIceConfig(user) {
        const validTimeStamp = (Math.floor(Date.now() / 1000) + this.validTime);
        const uname = [validTimeStamp, user.id].join(':');
        const hmac = crypto.createHmac('sha1', this.secret);
        hmac.setEncoding('base64');
        hmac.write(uname);
        hmac.end();
        const passwd = hmac.read();
        return {
            iceServers: [
                {
                    urls: this.stun,
                    credential: passwd,
                    username: uname
                },
                {
                    urls: this.turn,
                    credential: passwd,
                    username: uname
                }
            ]
        };
    }
}
exports.IceConfigGenerator = IceConfigGenerator;


/***/ }),

/***/ "./src/comms/socketcomms.ts":
/*!**********************************!*\
  !*** ./src/comms/socketcomms.ts ***!
  \**********************************/
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.SocketComms = void 0;
var SocketComms;
(function (SocketComms) {
    let MessageType;
    (function (MessageType) {
        MessageType.PeerSignal = "comms.peersignal";
        MessageType.InitConnection = "comms.initconn";
    })(MessageType = SocketComms.MessageType || (SocketComms.MessageType = {}));
})(SocketComms = exports.SocketComms || (exports.SocketComms = {}));


/***/ }),

/***/ "./src/server/clientmanager.ts":
/*!*************************************!*\
  !*** ./src/server/clientmanager.ts ***!
  \*************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.ClientManager = void 0;
const constants_1 = __webpack_require__(/*! ./constants */ "./src/server/constants.ts");
const game_1 = __webpack_require__(/*! ./game */ "./src/server/game.ts");
const mococlient_1 = __webpack_require__(/*! ./mococlient */ "./src/server/mococlient.ts");
const iceconfiggenerator_1 = __webpack_require__(/*! ../comms/server/iceconfiggenerator */ "./src/comms/server/iceconfiggenerator.ts");
const fs = __webpack_require__(/*! fs */ "fs");
class ClientManager {
    constructor(io) {
        this.io = io;
        this.clients = new Map();
        this.games = new Map();
        const conf = JSON.parse(fs.readFileSync('./iceconfig.json', 'utf8'));
        this.iceConfigGen = new iceconfiggenerator_1.IceConfigGenerator(conf.stun, conf.turn, conf.secret, conf.ttl);
        this.io.on('connection', (socket) => {
            console.log(`user connected: ${socket.id}`);
            const client = new mococlient_1.MoCoClient(this, socket, this.iceConfigGen);
            this.clients.set(client.id, client);
        });
    }
    createGame(client, key, hash) {
        do {
            var key = this.createGameKey();
        } while (this.games.has(key));
        this.games.set(key, new game_1.Game(client, this, key));
    }
    getGame(key) {
        return this.games.get(key);
    }
    dropGame(key) {
        this.games.delete(key);
    }
    dropClient(id) {
        this.clients.delete(id);
    }
    createGameKey() {
        var key = "";
        for (var i = 0; i < constants_1.Constants.KeyLength; i++) {
            key += constants_1.Constants.KeyChars[Math.floor(Math.random() * constants_1.Constants.KeyChars.length)];
        }
        return key;
    }
}
exports.ClientManager = ClientManager;


/***/ }),

/***/ "./src/server/constants.ts":
/*!*********************************!*\
  !*** ./src/server/constants.ts ***!
  \*********************************/
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.Constants = void 0;
var Constants;
(function (Constants) {
    Constants.KeyLength = 5;
    Constants.KeyChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
})(Constants = exports.Constants || (exports.Constants = {}));


/***/ }),

/***/ "./src/server/game.ts":
/*!****************************!*\
  !*** ./src/server/game.ts ***!
  \****************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.Game = void 0;
const mococomms_1 = __webpack_require__(/*! ../common/mococomms */ "./src/common/mococomms.ts");
class Game {
    constructor(client, owner, key) {
        this.client = client;
        this.owner = owner;
        this.key = key;
        this.playerId = 1;
        this.players = new Map();
        client.socket.emit(mococomms_1.MoCoComms.Events.CreateGameResponse, { key: key });
        console.log(`Game started ${this.key}`);
    }
    addPlayer(player) {
        var _a;
        player.setNextEntId((this.playerId++) * 32);
        const playerInfo = player.getPeerInfo();
        console.log(`player ${(_a = playerInfo === null || playerInfo === void 0 ? void 0 : playerInfo.name) !== null && _a !== void 0 ? _a : "Unnamed"} connecting to ${this.key}`);
        this.client.startPeerConnect(player, true);
    }
    dropPlayer(uuid) {
    }
}
exports.Game = Game;


/***/ }),

/***/ "./src/server/mococlient.ts":
/*!**********************************!*\
  !*** ./src/server/mococlient.ts ***!
  \**********************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.MoCoClient = void 0;
const baseclient_1 = __webpack_require__(/*! ../comms/server/baseclient */ "./src/comms/server/baseclient.ts");
const mococomms_1 = __webpack_require__(/*! ../common/mococomms */ "./src/common/mococomms.ts");
class MoCoClient extends baseclient_1.BaseClient {
    constructor(owner, socket, iceGen) {
        super(socket, null, iceGen);
        this.owner = owner;
        this.name = "";
        socket.on(mococomms_1.MoCoComms.Events.CreateGameRequest, (req) => {
            owner.createGame(this, req.key, req.hash);
        });
        socket.on(mococomms_1.MoCoComms.Events.JoinGameRequest, (req) => {
            this.name = req.name;
            const game = owner.getGame(req.key);
            game.addPlayer(this);
        });
        socket.on(mococomms_1.MoCoComms.Events.Disconnected, () => {
            this.owner.dropClient(this.id);
        });
    }
    getPeerInfo() {
        return { name: this.name, uuid: this.id };
    }
    setNextEntId(num) {
        this.entId = num;
    }
}
exports.MoCoClient = MoCoClient;


/***/ }),

/***/ "./src/server/server.ts":
/*!******************************!*\
  !*** ./src/server/server.ts ***!
  \******************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.Server = void 0;
const Http = __webpack_require__(/*! http */ "http");
const Https = __webpack_require__(/*! https */ "https");
const fs = __webpack_require__(/*! fs */ "fs");
const Express = __webpack_require__(/*! express */ "express");
const Io = __webpack_require__(/*! socket.io */ "socket.io");
const clientmanager_1 = __webpack_require__(/*! ./clientmanager */ "./src/server/clientmanager.ts");
class Server {
    constructor(enableHttps = false, port = process.env.PORT || "80") {
        this.app = Express();
        this.app.use('/node_modules', Express.static('node_modules'));
        this.app.use(Express.static('public'));
        this.app.use('/play/:key', (req, res) => {
            res.sendFile("public/play/index.html", { root: process.cwd() });
        });
        let server = null;
        if (enableHttps) {
            server = this.CreateHttpsServer(this.app);
            const redirectApp = Express();
            redirectApp.get('*', (req, res) => {
                res.redirect('https://' + req.headers.host + req.url);
            });
            this.CreateHttpServer(redirectApp, parseInt(port));
        }
        else {
            server = this.CreateHttpServer(this.app, parseInt(port));
        }
        this.io = Io(server);
        this.clientMan = new clientmanager_1.ClientManager(this.io);
    }
    CreateHttpsServer(app) {
        const privateKey = fs.readFileSync('/etc/letsencrypt/live/starduel.ciderpunk.net/privkey.pem', 'utf8');
        const certificate = fs.readFileSync('/etc/letsencrypt/live/starduel.ciderpunk.net/cert.pem', 'utf8');
        const ca = fs.readFileSync('/etc/letsencrypt/live/starduel.ciderpunk.net/chain.pem', 'utf8');
        const credentials = {
            key: privateKey,
            cert: certificate,
            ca: ca
        };
        const httpsServer = Https.createServer(credentials, app);
        httpsServer.listen(443, () => {
            console.log('listening on *:443');
        });
        return httpsServer;
    }
    CreateHttpServer(app, port = 80) {
        const httpServer = Http.createServer(app);
        httpServer.listen(port, () => {
            console.log('listening on *:' + port);
        });
        return httpServer;
    }
}
exports.Server = Server;


/***/ }),

/***/ "crypto":
/*!*************************!*\
  !*** external "crypto" ***!
  \*************************/
/***/ ((module) => {

module.exports = require("crypto");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/***/ ((module) => {

module.exports = require("express");

/***/ }),

/***/ "fs":
/*!*********************!*\
  !*** external "fs" ***!
  \*********************/
/***/ ((module) => {

module.exports = require("fs");

/***/ }),

/***/ "http":
/*!***********************!*\
  !*** external "http" ***!
  \***********************/
/***/ ((module) => {

module.exports = require("http");

/***/ }),

/***/ "https":
/*!************************!*\
  !*** external "https" ***!
  \************************/
/***/ ((module) => {

module.exports = require("https");

/***/ }),

/***/ "socket.io":
/*!****************************!*\
  !*** external "socket.io" ***!
  \****************************/
/***/ ((module) => {

module.exports = require("socket.io");

/***/ }),

/***/ "uuid":
/*!***********************!*\
  !*** external "uuid" ***!
  \***********************/
/***/ ((module) => {

module.exports = require("uuid");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
var exports = __webpack_exports__;
/*!*****************************!*\
  !*** ./src/server/index.ts ***!
  \*****************************/

Object.defineProperty(exports, "__esModule", ({ value: true }));
const server_1 = __webpack_require__(/*! ./server */ "./src/server/server.ts");
let s = new server_1.Server(false, "8000");

})();

/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VydmVyLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7QUFDQSxJQUFpQixTQUFTLENBZ0R6QjtBQWhERCxXQUFpQixTQUFTO0lBRXhCLElBQWlCLE1BQU0sQ0FXdEI7SUFYRCxXQUFpQixNQUFNO1FBQ1Isd0JBQWlCLEdBQUcsTUFBTTtRQUMxQix5QkFBa0IsR0FBRyxNQUFNO1FBQzNCLHNCQUFlLEdBQUcsTUFBTTtRQUN4QixrQkFBVyxHQUFHLEtBQUs7UUFDbkIsaUJBQVUsR0FBRyxNQUFNO1FBQ25CLHdCQUFpQixHQUFHLE1BQU07UUFDMUIsdUJBQWdCLEdBQUcsTUFBTTtRQUN6QixZQUFLLEdBQUcsS0FBSztRQUNiLGdCQUFTLEdBQUcsU0FBUztRQUNyQixtQkFBWSxHQUFHLFlBQVk7SUFDMUMsQ0FBQyxFQVhnQixNQUFNLEdBQU4sZ0JBQU0sS0FBTixnQkFBTSxRQVd0QjtBQW1DSCxDQUFDLEVBaERnQixTQUFTLEdBQVQsaUJBQVMsS0FBVCxpQkFBUyxRQWdEekI7Ozs7Ozs7Ozs7Ozs7O0FDL0NELHFEQUE4QjtBQUM5Qiw4RkFBNkM7QUFFN0MsTUFBc0IsVUFBVTtJQUs5QixZQUE0QixNQUFnQixFQUFFLFdBQWtCLElBQUksRUFBWSxlQUFtQyxJQUFJO1FBQTNGLFdBQU0sR0FBTixNQUFNLENBQVU7UUFBb0MsaUJBQVksR0FBWixZQUFZLENBQTJCO1FBRmhILFVBQUssR0FBVSxDQUFDO1FBR3JCLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxHQUFHLEVBQW1CO1FBRWpELE1BQU0sQ0FBQyxFQUFFLENBQUMseUJBQVcsQ0FBQyxXQUFXLENBQUMsVUFBVSxFQUFFLENBQUMsR0FBa0MsRUFBRSxFQUFFO1lBRW5GLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUM7WUFDaEQsSUFBSSxNQUFNLEtBQUssSUFBSSxFQUFDO2dCQUVsQixNQUFNLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxHQUFHLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxJQUFJLENBQUM7YUFDL0M7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUVILE1BQU0sQ0FBQyxFQUFFLENBQUMsWUFBWSxFQUFFLEdBQUUsRUFBRTtZQUUxQixJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxDQUFDLE1BQU0sRUFBQyxHQUFHLEVBQUUsR0FBRyxFQUFDLEVBQUUsR0FBRSxNQUFNLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUFHLENBQUMsQ0FBQztRQUM5RSxDQUFDLENBQUM7UUFFRixJQUFJLENBQUMsRUFBRSxHQUFHLFFBQVEsYUFBUixRQUFRLGNBQVIsUUFBUSxHQUFJLElBQUksRUFBRTtJQUM5QixDQUFDO0lBRU0sV0FBVztRQUNoQixPQUFPLElBQUk7SUFDYixDQUFDO0lBRU0sY0FBYyxDQUFDLEtBQWEsRUFBRSxHQUFVLEVBQUUsSUFBVztRQUUxRCxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUM7WUFFakMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFDLEtBQUssQ0FBQztTQUNwQztRQUVELElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLHlCQUFXLENBQUMsV0FBVyxDQUFDLFVBQVUsRUFBRSxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBbUMsQ0FBQztJQUNqSCxDQUFDO0lBSU0sZ0JBQWdCLENBQUMsTUFBYyxFQUFFLFlBQW9CLElBQUksRUFBRSxNQUFhLElBQUk7O1FBRWpGLElBQUksU0FBUyxFQUFDO1lBQ1osR0FBRyxHQUFHLElBQUksRUFBRTtZQUVaLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLEdBQUcsQ0FBQztTQUMxQztRQUVELElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxNQUFNLENBQUM7UUFFckMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMseUJBQVcsQ0FBQyxXQUFXLENBQUMsY0FBYyxFQUFFO1lBQ3ZELEdBQUcsRUFBRyxHQUFHO1lBQ1QsT0FBTyxFQUFHLFNBQVM7WUFDbkIsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLFFBQVEsRUFBRSxNQUFNLENBQUMsV0FBVyxFQUFFO1lBQzlCLFNBQVMsRUFBRSxVQUFJLENBQUMsWUFBWSwwQ0FBRSxZQUFZLENBQUMsSUFBSSxDQUFDO1NBQ1gsQ0FBQztJQUUxQyxDQUFDO0lBRUQsU0FBUyxDQUFDLEdBQVc7UUFFbkIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDO0lBQ2xDLENBQUM7Q0FFRjtBQWxFRCxnQ0FrRUM7Ozs7Ozs7Ozs7Ozs7O0FDdEVELDJEQUFpQztBQUVqQyxNQUFhLGtCQUFrQjtJQUU3QixZQUFzQixJQUFXLEVBQVksSUFBVyxFQUFhLE1BQWEsRUFBWSxZQUFtQixJQUFJO1FBQS9GLFNBQUksR0FBSixJQUFJLENBQU87UUFBWSxTQUFJLEdBQUosSUFBSSxDQUFPO1FBQWEsV0FBTSxHQUFOLE1BQU0sQ0FBTztRQUFZLGNBQVMsR0FBVCxTQUFTLENBQWM7SUFBRSxDQUFDO0lBRXhILFlBQVksQ0FBQyxJQUFZO1FBQ3ZCLE1BQU0sY0FBYyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLEdBQUcsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUN2RSxNQUFNLEtBQUssR0FBRyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztRQUNqRCxNQUFNLElBQUksR0FBRyxNQUFNLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ25ELElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDO1FBQzFCLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO1FBQ2pCLElBQUksQ0FBQyxHQUFHLEVBQUU7UUFDVixNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxFQUFFO1FBQzFCLE9BQU87WUFDTCxVQUFVLEVBQUM7Z0JBQ1Q7b0JBQ0UsSUFBSSxFQUFDLElBQUksQ0FBQyxJQUFJO29CQUNkLFVBQVUsRUFBQyxNQUFNO29CQUNqQixRQUFRLEVBQUMsS0FBSztpQkFDZjtnQkFDRDtvQkFDRSxJQUFJLEVBQUMsSUFBSSxDQUFDLElBQUk7b0JBQ2QsVUFBVSxFQUFDLE1BQU07b0JBQ2pCLFFBQVEsRUFBQyxLQUFLO2lCQUNmO2FBQ0Y7U0FBQztJQUNOLENBQUM7Q0FFRjtBQTNCRCxnREEyQkM7Ozs7Ozs7Ozs7Ozs7O0FDOUJELElBQWlCLFdBQVcsQ0FzQjNCO0FBdEJELFdBQWlCLFdBQVc7SUFFMUIsSUFBaUIsV0FBVyxDQUczQjtJQUhELFdBQWlCLFdBQVc7UUFDYixzQkFBVSxHQUFHLGtCQUFrQjtRQUMvQiwwQkFBYyxHQUFHLGdCQUFnQjtJQUNoRCxDQUFDLEVBSGdCLFdBQVcsR0FBWCx1QkFBVyxLQUFYLHVCQUFXLFFBRzNCO0FBaUJILENBQUMsRUF0QmdCLFdBQVcsR0FBWCxtQkFBVyxLQUFYLG1CQUFXLFFBc0IzQjs7Ozs7Ozs7Ozs7Ozs7QUNwQkQsd0ZBQXVDO0FBQ3ZDLHlFQUE2QjtBQUM3QiwyRkFBd0M7QUFDeEMsdUlBQXVFO0FBQ3ZFLCtDQUF5QjtBQUl6QixNQUFhLGFBQWE7SUErRHhCLFlBQW9CLEVBQWtCO1FBQWxCLE9BQUUsR0FBRixFQUFFLENBQWdCO1FBQ3BDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxHQUFHLEVBQXVCO1FBQzdDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxHQUFHLEVBQWlCO1FBR3JDLE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLFlBQVksQ0FBQyxrQkFBa0IsRUFBRSxNQUFNLENBQUMsQ0FBa0I7UUFDckYsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLHVDQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxHQUFHLENBQUM7UUFNdkYsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsWUFBWSxFQUFFLENBQUMsTUFBZ0IsRUFBQyxFQUFFO1lBQzNDLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLE1BQU0sQ0FBQyxFQUFFLEVBQUUsQ0FBQztZQUMzQyxNQUFNLE1BQU0sR0FBRyxJQUFLLHVCQUFVLENBQUMsSUFBSSxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDO1lBQy9ELElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsTUFBTSxDQUFDO1FBQ3JDLENBQUMsQ0FBQztJQUNKLENBQUM7SUF4RUQsVUFBVSxDQUFDLE1BQWtCLEVBQUUsR0FBVyxFQUFFLElBQVk7UUFFdEQsR0FBRTtZQUNBLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxhQUFhLEVBQUU7U0FDL0IsUUFBTSxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBQztRQUUzQixJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsSUFBSSxXQUFJLENBQUMsTUFBTSxFQUFFLElBQUksRUFBRSxHQUFHLENBQUMsQ0FBQztJQUNsRCxDQUFDO0lBRUQsT0FBTyxDQUFDLEdBQVU7UUFDaEIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUM7SUFDNUIsQ0FBQztJQUVELFFBQVEsQ0FBQyxHQUFXO1FBQ2xCLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQztJQUN4QixDQUFDO0lBRUQsVUFBVSxDQUFDLEVBQVM7UUFDbEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDO0lBQ3pCLENBQUM7SUFFRCxhQUFhO1FBQ1gsSUFBSSxHQUFHLEdBQUcsRUFBRTtRQUNaLEtBQUssSUFBSyxDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQUMsR0FBRyxxQkFBUyxDQUFDLFNBQVMsRUFBRSxDQUFDLEVBQUUsRUFBQztZQUMxQyxHQUFHLElBQUsscUJBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcscUJBQVMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDbkY7UUFDRCxPQUFPLEdBQUcsQ0FBQztJQUNiLENBQUM7Q0E4Q0Y7QUFqRkQsc0NBaUZDOzs7Ozs7Ozs7Ozs7OztBQzNGRCxJQUFpQixTQUFTLENBS3pCO0FBTEQsV0FBaUIsU0FBUztJQUNYLG1CQUFTLEdBQVUsQ0FBQztJQUNwQixrQkFBUSxHQUFVLHNDQUFzQztBQUd2RSxDQUFDLEVBTGdCLFNBQVMsR0FBVCxpQkFBUyxLQUFULGlCQUFTLFFBS3pCOzs7Ozs7Ozs7Ozs7OztBQ0pELGdHQUFnRDtBQUdoRCxNQUFhLElBQUk7SUFnQmYsWUFBc0IsTUFBa0IsRUFBWSxLQUFvQixFQUFTLEdBQVU7UUFBckUsV0FBTSxHQUFOLE1BQU0sQ0FBWTtRQUFZLFVBQUssR0FBTCxLQUFLLENBQWU7UUFBUyxRQUFHLEdBQUgsR0FBRyxDQUFPO1FBYjNGLGFBQVEsR0FBVSxDQUFDO1FBY2pCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxHQUFHLEVBQXNCO1FBRTVDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLHFCQUFTLENBQUMsTUFBTSxDQUFDLGtCQUFrQixFQUFFLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBa0MsQ0FBQztRQUNyRyxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7SUFDekMsQ0FBQztJQWpCRCxTQUFTLENBQUMsTUFBa0I7O1FBQzFCLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUM7UUFDM0MsTUFBTSxVQUFVLEdBQUcsTUFBTSxDQUFDLFdBQVcsRUFBMEI7UUFDL0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLGdCQUFVLGFBQVYsVUFBVSx1QkFBVixVQUFVLENBQUUsSUFBSSxtQ0FBSSxTQUFTLGtCQUFrQixJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7UUFDaEYsSUFBSSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUcsSUFBSSxDQUFDO0lBQzdDLENBQUM7SUFFRCxVQUFVLENBQUMsSUFBVztJQUd0QixDQUFDO0NBWUY7QUExQkQsb0JBMEJDOzs7Ozs7Ozs7Ozs7OztBQzVCRCwrR0FBd0Q7QUFFeEQsZ0dBQWdEO0FBSWhELE1BQWEsVUFBVyxTQUFRLHVCQUFVO0lBSXhDLFlBQTZCLEtBQW9CLEVBQUUsTUFBZ0IsRUFBRSxNQUEwQjtRQUM3RixLQUFLLENBQUMsTUFBTSxFQUFFLElBQUksRUFBRSxNQUFNLENBQUM7UUFEQSxVQUFLLEdBQUwsS0FBSyxDQUFlO1FBRS9DLElBQUksQ0FBQyxJQUFJLEdBQUcsRUFBRTtRQUVkLE1BQU0sQ0FBQyxFQUFFLENBQUMscUJBQVMsQ0FBQyxNQUFNLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxHQUErQixFQUFDLEVBQUU7WUFDL0UsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsSUFBSSxDQUFDO1FBQzNDLENBQUMsQ0FBQztRQUVGLE1BQU0sQ0FBQyxFQUFFLENBQUMscUJBQVMsQ0FBQyxNQUFNLENBQUMsZUFBZSxFQUFFLENBQUMsR0FBNkIsRUFBQyxFQUFFO1lBQzNFLElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDLElBQUk7WUFDcEIsTUFBTSxJQUFJLEdBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDO1lBQ2xDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFdkIsQ0FBQyxDQUFDO1FBQ0YsTUFBTSxDQUFDLEVBQUUsQ0FBQyxxQkFBUyxDQUFDLE1BQU0sQ0FBQyxZQUFZLEVBQUUsR0FBRSxFQUFFO1lBQzNDLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUM7UUFDaEMsQ0FBQyxDQUFDO0lBQ0osQ0FBQztJQUVNLFdBQVc7UUFDaEIsT0FBTyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksRUFBQyxJQUFJLENBQUMsRUFBRSxFQUEwQjtJQUNsRSxDQUFDO0lBRUQsWUFBWSxDQUFDLEdBQVc7UUFDdEIsSUFBSSxDQUFDLEtBQUssR0FBRyxHQUFHO0lBQ2xCLENBQUM7Q0FHRjtBQWhDRCxnQ0FnQ0M7Ozs7Ozs7Ozs7Ozs7O0FDeENELHFEQUE0QjtBQUM1Qix3REFBOEI7QUFDOUIsK0NBQXdCO0FBQ3hCLDhEQUFtQztBQUNuQyw2REFBK0I7QUFFL0Isb0dBQWdEO0FBRWhELE1BQWEsTUFBTTtJQWdDakIsWUFBWSxjQUFzQixLQUFLLEVBQUUsT0FBYyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksSUFBSSxJQUFJO1FBQzdFLElBQUksQ0FBQyxHQUFHLEdBQUksT0FBTyxFQUFFLENBQUM7UUFFdEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsZUFBZSxFQUFFLE9BQU8sQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztRQUM5RCxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7UUFDdkMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFFLENBQUMsR0FBRyxFQUFDLEdBQUcsRUFBQyxFQUFFO1lBQ3BDLEdBQUcsQ0FBQyxRQUFRLENBQUMsd0JBQXdCLEVBQUUsRUFBRSxJQUFJLEVBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxFQUFDLENBQUM7UUFDL0QsQ0FBQyxDQUFDO1FBR0YsSUFBSSxNQUFNLEdBQU8sSUFBSTtRQUVyQixJQUFJLFdBQVcsRUFBQztZQUNkLE1BQU0sR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztZQUV6QyxNQUFNLFdBQVcsR0FBRyxPQUFPLEVBQUU7WUFDN0IsV0FBVyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxHQUFHLEVBQUMsR0FBRyxFQUFDLEVBQUU7Z0JBQzlCLEdBQUcsQ0FBQyxRQUFRLENBQUMsVUFBVSxHQUFHLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUN4RCxDQUFDLENBQUM7WUFDRixJQUFJLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNsRDthQUNHO1lBQ0YsTUFBTSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUN6RDtRQUdELElBQUksQ0FBQyxFQUFFLEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQztRQUNwQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksNkJBQWEsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDO0lBQzdDLENBQUM7SUFyRFMsaUJBQWlCLENBQUMsR0FBbUI7UUFDN0MsTUFBTSxVQUFVLEdBQUcsRUFBRSxDQUFDLFlBQVksQ0FBQywwREFBMEQsRUFBRSxNQUFNLENBQUM7UUFDdEcsTUFBTSxXQUFXLEdBQUcsRUFBRSxDQUFDLFlBQVksQ0FBQyx1REFBdUQsRUFBRSxNQUFNLENBQUM7UUFDcEcsTUFBTSxFQUFFLEdBQUcsRUFBRSxDQUFDLFlBQVksQ0FBQyx3REFBd0QsRUFBRSxNQUFNLENBQUM7UUFDNUYsTUFBTSxXQUFXLEdBQUc7WUFDbEIsR0FBRyxFQUFFLFVBQVU7WUFDZixJQUFJLEVBQUUsV0FBVztZQUNqQixFQUFFLEVBQUUsRUFBRTtTQUNQO1FBRUQsTUFBTSxXQUFXLEdBQUcsS0FBSyxDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsR0FBRyxDQUFDO1FBQ3hELFdBQVcsQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFDLEdBQUUsRUFBRTtZQUN6QixPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixDQUFDO1FBQ25DLENBQUMsQ0FBQztRQUNGLE9BQU8sV0FBVztJQUNwQixDQUFDO0lBRVMsZ0JBQWdCLENBQUMsR0FBbUIsRUFBRSxPQUFjLEVBQUU7UUFDOUQsTUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUM7UUFDekMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUMsR0FBRSxFQUFFO1lBQ3pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1FBQ3ZDLENBQUMsQ0FBQztRQUNGLE9BQU8sVUFBVTtJQUNuQixDQUFDO0NBZ0NGO0FBOURELHdCQThEQzs7Ozs7Ozs7Ozs7QUN0RUQ7Ozs7Ozs7Ozs7QUNBQTs7Ozs7Ozs7OztBQ0FBOzs7Ozs7Ozs7O0FDQUE7Ozs7Ozs7Ozs7QUNBQTs7Ozs7Ozs7OztBQ0FBOzs7Ozs7Ozs7O0FDQUE7Ozs7OztVQ0FBO1VBQ0E7O1VBRUE7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7O1VBRUE7VUFDQTs7VUFFQTtVQUNBO1VBQ0E7Ozs7Ozs7Ozs7OztBQ3RCQSwrRUFBZ0M7QUFDaEMsSUFBSSxDQUFDLEdBQUcsSUFBSSxlQUFNLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vbW9jby8uL3NyYy9jb21tb24vbW9jb2NvbW1zLnRzIiwid2VicGFjazovL21vY28vLi9zcmMvY29tbXMvc2VydmVyL2Jhc2VjbGllbnQudHMiLCJ3ZWJwYWNrOi8vbW9jby8uL3NyYy9jb21tcy9zZXJ2ZXIvaWNlY29uZmlnZ2VuZXJhdG9yLnRzIiwid2VicGFjazovL21vY28vLi9zcmMvY29tbXMvc29ja2V0Y29tbXMudHMiLCJ3ZWJwYWNrOi8vbW9jby8uL3NyYy9zZXJ2ZXIvY2xpZW50bWFuYWdlci50cyIsIndlYnBhY2s6Ly9tb2NvLy4vc3JjL3NlcnZlci9jb25zdGFudHMudHMiLCJ3ZWJwYWNrOi8vbW9jby8uL3NyYy9zZXJ2ZXIvZ2FtZS50cyIsIndlYnBhY2s6Ly9tb2NvLy4vc3JjL3NlcnZlci9tb2NvY2xpZW50LnRzIiwid2VicGFjazovL21vY28vLi9zcmMvc2VydmVyL3NlcnZlci50cyIsIndlYnBhY2s6Ly9tb2NvL2V4dGVybmFsIFwiY3J5cHRvXCIiLCJ3ZWJwYWNrOi8vbW9jby9leHRlcm5hbCBcImV4cHJlc3NcIiIsIndlYnBhY2s6Ly9tb2NvL2V4dGVybmFsIFwiZnNcIiIsIndlYnBhY2s6Ly9tb2NvL2V4dGVybmFsIFwiaHR0cFwiIiwid2VicGFjazovL21vY28vZXh0ZXJuYWwgXCJodHRwc1wiIiwid2VicGFjazovL21vY28vZXh0ZXJuYWwgXCJzb2NrZXQuaW9cIiIsIndlYnBhY2s6Ly9tb2NvL2V4dGVybmFsIFwidXVpZFwiIiwid2VicGFjazovL21vY28vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vbW9jby8uL3NyYy9zZXJ2ZXIvaW5kZXgudHMiXSwic291cmNlc0NvbnRlbnQiOlsiXHJcbmV4cG9ydCBuYW1lc3BhY2UgTW9Db0NvbW1ze1xyXG5cclxuICBleHBvcnQgbmFtZXNwYWNlIEV2ZW50c3tcclxuICAgIGV4cG9ydCBjb25zdCBDcmVhdGVHYW1lUmVxdWVzdCA9IFwiY2dycVwiXHJcbiAgICBleHBvcnQgY29uc3QgQ3JlYXRlR2FtZVJlc3BvbnNlID0gXCJjZ3JwXCJcclxuICAgIGV4cG9ydCBjb25zdCBKb2luR2FtZVJlcXVlc3QgPSBcImpncnFcIlxyXG4gICAgZXhwb3J0IGNvbnN0IFJlYWR5VG9Kb2luID0gXCJydGpcIlxyXG4gICAgZXhwb3J0IGNvbnN0IFBlZXJTaWduYWwgPSBcInNnbmxcIlxyXG4gICAgZXhwb3J0IGNvbnN0IFBsYXllckpvaW5SZXF1ZXN0ID0gXCJwanJxXCJcclxuICAgIGV4cG9ydCBjb25zdCBQbGF5ZXJEaXNjb25uZWN0ID0gXCJwZHJxXCJcclxuICAgIGV4cG9ydCBjb25zdCBFcnJvciA9IFwiRXJyXCJcclxuICAgIGV4cG9ydCBjb25zdCBDb25uZWN0ZWQgPSBcImNvbm5lY3RcIlxyXG4gICAgZXhwb3J0IGNvbnN0IERpc2Nvbm5lY3RlZCA9IFwiZGlzY29ubmVjdFwiXHJcbiAgfVxyXG5cclxuICBleHBvcnQgaW50ZXJmYWNlIFBsYXllckpvaW5pbmd7XHJcbiAgICB1dWlkOnN0cmluZ1xyXG4gICAgbmFtZTpzdHJpbmdcclxuICB9XHJcblxyXG4gIGV4cG9ydCBpbnRlcmZhY2UgUGxheWVyRGlzY29ubmVjdGluZ3tcclxuICAgIHV1aWQ6c3RyaW5nXHJcbiAgfVxyXG5cclxuICBleHBvcnQgaW50ZXJmYWNlIEVycm9yUmVzcG9uc2V7XHJcbiAgICBtZXNzYWdlOnN0cmluZ1xyXG4gIH1cclxuXHJcbiAgZXhwb3J0IGludGVyZmFjZSBKb2luR2FtZVJlcXVlc3R7XHJcbiAgICBrZXk6c3RyaW5nXHJcbiAgICBuYW1lOnN0cmluZ1xyXG4gIH0gXHJcblxyXG4gIGV4cG9ydCBpbnRlcmZhY2UgQ3JlYXRlR2FtZVJlcXVlc3R7XHJcbiAgICBrZXk6c3RyaW5nXHJcbiAgICBoYXNoOnN0cmluZ1xyXG4gIH1cclxuXHJcbiAgZXhwb3J0IGludGVyZmFjZSBDcmVhdGVHYW1lUmVzcG9uc2V7XHJcbiAgICBrZXk6c3RyaW5nXHJcbiAgICBoYXNoOnN0cmluZ1xyXG4gIH1cclxuXHJcbiAgZXhwb3J0IGludGVyZmFjZSBQbGF5ZXJJbmZve1xyXG4gICAgbmFtZTpzdHJpbmdcclxuICAgIHV1aWQ6c3RyaW5nXHJcbiAgfVxyXG5cclxufSIsImltcG9ydCAqIGFzIElvIGZyb20gXCJzb2NrZXQuaW9cIlxyXG5pbXBvcnQgeyBJQ2xpZW50LCBJSWNlQ29uZmlnR2VuZXJhdG9yIH0gZnJvbSBcIi4vaW50ZXJmYWNlc1wiO1xyXG5pbXBvcnQgdXVpZCA9IHJlcXVpcmUoXCJ1dWlkXCIpO1xyXG5pbXBvcnQgeyBTb2NrZXRDb21tcyB9IGZyb20gXCIuLi9zb2NrZXRjb21tc1wiO1xyXG5cclxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIEJhc2VDbGllbnQgaW1wbGVtZW50cyBJQ2xpZW50e1xyXG4gIHB1YmxpYyByZWFkb25seSBpZDogc3RyaW5nXHJcblxyXG4gIHB1YmxpYyBlbnRJZDpudW1iZXIgPSAwXHJcblxyXG4gIGNvbnN0cnVjdG9yKHB1YmxpYyByZWFkb25seSBzb2NrZXQ6SW8uU29ja2V0LCBjbGllbnRJZDpzdHJpbmcgPSBudWxsLCBwcm90ZWN0ZWQgaWNlQ29uZmlnR2VuOklJY2VDb25maWdHZW5lcmF0b3IgPSBudWxsKXtcclxuICAgIHRoaXMucGVlckNvbm5lY3Rpb25zID0gbmV3IE1hcDxzdHJpbmcsIElDbGllbnQ+KClcclxuICAgIC8vd2hlbiB3ZSBnZXQgYSBjb25uZWN0aW9uIGF0dGVtcHQuLi5cclxuICAgIHNvY2tldC5vbihTb2NrZXRDb21tcy5NZXNzYWdlVHlwZS5QZWVyU2lnbmFsLCAocmVxOlNvY2tldENvbW1zLlBheWxvYWQuUGVlclNpZ25hbCkgPT4ge1xyXG4gICAgICAvL2ZpbmQgdGhlIHBlZXIgY29ubmVjdGlvbiByZWNvcmRcclxuICAgICAgY29uc3QgdGFyZ2V0ID0gdGhpcy5wZWVyQ29ubmVjdGlvbnMuZ2V0KHJlcS5rZXkpXHJcbiAgICAgIGlmICh0YXJnZXQgIT09IG51bGwpe1xyXG4gICAgICAgIC8vYW5kIHNlbmQgaXQuLi5cclxuICAgICAgICB0YXJnZXQuc2VuZFBlZXJTaWduYWwodGhpcywgcmVxLmtleSwgcmVxLmRhdGEpXHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgIHNvY2tldC5vbihcImRpc2Nvbm5lY3RcIiwgKCk9PnsgXHJcbiAgICAgIC8vcmVtb3ZlIHBlZXIgY29ubmVjdGlvbnMgdG8gdXNcclxuICAgICAgdGhpcy5wZWVyQ29ubmVjdGlvbnMuZm9yRWFjaCgoY2xpZW50LGtleSwgbWFwKT0+eyBjbGllbnQuY2xvc2VQZWVyKGtleSkgICB9KVxyXG4gICAgfSlcclxuICAgIFxyXG4gICAgdGhpcy5pZCA9IGNsaWVudElkID8/IHV1aWQoKVxyXG4gIH1cclxuXHJcbiAgcHVibGljIGdldFBlZXJJbmZvKCk6b2JqZWN0e1xyXG4gICAgcmV0dXJuIG51bGxcclxuICB9XHJcblxyXG4gIHB1YmxpYyBzZW5kUGVlclNpZ25hbChvdGhlcjpJQ2xpZW50LCBrZXk6c3RyaW5nLCBkYXRhOm9iamVjdCkge1xyXG4gICAgLy9jaGVjayB3ZSBoYXZlIGEgcmVjb3JkIG9mIHRoaXMgcGVlciBjb25uZWN0aW9uXHJcbiAgICBpZiAoIXRoaXMucGVlckNvbm5lY3Rpb25zLmhhcyhrZXkpKXtcclxuICAgICAgLy9hZGQgaXRcclxuICAgICAgdGhpcy5wZWVyQ29ubmVjdGlvbnMuc2V0KGtleSxvdGhlcilcclxuICAgIH1cclxuICAgIC8vdHJhbnNmZXIgdGhlIGRhdGEuLi5cclxuICAgIHRoaXMuc29ja2V0LmVtaXQoU29ja2V0Q29tbXMuTWVzc2FnZVR5cGUuUGVlclNpZ25hbCwgeyBrZXk6IGtleSwgZGF0YTogZGF0YX0gYXMgU29ja2V0Q29tbXMuUGF5bG9hZC5QZWVyU2lnbmFsKVxyXG4gIH1cclxuXHJcbiAgcmVhZG9ubHkgcGVlckNvbm5lY3Rpb25zOk1hcDxzdHJpbmcsIElDbGllbnQ+IFxyXG5cclxuICBwdWJsaWMgc3RhcnRQZWVyQ29ubmVjdCh0YXJnZXQ6SUNsaWVudCwgaXNQcmltYXJ5OmJvb2xlYW4gPSB0cnVlLCBrZXk6c3RyaW5nID0gbnVsbCk6dm9pZHtcclxuICAgIC8vY3JlYXRlIGEgbmV3IGtleSBmb3IgdGhpcyBjb25uZWN0aW9uXHJcbiAgICBpZiAoaXNQcmltYXJ5KXtcclxuICAgICAga2V5ID0gdXVpZCgpXHJcbiAgICAgIC8vcHJlcCB0aGUgdGFyZ2V0IGZvciB0aGUgY29ubmVjdGlvblxyXG4gICAgICB0YXJnZXQuc3RhcnRQZWVyQ29ubmVjdCh0aGlzLCBmYWxzZSwga2V5KVxyXG4gICAgfVxyXG4gICAgLy9jcmVhdGUgcGVlciBjb25uZWN0aW9uIHJlY29yZFxyXG4gICAgdGhpcy5wZWVyQ29ubmVjdGlvbnMuc2V0KGtleSwgdGFyZ2V0KVxyXG4gICAgLy90ZWxsIGNsaWVudCB0byBzdGFydCBjb25uZWN0aW9uIHByb2Nlc3NcclxuICAgIHRoaXMuc29ja2V0LmVtaXQoU29ja2V0Q29tbXMuTWVzc2FnZVR5cGUuSW5pdENvbm5lY3Rpb24sIHsgXHJcbiAgICAgIGtleSA6IGtleSwgXHJcbiAgICAgIHByaW1hcnkgOiBpc1ByaW1hcnksXHJcbiAgICAgIGVudElkOiB0aGlzLmVudElkLFxyXG4gICAgICBwZWVySW5mbzogdGFyZ2V0LmdldFBlZXJJbmZvKCksXHJcbiAgICAgIGljZUNvbmZpZzogdGhpcy5pY2VDb25maWdHZW4/LmdldEljZUNvbmZpZyh0aGlzKVxyXG4gICAgfSBhcyBTb2NrZXRDb21tcy5QYXlsb2FkLkluaXRDb25uZWN0aW9uKVxyXG4gICAgXHJcbiAgfVxyXG5cclxuICBjbG9zZVBlZXIoa2V5OiBzdHJpbmcpIHtcclxuICAgIC8vY2FsbGVkIGJ5IGRpc2Nvbm5lY3RlZCBwZWVycyB0byByZW1vdmUgdGhlbXNlbHZlcyBmcm9tIG91ciBsaXN0XHJcbiAgICB0aGlzLnBlZXJDb25uZWN0aW9ucy5kZWxldGUoa2V5KVxyXG4gIH1cclxuXHJcbn0iLCJpbXBvcnQgeyBJSWNlQ29uZmlnR2VuZXJhdG9yLCBJQ2xpZW50IH0gZnJvbSBcIi4vaW50ZXJmYWNlc1wiXHJcbmltcG9ydCBjcnlwdG8gPSByZXF1aXJlKCdjcnlwdG8nKVxyXG5cclxuZXhwb3J0IGNsYXNzIEljZUNvbmZpZ0dlbmVyYXRvciBpbXBsZW1lbnRzIElJY2VDb25maWdHZW5lcmF0b3J7XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByb3RlY3RlZCBzdHVuOnN0cmluZywgcHJvdGVjdGVkIHR1cm46c3RyaW5nLCAgcHJvdGVjdGVkIHNlY3JldDpzdHJpbmcsIHByb3RlY3RlZCB2YWxpZFRpbWU6bnVtYmVyID0gMzYwMCl7fVxyXG5cclxuICBnZXRJY2VDb25maWcodXNlcjpJQ2xpZW50KTpvYmplY3Qge1xyXG4gICAgY29uc3QgdmFsaWRUaW1lU3RhbXAgPSAoTWF0aC5mbG9vcihEYXRlLm5vdygpIC8gMTAwMCkgKyB0aGlzLnZhbGlkVGltZSlcclxuICAgIGNvbnN0IHVuYW1lID0gW3ZhbGlkVGltZVN0YW1wLCB1c2VyLmlkXS5qb2luKCc6JylcclxuICAgIGNvbnN0IGhtYWMgPSBjcnlwdG8uY3JlYXRlSG1hYygnc2hhMScsIHRoaXMuc2VjcmV0KVxyXG4gICAgaG1hYy5zZXRFbmNvZGluZygnYmFzZTY0JylcclxuICAgIGhtYWMud3JpdGUodW5hbWUpXHJcbiAgICBobWFjLmVuZCgpXHJcbiAgICBjb25zdCBwYXNzd2QgPSBobWFjLnJlYWQoKVxyXG4gICAgcmV0dXJuIHtcclxuICAgICAgaWNlU2VydmVyczpbXHJcbiAgICAgICAgeyBcclxuICAgICAgICAgIHVybHM6dGhpcy5zdHVuLFxyXG4gICAgICAgICAgY3JlZGVudGlhbDpwYXNzd2QsXHJcbiAgICAgICAgICB1c2VybmFtZTp1bmFtZVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgdXJsczp0aGlzLnR1cm4sXHJcbiAgICAgICAgICBjcmVkZW50aWFsOnBhc3N3ZCxcclxuICAgICAgICAgIHVzZXJuYW1lOnVuYW1lXHJcbiAgICAgICAgfSBcclxuICAgICAgXX1cclxuICB9XHJcblxyXG59IiwiZXhwb3J0IG5hbWVzcGFjZSBTb2NrZXRDb21tc3tcclxuXHJcbiAgZXhwb3J0IG5hbWVzcGFjZSBNZXNzYWdlVHlwZXtcclxuICAgIGV4cG9ydCBjb25zdCBQZWVyU2lnbmFsID0gXCJjb21tcy5wZWVyc2lnbmFsXCJcclxuICAgIGV4cG9ydCBjb25zdCBJbml0Q29ubmVjdGlvbiA9IFwiY29tbXMuaW5pdGNvbm5cIlxyXG4gIH1cclxuXHJcbiAgZXhwb3J0IG5hbWVzcGFjZSBQYXlsb2Fke1xyXG5cclxuICAgIGV4cG9ydCBpbnRlcmZhY2UgUGVlclNpZ25hbHtcclxuICAgICAga2V5OnN0cmluZ1xyXG4gICAgICBkYXRhOm9iamVjdFxyXG4gICAgfVxyXG4gICAgZXhwb3J0IGludGVyZmFjZSBJbml0Q29ubmVjdGlvbntcclxuICAgICAga2V5OnN0cmluZ1xyXG4gICAgICBwcmltYXJ5OmJvb2xlYW5cclxuICAgICAgZW50SWQ6bnVtYmVyXHJcbiAgICAgIHBlZXJJbmZvOm9iamVjdFxyXG4gICAgICBpY2VDb25maWc6b2JqZWN0XHJcbiAgICB9XHJcblxyXG4gIH1cclxufVxyXG5cclxuIiwiaW1wb3J0ICogYXMgSW8gZnJvbSBcInNvY2tldC5pb1wiXHJcbmltcG9ydCB7IElDbGllbnRNYW5hZ2VyLCBJR2FtZSwgSU1vQ29DbGllbnQsIEljZUNvbmZvZ0pzb24sIFNpbXBsZUljZUNvbmZvZ0pzb24gfSBmcm9tIFwiLi9pbnRlcmZhY2VzXCJcclxuaW1wb3J0IHsgQ29uc3RhbnRzIH0gZnJvbSBcIi4vY29uc3RhbnRzXCJcclxuaW1wb3J0IHsgR2FtZSB9IGZyb20gXCIuL2dhbWVcIlxyXG5pbXBvcnQgeyBNb0NvQ2xpZW50fSBmcm9tIFwiLi9tb2NvY2xpZW50XCJcclxuaW1wb3J0IHsgSWNlQ29uZmlnR2VuZXJhdG9yIH0gZnJvbSBcIi4uL2NvbW1zL3NlcnZlci9pY2Vjb25maWdnZW5lcmF0b3JcIlxyXG5pbXBvcnQgZnMgPSByZXF1aXJlKCdmcycpXHJcbmltcG9ydCB7IFNpbXBsZUljZUNvbmZpZ0dlbiB9IGZyb20gXCIuLi9jb21tcy9zZXJ2ZXIvc2ltcGxlaWNlY29uZmlnZ2VuXCJcclxuaW1wb3J0IHsgSUljZUNvbmZpZ0dlbmVyYXRvciB9IGZyb20gXCIuLi9jb21tcy9zZXJ2ZXIvaW50ZXJmYWNlc1wiXHJcblxyXG5leHBvcnQgY2xhc3MgQ2xpZW50TWFuYWdlciBpbXBsZW1lbnRzIElDbGllbnRNYW5hZ2Vye1xyXG5cclxuXHJcbiAgcmVhZG9ubHkgaWNlQ29uZmlnR2VuOklJY2VDb25maWdHZW5lcmF0b3JcclxuXHJcbiAgcmVhZG9ubHkgY2xpZW50czpNYXA8c3RyaW5nLElNb0NvQ2xpZW50PlxyXG4gIHJlYWRvbmx5IGdhbWVzOk1hcDxzdHJpbmcsSUdhbWU+XHJcblxyXG4gIGNyZWF0ZUdhbWUoY2xpZW50OklNb0NvQ2xpZW50LCBrZXk6IHN0cmluZywgaGFzaDogc3RyaW5nKSB7XHJcbiAgICAvL2dlbmVyYXRlIHJhbmRvbSBrZXlzIHVudGlsIHdlIGZpbmQgb25lIG5vdCBjdXJyZW50bHkgaW4gdXNlLi4gdW5saWtlbHkgYnV0IHBvc3NpYmxlIGkgZ3Vlc3MuXHJcbiAgICBkb3tcclxuICAgICAgdmFyIGtleSA9IHRoaXMuY3JlYXRlR2FtZUtleSgpXHJcbiAgICB9d2hpbGUodGhpcy5nYW1lcy5oYXMoa2V5KSlcclxuICAgIC8vc3RvcmUgdGhlIG5ldyBnYW1lXHJcbiAgICB0aGlzLmdhbWVzLnNldChrZXksIG5ldyBHYW1lKGNsaWVudCwgdGhpcywga2V5KSlcclxuICB9XHJcblxyXG4gIGdldEdhbWUoa2V5OnN0cmluZyk6IElHYW1le1xyXG4gICAgcmV0dXJuIHRoaXMuZ2FtZXMuZ2V0KGtleSlcclxuICB9XHJcblxyXG4gIGRyb3BHYW1lKGtleTogc3RyaW5nKTogdm9pZCB7XHJcbiAgICB0aGlzLmdhbWVzLmRlbGV0ZShrZXkpXHJcbiAgfVxyXG5cclxuICBkcm9wQ2xpZW50KGlkOnN0cmluZyk6dm9pZHtcclxuICAgIHRoaXMuY2xpZW50cy5kZWxldGUoaWQpXHJcbiAgfVxyXG5cclxuICBjcmVhdGVHYW1lS2V5KCl7XHJcbiAgICB2YXIga2V5ID0gXCJcIlxyXG4gICAgZm9yICh2YXIgIGkgPSAwO2kgPCBDb25zdGFudHMuS2V5TGVuZ3RoOyBpKyspe1xyXG4gICAgICAga2V5ICArPSBDb25zdGFudHMuS2V5Q2hhcnNbTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogQ29uc3RhbnRzLktleUNoYXJzLmxlbmd0aCldXHJcbiAgICB9XHJcbiAgICByZXR1cm4ga2V5O1xyXG4gIH1cclxuXHJcblxyXG4vKlxyXG4gIGpvaW5HYW1lKHJlcTogQ29tbXMuSm9pbkdhbWVSZXF1ZXN0LCBzb2NrZXQ6IElvLlNvY2tldCkge1xyXG4gICAgLy92YWxpZGF0ZSByZXF1ZXN0ZWQga2V5Li4uIG9yIGRvbnQgYm90aGVyXHJcbiAgICBsZXQgZ2FtZSA9IHRoaXMuZ2FtZXMuZ2V0KHJlcS5rZXkpXHJcbiAgICBpZiAoZ2FtZSl7XHJcbiAgICAgIGdhbWUuYWRkUGxheWVyKG5ldyBQbGF5ZXIocmVxLm5hbWUsIHNvY2tldCwgZ2FtZSkpXHJcbiAgICB9XHJcbiAgICBlbHNle1xyXG4gICAgICBzb2NrZXQuZW1pdChFdmVudE5hbWVzLkVycm9yLCB7bWVzc2FnZTogXCJHYW1lIG5vdCBmb3VuZFwiIH0gYXMgQ29tbXMuRXJyb3JSZXNwb25zZSlcclxuICAgIH1cclxuICB9XHJcbiAgXHJcbiAgZHJvcEdhbWUoa2V5OnN0cmluZyk6dm9pZHtcclxuICAgIHRoaXMuZ2FtZXMuZGVsZXRlKGtleSlcclxuICB9XHJcblxyXG4gIGNyZWF0ZUdhbWUocmVxOiBDb21tcy5DcmVhdGVHYW1lUmVxdWVzdCwgc29ja2V0OiBJby5Tb2NrZXQpOnZvaWQge1xyXG4gICAgLy9UT0RPOiB2YWxpZGF0ZSByZXF1ZXN0ZWQgZXhpc3Rpbmcga2V5XHJcbiAgICAvL2dldCBhIG5ldyB1bnVzZWQga2V5XHJcbiBcclxuICB9XHJcblxyXG4gICovXHJcblxyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGlvOlNvY2tldElPLlNlcnZlcil7XHJcbiAgICB0aGlzLmNsaWVudHMgPSBuZXcgTWFwPHN0cmluZywgSU1vQ29DbGllbnQ+KClcclxuICAgIHRoaXMuZ2FtZXMgPSBuZXcgTWFwPHN0cmluZywgSUdhbWU+KClcclxuXHJcblxyXG4gICAgY29uc3QgY29uZiA9IEpTT04ucGFyc2UoZnMucmVhZEZpbGVTeW5jKCcuL2ljZWNvbmZpZy5qc29uJywgJ3V0ZjgnKSkgYXMgSWNlQ29uZm9nSnNvblxyXG4gICAgdGhpcy5pY2VDb25maWdHZW4gPSBuZXcgSWNlQ29uZmlnR2VuZXJhdG9yKGNvbmYuc3R1biwgY29uZi50dXJuLCBjb25mLnNlY3JldCwgY29uZi50dGwpXHJcblxyXG4gICAgLy9jb25zdCBjb25mID0gSlNPTi5wYXJzZShmcy5yZWFkRmlsZVN5bmMoJy4vc2ltcGxlY29uZmlnLmpzb24nLCAndXRmOCcpKSBhcyBTaW1wbGVJY2VDb25mb2dKc29uXHJcbiAgICAvL3RoaXMuaWNlQ29uZmlnR2VuID0gbmV3IFNpbXBsZUljZUNvbmZpZ0dlbihjb25mLnN0dW4sIGNvbmYudHVybiwgY29uZi51c2VybmFtZSwgY29uZi5wYXNzd29yZClcclxuICAgICAgXHJcblxyXG4gICAgdGhpcy5pby5vbignY29ubmVjdGlvbicsIChzb2NrZXQ6SW8uU29ja2V0KT0+e1xyXG4gICAgICBjb25zb2xlLmxvZyhgdXNlciBjb25uZWN0ZWQ6ICR7c29ja2V0LmlkfWApXHJcbiAgICAgIGNvbnN0IGNsaWVudCA9IG5ldyAgTW9Db0NsaWVudCh0aGlzLCBzb2NrZXQsIHRoaXMuaWNlQ29uZmlnR2VuKVxyXG4gICAgICB0aGlzLmNsaWVudHMuc2V0KGNsaWVudC5pZCwgY2xpZW50KVxyXG4gICAgfSlcclxuICB9XHJcbn1cclxuIiwiZXhwb3J0IG5hbWVzcGFjZSBDb25zdGFudHN7XHJcbiAgZXhwb3J0IGNvbnN0IEtleUxlbmd0aDpudW1iZXIgPSA1XHJcbiAgZXhwb3J0IGNvbnN0IEtleUNoYXJzOnN0cmluZyA9IFwiQUJDREVGR0hJSktMTU5PUFFSU1RVVldYWVowMTIzNDU2Nzg5XCJcclxuXHJcblxyXG59IiwiaW1wb3J0IHsgSUdhbWUsICAgSU1vQ29DbGllbnQsIElDbGllbnRNYW5hZ2VyIH0gZnJvbSBcIi4vaW50ZXJmYWNlc1wiO1xyXG5pbXBvcnQgeyBNb0NvQ29tbXMgfSBmcm9tIFwiLi4vY29tbW9uL21vY29jb21tc1wiO1xyXG5pbXBvcnQgeyBCYXNlQ2xpZW50IH0gZnJvbSBcIi4uL2NvbW1zL3NlcnZlci9iYXNlY2xpZW50XCI7XHJcblxyXG5leHBvcnQgY2xhc3MgR2FtZSBpbXBsZW1lbnRzIElHYW1le1xyXG5cclxuICByZWFkb25seSBwbGF5ZXJzOk1hcDxzdHJpbmcsSU1vQ29DbGllbnQ+XHJcbiAgcGxheWVySWQ6bnVtYmVyID0gMVxyXG4gIGFkZFBsYXllcihwbGF5ZXI6SU1vQ29DbGllbnQpOnZvaWR7XHJcbiAgICBwbGF5ZXIuc2V0TmV4dEVudElkKCh0aGlzLnBsYXllcklkKyspICogMzIpXHJcbiAgICBjb25zdCBwbGF5ZXJJbmZvID0gcGxheWVyLmdldFBlZXJJbmZvKCkgYXMgTW9Db0NvbW1zLlBsYXllckluZm9cclxuICAgIGNvbnNvbGUubG9nKGBwbGF5ZXIgJHtwbGF5ZXJJbmZvPy5uYW1lID8/IFwiVW5uYW1lZFwifSBjb25uZWN0aW5nIHRvICR7dGhpcy5rZXl9YClcclxuICAgIHRoaXMuY2xpZW50LnN0YXJ0UGVlckNvbm5lY3QocGxheWVyICwgdHJ1ZSlcclxuICB9XHJcblxyXG4gIGRyb3BQbGF5ZXIodXVpZDpzdHJpbmcpIHtcclxuICAvLyAgdGhpcy5wbGF5ZXJzLmRlbGV0ZSh1dWlkKSAgXHJcbiAgLy8gIHRoaXMuc29ja2V0LmVtaXQoRXZlbnROYW1lcy5QbGF5ZXJEaXNjb25uZWN0LCB7IHV1aWQ6dXVpZCB9ICBhcyBDb21tcy5QbGF5ZXJEaXNjb25uZWN0aW5nIClcclxuICB9XHJcbiAgXHJcbiAgY29uc3RydWN0b3IocHJvdGVjdGVkIGNsaWVudDpJTW9Db0NsaWVudCwgcHJvdGVjdGVkIG93bmVyOklDbGllbnRNYW5hZ2VyLCBwdWJsaWMga2V5OnN0cmluZyl7XHJcbiAgICB0aGlzLnBsYXllcnMgPSBuZXcgTWFwPHN0cmluZyxJTW9Db0NsaWVudD4oKVxyXG4gICAgLy90ZWxsIHRoZSBnYW1lIHRoYXQgaXQncyByZWdpc3RlcmVkIGFuZCBjYW4gc3RhcnQuLi4uXHJcbiAgICBjbGllbnQuc29ja2V0LmVtaXQoTW9Db0NvbW1zLkV2ZW50cy5DcmVhdGVHYW1lUmVzcG9uc2UsIHsga2V5OiBrZXkgfSBhcyBNb0NvQ29tbXMuQ3JlYXRlR2FtZVJlc3BvbnNlKVxyXG4gICAgY29uc29sZS5sb2coYEdhbWUgc3RhcnRlZCAke3RoaXMua2V5fWApXHJcbiAgfVxyXG5cclxuXHJcblxyXG5cclxufSAgICIsImltcG9ydCAqIGFzIElvIGZyb20gXCJzb2NrZXQuaW9cIlxyXG5pbXBvcnQgdXVpZCA9IHJlcXVpcmUoXCJ1dWlkXCIpO1xyXG5pbXBvcnQgeyBCYXNlQ2xpZW50IH0gZnJvbSBcIi4uL2NvbW1zL3NlcnZlci9iYXNlY2xpZW50XCI7XHJcbmltcG9ydCB7IElNb0NvQ2xpZW50LCBJQ2xpZW50TWFuYWdlciB9IGZyb20gXCIuL2ludGVyZmFjZXNcIjtcclxuaW1wb3J0IHsgTW9Db0NvbW1zIH0gZnJvbSBcIi4uL2NvbW1vbi9tb2NvY29tbXNcIjtcclxuaW1wb3J0IHsgSUljZUNvbmZpZ0dlbmVyYXRvciB9IGZyb20gXCIuLi9jb21tcy9zZXJ2ZXIvaW50ZXJmYWNlc1wiO1xyXG5cclxuXHJcbmV4cG9ydCBjbGFzcyBNb0NvQ2xpZW50IGV4dGVuZHMgQmFzZUNsaWVudCBpbXBsZW1lbnRzIElNb0NvQ2xpZW50e1xyXG5cclxuICBwcm90ZWN0ZWQgbmFtZTpzdHJpbmdcclxuXHJcbiAgcHVibGljIGNvbnN0cnVjdG9yKHByb3RlY3RlZCBvd25lcjpJQ2xpZW50TWFuYWdlciwgc29ja2V0OklvLlNvY2tldCwgaWNlR2VuOklJY2VDb25maWdHZW5lcmF0b3Ipe1xyXG4gICAgc3VwZXIoc29ja2V0LCBudWxsLCBpY2VHZW4pXHJcbiAgICB0aGlzLm5hbWUgPSBcIlwiXHJcblxyXG4gICAgc29ja2V0Lm9uKE1vQ29Db21tcy5FdmVudHMuQ3JlYXRlR2FtZVJlcXVlc3QsIChyZXE6TW9Db0NvbW1zLkNyZWF0ZUdhbWVSZXF1ZXN0KT0+eyAgXHJcbiAgICAgIG93bmVyLmNyZWF0ZUdhbWUodGhpcywgcmVxLmtleSwgcmVxLmhhc2gpXHJcbiAgICB9KVxyXG5cclxuICAgIHNvY2tldC5vbihNb0NvQ29tbXMuRXZlbnRzLkpvaW5HYW1lUmVxdWVzdCwgKHJlcTpNb0NvQ29tbXMuSm9pbkdhbWVSZXF1ZXN0KT0+eyBcclxuICAgICAgdGhpcy5uYW1lID0gcmVxLm5hbWVcclxuICAgICAgY29uc3QgZ2FtZSA9b3duZXIuZ2V0R2FtZShyZXEua2V5KVxyXG4gICAgICBnYW1lLmFkZFBsYXllcih0aGlzKTtcclxuXHJcbiAgICB9KVxyXG4gICAgc29ja2V0Lm9uKE1vQ29Db21tcy5FdmVudHMuRGlzY29ubmVjdGVkLCAoKT0+eyBcclxuICAgICAgdGhpcy5vd25lci5kcm9wQ2xpZW50KHRoaXMuaWQpXHJcbiAgICB9KVxyXG4gIH1cclxuXHJcbiAgcHVibGljIGdldFBlZXJJbmZvKCk6b2JqZWN0e1xyXG4gICAgcmV0dXJuIHsgbmFtZTogdGhpcy5uYW1lLCB1dWlkOnRoaXMuaWQgfSBhcyBNb0NvQ29tbXMuUGxheWVySW5mb1xyXG4gIH1cclxuICBcclxuICBzZXROZXh0RW50SWQobnVtOiBudW1iZXIpOiB2b2lkIHtcclxuICAgIHRoaXMuZW50SWQgPSBudW1cclxuICB9XHJcblxyXG4gIFxyXG59XHJcbiIsImltcG9ydCAqIGFzIEh0dHAgZnJvbSBcImh0dHBcIlxyXG5pbXBvcnQgKiBhcyBIdHRwcyBmcm9tIFwiaHR0cHNcIlxyXG5pbXBvcnQgKiBhcyBmcyBmcm9tIFwiZnNcIlxyXG5pbXBvcnQgKiBhcyBFeHByZXNzICBmcm9tIFwiZXhwcmVzc1wiXHJcbmltcG9ydCAqIGFzIElvIGZyb20gXCJzb2NrZXQuaW9cIlxyXG5pbXBvcnQgKiBhcyBwYXRoIGZyb20gXCJwYXRoXCJcclxuaW1wb3J0IHsgQ2xpZW50TWFuYWdlciB9IGZyb20gXCIuL2NsaWVudG1hbmFnZXJcIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBTZXJ2ZXJ7XHJcblxyXG4gIHByaXZhdGUgcmVhZG9ubHkgcm91dGVyOkV4cHJlc3MuUm91dGVyXHJcbiAgcHJpdmF0ZSByZWFkb25seSBhcHA6RXhwcmVzcy5FeHByZXNzXHJcbiAgcHJpdmF0ZSByZWFkb25seSBpbzpTb2NrZXRJTy5TZXJ2ZXJcclxuICBwcml2YXRlIHJlYWRvbmx5IGNsaWVudE1hbjpDbGllbnRNYW5hZ2VyXHJcblxyXG4gIHByb3RlY3RlZCBDcmVhdGVIdHRwc1NlcnZlcihhcHA6RXhwcmVzcy5FeHByZXNzKXtcclxuICAgIGNvbnN0IHByaXZhdGVLZXkgPSBmcy5yZWFkRmlsZVN5bmMoJy9ldGMvbGV0c2VuY3J5cHQvbGl2ZS9zdGFyZHVlbC5jaWRlcnB1bmsubmV0L3ByaXZrZXkucGVtJywgJ3V0ZjgnKVxyXG4gICAgY29uc3QgY2VydGlmaWNhdGUgPSBmcy5yZWFkRmlsZVN5bmMoJy9ldGMvbGV0c2VuY3J5cHQvbGl2ZS9zdGFyZHVlbC5jaWRlcnB1bmsubmV0L2NlcnQucGVtJywgJ3V0ZjgnKVxyXG4gICAgY29uc3QgY2EgPSBmcy5yZWFkRmlsZVN5bmMoJy9ldGMvbGV0c2VuY3J5cHQvbGl2ZS9zdGFyZHVlbC5jaWRlcnB1bmsubmV0L2NoYWluLnBlbScsICd1dGY4JylcclxuICAgIGNvbnN0IGNyZWRlbnRpYWxzID0ge1xyXG4gICAgICBrZXk6IHByaXZhdGVLZXksXHJcbiAgICAgIGNlcnQ6IGNlcnRpZmljYXRlLFxyXG4gICAgICBjYTogY2FcclxuICAgIH1cclxuXHJcbiAgICBjb25zdCBodHRwc1NlcnZlciA9IEh0dHBzLmNyZWF0ZVNlcnZlcihjcmVkZW50aWFscywgYXBwKVxyXG4gICAgaHR0cHNTZXJ2ZXIubGlzdGVuKDQ0MywoKT0+e1xyXG4gICAgICBjb25zb2xlLmxvZygnbGlzdGVuaW5nIG9uICo6NDQzJylcclxuICAgIH0pXHJcbiAgICByZXR1cm4gaHR0cHNTZXJ2ZXJcclxuICB9IFxyXG5cclxuICBwcm90ZWN0ZWQgQ3JlYXRlSHR0cFNlcnZlcihhcHA6RXhwcmVzcy5FeHByZXNzLCBwb3J0Om51bWJlciA9IDgwKXtcclxuICAgIGNvbnN0IGh0dHBTZXJ2ZXIgPSBIdHRwLmNyZWF0ZVNlcnZlcihhcHApXHJcbiAgICBodHRwU2VydmVyLmxpc3Rlbihwb3J0LCgpPT57XHJcbiAgICAgIGNvbnNvbGUubG9nKCdsaXN0ZW5pbmcgb24gKjonICsgcG9ydClcclxuICAgIH0pXHJcbiAgICByZXR1cm4gaHR0cFNlcnZlclxyXG4gIH0gXHJcblxyXG4gIGNvbnN0cnVjdG9yKGVuYWJsZUh0dHBzOmJvb2xlYW4gPSBmYWxzZSwgcG9ydDpzdHJpbmcgPSBwcm9jZXNzLmVudi5QT1JUIHx8IFwiODBcIiApe1xyXG4gICAgdGhpcy5hcHAgID0gRXhwcmVzcygpO1xyXG4gICAgLy90aGlzLmFwcC51c2UoJy9zcmMnLCBFeHByZXNzLnN0YXRpYygnc3JjJykpO1xyXG4gICAgdGhpcy5hcHAudXNlKCcvbm9kZV9tb2R1bGVzJywgRXhwcmVzcy5zdGF0aWMoJ25vZGVfbW9kdWxlcycpKTtcclxuICAgIHRoaXMuYXBwLnVzZShFeHByZXNzLnN0YXRpYygncHVibGljJykpO1xyXG4gICAgdGhpcy5hcHAudXNlKCcvcGxheS86a2V5JywgKHJlcSxyZXMpPT57IFxyXG4gICAgICByZXMuc2VuZEZpbGUoXCJwdWJsaWMvcGxheS9pbmRleC5odG1sXCIsIHsgcm9vdDpwcm9jZXNzLmN3ZCgpfSkgICBcclxuICAgIH0pXHJcblxyXG5cclxuICAgIGxldCBzZXJ2ZXI6YW55ID0gbnVsbFxyXG4gICAgLy9odHRwc1xyXG4gICAgaWYgKGVuYWJsZUh0dHBzKXtcclxuICAgICAgc2VydmVyID0gdGhpcy5DcmVhdGVIdHRwc1NlcnZlcih0aGlzLmFwcClcclxuICAgICAgLy9jcmVhdGUgYSBzaW1wbGUgcmVkaXJlY3Qgc2VydmljZVxyXG4gICAgICBjb25zdCByZWRpcmVjdEFwcCA9IEV4cHJlc3MoKVxyXG4gICAgICByZWRpcmVjdEFwcC5nZXQoJyonLCAocmVxLHJlcyk9PntcclxuICAgICAgICByZXMucmVkaXJlY3QoJ2h0dHBzOi8vJyArIHJlcS5oZWFkZXJzLmhvc3QgKyByZXEudXJsKTtcclxuICAgICAgfSlcclxuICAgICAgdGhpcy5DcmVhdGVIdHRwU2VydmVyKHJlZGlyZWN0QXBwLHBhcnNlSW50KHBvcnQpKVxyXG4gICAgfVxyXG4gICAgZWxzZXtcclxuICAgICAgc2VydmVyID0gdGhpcy5DcmVhdGVIdHRwU2VydmVyKHRoaXMuYXBwLCBwYXJzZUludChwb3J0KSlcclxuICAgIH1cclxuXHJcbiAgICAvL2NyZWF0ZSBzb2NrZXRpb1xyXG4gICAgdGhpcy5pbyA9IElvKHNlcnZlcilcclxuICAgIHRoaXMuY2xpZW50TWFuID0gbmV3IENsaWVudE1hbmFnZXIodGhpcy5pbylcclxuICB9XHJcblxyXG59IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiY3J5cHRvXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcImV4cHJlc3NcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiZnNcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiaHR0cFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJodHRwc1wiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJzb2NrZXQuaW9cIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwidXVpZFwiKTsiLCIvLyBUaGUgbW9kdWxlIGNhY2hlXG52YXIgX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fID0ge307XG5cbi8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG5mdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuXHR2YXIgY2FjaGVkTW9kdWxlID0gX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fW21vZHVsZUlkXTtcblx0aWYgKGNhY2hlZE1vZHVsZSAhPT0gdW5kZWZpbmVkKSB7XG5cdFx0cmV0dXJuIGNhY2hlZE1vZHVsZS5leHBvcnRzO1xuXHR9XG5cdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG5cdHZhciBtb2R1bGUgPSBfX3dlYnBhY2tfbW9kdWxlX2NhY2hlX19bbW9kdWxlSWRdID0ge1xuXHRcdC8vIG5vIG1vZHVsZS5pZCBuZWVkZWRcblx0XHQvLyBubyBtb2R1bGUubG9hZGVkIG5lZWRlZFxuXHRcdGV4cG9ydHM6IHt9XG5cdH07XG5cblx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG5cdF9fd2VicGFja19tb2R1bGVzX19bbW9kdWxlSWRdKG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG5cdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG5cdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbn1cblxuIiwiaW1wb3J0IHtTZXJ2ZXJ9IGZyb20gXCIuL3NlcnZlclwiO1xyXG5sZXQgcyA9IG5ldyBTZXJ2ZXIoZmFsc2UsIFwiODAwMFwiKTsiXSwibmFtZXMiOltdLCJzb3VyY2VSb290IjoiIn0=