import { h, Component, ComponentChild } from "preact"
import * as io from "socket.io-client"
import { Status, ClientConstants } from "./constants"
import { Register } from "./ui/register"
import { PeerConnectionManager } from "../comms/client/peerconnectionmanager"
import { Config } from "../common/config"
import { MoCoComms } from "../common/mococomms"
import { Control } from "./ui/control"
import { CommsEntityManager } from "../comms/client/commsentitymanager"
import { PeerConnection } from "../comms/client/peerconnection"
import { MoCoEntityFactory } from "../common/mocogameentityfactory"
import { RemotePlayerControl } from "../common/peercomms/remoteplayercontrol"
import { MoCoPeerComms } from "../common/peercomms/enums"
import { RegisterList } from "../common/utils/registerlist"
import { PcmProxy } from "../common/pcmproxy"
import { IScreenSyncd, ITankDescription, ITankHealth } from "../common/interfaces"
import { ICommsEntity, IPeerConnection } from "../comms/client/clientinterfaces"
import { TankInfo } from "./ui/tankinfo"
import { HealthBar } from "./ui/healthbar"
import { TankHealth } from "../common/peercomms/tankHealth"

export interface ClientProps{

}

interface ClientState{
  state:Status
  playerName:string
  gameKey:string
  message:string
  navOpen:boolean
  control?:RemotePlayerControl
  tankDesc?:ITankDescription
  tankHealth:number
}

export class Client extends Component<ClientProps, ClientState>{


  private readonly screenSynced:RegisterList<IScreenSyncd>


  public readonly socket:SocketIOClient.Socket
  public readonly pcm:PeerConnectionManager
  public readonly entman:CommsEntityManager
  protected controller:RemotePlayerControl

  protected reqAnimFrame:number

  public constructor(props:ClientProps){
    super(props)
    this.screenSynced = new RegisterList<IScreenSyncd>()
    this.socket = io()    
    this.entman = new CommsEntityManager(new MoCoEntityFactory())


    this.entman.onNetCreate=(peer:IPeerConnection, ent:ICommsEntity )=>{
      switch (ent.typeId){
        case  MoCoPeerComms.EntityType.TankDescrition:
          const tankDesc = ent as ITankDescription
          //update state
          this.setState({ tankDesc:tankDesc})
          break


        case MoCoPeerComms.EntityType.TankHealth: 
          const tankHealth = ent as ITankHealth 
          tankHealth.onUpdated = (health:number)=>{
            this.setState({ tankHealth:health })
          }
          break
          
      }

    }


    this.pcm = new PeerConnectionManager(this.socket, this.entman, Config.iceConfig)

    this.pcm.onConnecting = ()=>{ this.setState({ message: "Connecting"})}
    this.pcm.onConnected = (pc:PeerConnection)=>{  this.startGame(pc) }
    this.pcm.onDisconnected = ()=>{ this.setState({ message: "Disconnected",  state: Status.Error })}
    /*
    this.socket.on(EventNames.Error, (error:Comms.ErrorResponse)=>{ this.handleCommsError(error.message) })
    this.socket.on(EventNames.ReadyToJoin, ()=>{ this.gc = new GameComm(this, true)})
    */
    //get stored player name
    const playerName = localStorage.getItem(ClientConstants.PlayerNameKey) 
    const url = window.location.href
    const key= url.substr(url.lastIndexOf('/')+1) 
    this.state = { playerName:playerName, gameKey: key, state:  Status.Register, message:"", navOpen:false, tankHealth:100 }

    this.componentDidMount = ()=>{
      this.reqAnimFrame = window.requestAnimationFrame(()=>{this.doFrame()})
    }

    this.componentWillUnmount= ()=>{
      window.cancelAnimationFrame(this.reqAnimFrame)
    }
  }

  private lastFrame:number
  protected doFrame():void{
    //get time and last time..
    const  t:number = performance.now()
    const dt = t- this.lastFrame
    this.lastFrame = t
    //queue up next frame
    this.reqAnimFrame = window.requestAnimationFrame(()=>{this.doFrame()})
    //lets trigger those timed components
    this.screenSynced.items.forEach((com:IScreenSyncd)=>{
      com.refresh(t, dt)
    })
  }

  startGame(pc:PeerConnection) {
    this.controller = this.entman.createEntity(MoCoPeerComms.EntityType.Controller) as RemotePlayerControl
    this.controller.onChange=(controller:RemotePlayerControl )=>{ 
      pc.sendEntity( controller)
      pc.flush()
    }
    this.setState({ message: "Connected", control:this.controller, state: Status.Playing })
    //make sure server has ourt controller ent from the start,,,
    pc.sendEntity(this.controller)
    pc.flush()

    this.screenSynced.register( new PcmProxy(this.pcm), 0 )
  }

  joinGame(toJoinKey:String):void{
    //this should start the rtc comms...
    this.socket.emit(MoCoComms.Events.JoinGameRequest, { key: toJoinKey, name: this.state.playerName } as MoCoComms.JoinGameRequest)  
  }

  handleCommsError(message: string):void {
    this.setState({message:message, state: Status.Error})
  }

  handleUpdatePrefs(name:string):void{
    this.setState({ playerName: name })
    localStorage.setItem(ClientConstants.PlayerNameKey, name) 
  }

  handleRegister(key:string, playerName:string):void{
    localStorage.setItem(ClientConstants.PlayerNameKey, playerName) 
    this.setState({playerName: playerName, state: Status.Waiting, message:"initiating connection..."})
    this.joinGame(key)

    document.documentElement.requestFullscreen()

  }

  toggleMenu(){
    this.setState({ navOpen: !this.state.navOpen})
  }

  render(props: Readonly<ClientProps>, state?: Readonly<ClientState>): ComponentChild { 
    return state.state == Status.Playing ? 
    <div>
      <Control control={state.control} registerScreenSyncd={(com:IScreenSyncd, sort:number)=>this.screenSynced.register(com, sort)} 
        deRegisterScreenSyncd={(com:IScreenSyncd)=>this.screenSynced.deregister(com)}/> 
      <TankInfo tankDescription={state.tankDesc}/> 
      <HealthBar health={state.tankHealth}/>


      

    </div>
      : 
    <div> 
      <nav class="navbar">
        <div class="navbar-brand">
          <div class="navbar-item">
            <h1>TANK CLUB!</h1>



          </div>
        </div>
      </nav>
      <HealthBar health={50} />
      {this.renderInner(props, state)}
    </div>
  }


  renderInner(props: Readonly<ClientProps>, state?: Readonly<ClientState> ){
   
    switch (state.state){
      case Status.Waiting:
        return <section class="section">
          <div class="message is-info">
            <div class="message-header">
              <h2>Status</h2>
            </div>
            <div class="message-body">
              {state.message}
            </div>
          </div>
        </section>

      case Status.Register:
        return <Register gameKey={state.gameKey} onRegister={(key:string, playerName:string)=>{this.handleRegister(key, playerName)} } playerName={state.playerName} />

      case Status.Error:
        return <section class="section">
          <div class="message is-danger">
            <div class="message-header">
              <p>Error</p>
            </div>
            <div class="message-body">
              {state.message}
            </div>
          </div>
        </section>
      default:
        return <div>Player</div>
    }

  }
}
