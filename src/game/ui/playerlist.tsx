import { h, Component, ComponentChild } from "preact"
import { IPeerPlayer } from "../interfaces"

const PlayerView = (prop:{name:string})=>(
  <li>{prop.name}</li>
)

export interface PlayerListProps{
   players:Map<string,IPeerPlayer>
}

interface PlayerListState{
}

export class PlayerList extends Component<PlayerListProps,PlayerListState>{
  private url:string
  render(props: Readonly<PlayerListProps>, state?: Readonly<PlayerListState>, context?: any): ComponentChild {
    const players = Array.from(props.players.values())
    return (  <div class="playerlist"> player list
      <ul>
        {players.map((item:IPeerPlayer)=><PlayerView name={item.name}/>)}
      </ul>
    </div>)
  }
  
}