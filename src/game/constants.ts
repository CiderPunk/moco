import { Color3 } from "babylonjs"

export namespace Constants{
  export const TickLength:number = 1/60

  export const GoalSize = 40
  export const GoalDepth = 6
  export const BallRadius = 2.2
  export const TankHealth = 6000
  export const DeathTimer = 5000
  export const GoalResetTime = 5000
}


export namespace Colours{
  export const red = new Color3(1,0,0)
  export const green = new Color3(0,1,0)
  export const blue = new Color3(0,0,1)
  export const white = new Color3(0.9,0.9,0.9)
  export const black = new Color3(0.2,0.2,0.2)
  export const cyan = new Color3(0,1,1)
  export const yellow = new Color3(1,1,0)
  export const orange = new Color3(1,0.5,0)
  export const mauve = new Color3(0.5,0.5,1)
  export const teal = new Color3(0.25,0.5,0.5)
  export const brown = new Color3(0.5,0.25,0.0)
  export const gold = new Color3(0.5,0.5,0.25)
  export const pink = new Color3(1,0.5,0.75)
  export const fuschia = new Color3(1,0,0.5)
  export const purple = new Color3(0.5,0,1)
}


export enum CollisionGroup {
  boundary = 0x0001,
  tank = 0x0002,
  projectile = 0x0004,
  ball = 0x0008,
  ballBoundary = 0x0010,
  all = 0xffff,
  none = 0x0000,

}




