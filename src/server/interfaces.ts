import { IClient } from "../comms/server/interfaces";

export interface IClientManager{
  getGame(key: string):IGame
  dropClient(id: string):void
  createGame(client:IMoCoClient, key: string, hash: string);
  dropGame(key:string):void
}

export interface IMoCoClient extends IClient{
  id:string
  setNextEntId(num:number):void
}

export interface IGame{
  addPlayer(player:IMoCoClient);
  key:string
}

export interface IceConfogJson{
  stun:string
  turn:string
  secret:string
  ttl:number
}


export interface SimpleIceConfogJson{
  stun:string
  turn:string
  username:string
  password:string
}