import { MoCoComms } from "../common/mococomms";
import { Game } from "./game";
import { IPeerPlayer } from "./interfaces";
import { IPeerConnection } from "../comms/client/clientinterfaces";


export class PeerPlayer implements IPeerPlayer {
  
  //public readonly uuid:string
  public readonly name:string
  public readonly uuid:string


  constructor(protected readonly owner: Game, protected readonly pc: IPeerConnection) {
    //get player info....
    const playerInfo = pc.peerInfo as MoCoComms.PlayerInfo
    this.name = playerInfo.name
    this.uuid = playerInfo.uuid
  }
}
