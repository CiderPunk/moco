import { h, Component, ComponentChild } from "preact"
import * as QRCode from 'qrcode'

export interface JoinProps{
  gameKey:string
}


interface JoinState{
}

export class Join extends Component<JoinProps,JoinState>{
  private url:string
  render(props: Readonly<JoinProps>, state?: Readonly<JoinState>, context?: any): ComponentChild {
    this.url = window.location.origin +"/play/" + props.gameKey
    return (
      <div class="join">
        <p>To join, go to <strong><a href={this.url} target="_blank">{this.url}</a></strong> on your mobile telephone device</p>
        <canvas id="joinqr"></canvas>
      </div>)
  }
  public componentDidUpdate():void {
    QRCode.toCanvas(document.getElementById("joinqr"), this.url, function (error) {
      if (error) console.error(error)
      console.log('success!');
    })
  }
}