import { Scene, SpotLight, Vector3 } from 'babylonjs';

export namespace LightHelpers{

  
  export function initSpot(name:string, scene:Scene, pos:Vector3, target:Vector3, spread:number, reach:number, intensity:number):SpotLight{
    const dir = target.clone().subtract(pos).normalize()
    const spot = new SpotLight(name, pos,dir, spread, reach, scene);
    spot.intensity = intensity

    return spot
  }


}